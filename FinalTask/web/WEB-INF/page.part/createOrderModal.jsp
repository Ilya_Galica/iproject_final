<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <div class="modal fade" id="createOrderModal" tabindex="-1" role="dialog" aria-labelledby="createOrderModalTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createOrderModalTitle">
                        <fmt:message key="create.order.on.hotelroom"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="<c:url value="/hotels/hotelroom/order"/>" method="post" id="orderForm" class="needs-validation"
                          novalidate>
                        <div id="wrongConfirm" style="display: none; color: red"><fmt:message key="wrong.email.confirm.code"/></div>
                        <input type="hidden" name="command" value="createOrder">
                        <div id="orderError">${orderError}</div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="nameInputOrder"><fmt:message key="name"/></label>
                                <input type="text" class="form-control" id="nameInputOrder"
                                       placeholder="<fmt:message key="name"/>"
                                       name="name" pattern="[a-zA-Zа-яА-Я]{2,40}" required>
                                <div class="valid-feedback">
                                    <fmt:message key="right.name"/>
                                </div>
                                <div class="invalid-feedback">
                                    <fmt:message key="not.right.name"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="surnameInputOrder"><fmt:message key="surname"/></label>
                                <input type="text" class="form-control" id="surnameInputOrder"
                                       placeholder="<fmt:message key="surname"/>"
                                       name="surname" pattern="[a-zA-Zа-яА-Я]{2,40}" required>
                                <div class="valid-feedback">
                                    <fmt:message key="right.surname"/>
                                </div>
                                <div class="invalid-feedback">
                                    <fmt:message key="not.right.surname"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="patronymicInputOrder"><fmt:message key="patronymic"/></label>
                            <input type="text" class="form-control" id="patronymicInputOrder"
                                   placeholder="<fmt:message key="patronymic"/>"
                                   name="patronymic" pattern="[a-zA-Zа-яА-Я]{2,40}" required>
                            <div class="valid-feedback">
                                <fmt:message key="right.patronymic"/>
                            </div>
                            <div class="invalid-feedback">
                                <fmt:message key="not.right.patronymic"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="emailInputOrder">Email</label>
                            <input type="email" class="form-control" id="emailInputOrder" placeholder="Email"
                                   name="email" pattern="^[\w]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"
                                   min="5" max="34" required>
                            <div class="valid-feedback">
                                <fmt:message key="right.email"/>
                            </div>
                            <div class="invalid-feedback">
                                <fmt:message key="not.right.email"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="countryOrder"> <fmt:message key="choose.country"/></label>
                                <select id="countryOrder" class="form-control">
                                    <option value="ua"><fmt:message key="ukraine"/> +380</option>
                                    <option value="by" selected><fmt:message key="belarus"/> +375</option>
                                </select>
                            </div>
                            <div class=" col-md-6">
                                <label for="telephoneInputOrder"><fmt:message key="telephone"/></label>

                                <input type="text" class="form-control" id="telephoneInputOrder"
                                       placeholder="<fmt:message key="telephone"/>"
                                       name="telephone"
                                       pattern="((80)?|(\+375)?)(\(\d{2}\)|\d{2})((\d{3}-\d{2}-\d{2})|\d{7})"
                                       required>
                                <div class="valid-feedback">
                                    <fmt:message key="right.telephone"/>
                                </div>
                                <div class="invalid-feedback">
                                    <fmt:message key="not.right.telephone"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="jobInputOrder"><fmt:message key="job"/></label>
                            <input type="text" class="form-control" id="jobInputOrder"
                                   placeholder="<fmt:message key="job"/>" max="45"
                                   name="jobPlace" aria-describedby="jobInputHelp">
                            <small id="jobInputHelp" class="text-muted">
                                <fmt:message key="job.opportunity"/>
                            </small>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <fmt:message key="close"/></button>
                    <input type="button" class="btn btn-primary" id="createOrder" form="orderForm"
                           value="<fmt:message key="create.order"/>">
                </div>
            </div>
        </div>
    </div>
</fmt:bundle>