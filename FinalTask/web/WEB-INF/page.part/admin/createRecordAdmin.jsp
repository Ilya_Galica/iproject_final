<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="<c:url value="/js/admin/createRecord.js"/>"></script>
<div class="card">
    <div class="card-header" id="headingFour">
        <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                    data-target="#collapseFour"
                    aria-expanded="false" aria-controls="collapseFour">
                Add information
            </button>
        </h2>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#adminAction">
        <div class="card-body col-12">
            <div class="col-12 text-center">
                <button class="btn btn-light" type="button" data-toggle="collapse" data-target="#collapseDiscount"
                        aria-expanded="false" aria-controls="collapseExample">
                    New discount
                </button>
                <br>
                <div class="collapse" id="collapseDiscount">
                    <div class="card card-body">
                        <form class="col-12 row" id="createDiscountForm" novalidate>
                            <div class="form-group col-sm-6 col-md-4">
                                <label>Discount's job
                                    <input type="text" class="form-control" placeholder="Discount's job"
                                           name="insertUserJobPlace" required id="insertUserJobPlace"
                                           pattern="[a-zA-Zа-яА-Я\s]{2,}">
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-4">
                                <label>Discount
                                    <input type="text" class="form-control" placeholder="Discount"
                                           name="insertDiscountValueForJobPlace" required
                                           id="insertDiscountValueForJobPlace" pattern="[0-9]{1,2}">
                                </label>
                            </div>
                            <input type="button" class="btn btn-primary col-sm-10 col-md-3 align-self-center"
                                   value="Add new discount" id="requestCreateDiscount">

                            <div class="col-12 justify-content-center" id="discountTempCreate">

                            </div>

                        </form>
                    </div>
                </div>

            </div>
            <div class="col-12  text-center">
                <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#collapseHotelroom"
                        aria-expanded="false" aria-controls="collapseExample">
                    New hotelroom
                </button>
                <br>
                <div class="collapse" id="collapseHotelroom">
                    <div class="card card-body">
                        <form class="col-12 row" id="createHotelroomForm" novalidate>

                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>Room number
                                    <input type="text" class="form-control" placeholder="Room number"
                                           name="insertNumber" id="insertNumber" required
                                           pattern="[0-9]{1,3}">
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>Hotel id
                                    <input type="text" class="form-control" placeholder="Hotel id"
                                           name="insertHotelId" id="insertHotelId" required
                                           pattern="[0-9]{1,2}">
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>Price per night
                                    <input type="text" class="form-control" placeholder="Price per night"
                                           name="insertPrice" id="insertPrice" required
                                           pattern="[0-9]{1,4}">
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>Capacity
                                    <input type="text" class="form-control" placeholder="Capacity (up to 5)"
                                           name="insertCapacity" id="insertCapacity"
                                           required
                                           pattern="[0-5]{1}">
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>Comfort
                                    <select name="insertComfort" class="input-data form-control"
                                            id="insertComfort">
                                        <option>lux</option>
                                        <option>halflux</option>
                                        <option>common</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>Picture address
                                    <input type="text" class="form-control" placeholder="Picture address"
                                           name="insertImgURL" id="insertImgURL" required>
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                <label class="col-12"> Room description
                                    <input class="col-12 form-control" type="text" placeholder="Room description(ru)"
                                           name="insertDescription" id="insertDescription"
                                           required>
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                <label class="col-12">Room description
                                    <input class="col-12 form-control" type="text"
                                           placeholder="Room description(en)"
                                           name="insertDescriptionEn" required>
                                </label>
                            </div>
                            <input type="button" class="btn btn-primary justify-content-center col-3 align-self-center"
                                   value="Add new hotelroom" id="requestCreateHotelroom">

                            <div class="col-12 justify-content-center" id="hotelroomTempCreate">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12  text-center">
                <button class="btn btn-light justify-content-center" type="button" data-toggle="collapse" data-target="#collapseHotel"
                        aria-expanded="false" aria-controls="collapseExample">
                    New hotel
                </button>
                <br>
                <div class="collapse" id="collapseHotel">
                    <div class="card card-body">
                        <form class="col-12 row" id="createHotelForm" novalidate>
                            <input type="hidden" name="command" value="createHotelAdmin">

                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>Hotel name
                                    <input type="text" class="form-control" placeholder="Hotel name"
                                           name="insertHotelName" id="insertHotelName" required
                                           pattern="[a-zA-Zа-яА-Я\s]{1,}">
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>City
                                    <input type="text" class="form-control" placeholder="City"
                                           name="insertCity" id="insertCity" required
                                           pattern="[a-zA-Z\s]{1,}">
                                </label>
                            </div>
                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label>Country
                                    <input type="text" class="form-control" placeholder="Country"
                                           name="insertCountry" id="insertCountry" required
                                           pattern="[a-zA-Z\s]{1,}">
                                </label>
                            </div>

                            <input type="button" class="btn btn-primary col-3" value="Add new hotel"
                                   id="requestCreateHotel">

                            <div class="col-12 justify-content-center" id="hotelTempCreate">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>