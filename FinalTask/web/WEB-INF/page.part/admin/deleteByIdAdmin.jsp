<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="<c:url value="/js/admin/deleteById.js"/>"></script>
<div class="card">
    <div class="card-header" id="headingThree">
        <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                    data-target="#collapseThree"
                    aria-expanded="false" aria-controls="collapseThree">
                Delete data
            </button>
        </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#adminAction">
        <div class="card-body">
            <form class="justify-content-center row pt-1 pb-1" id="deleteUserForm" novalidate>
                <input type="hidden" name="command" value="deleteUserAdmin">
                <label class="sr-only" for="deleteUserIdInput">User</label>
                <div class="input-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="deleteUserIdInput" name="deleteId"
                           placeholder="Enter wanted user Id" required>
                    <input id="requestDeleteUser" type="button" class="ml-1 btn btn-light" value="Permanent ban">
                    <input id="clearDeleteUser" type="button" class="ml-2 btn btn-light" value="Clear">
                </div>
                <div class="col-12 justify-content-center" id="userTempDelete">

                </div>
            </form>

            <form class="justify-content-center row pt-1 pb-1" id="deleteDiscountForm" novalidate>
                <input type="hidden" name="command" value="deleteDiscountAdmin">
                <label class="sr-only" for="deleteDiscountIdInput">User</label>
                <div class="input-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="deleteDiscountIdInput" name="deleteId"
                           placeholder="Enter wanted discount Id" required>
                    <input id="requestDeleteDiscount" type="button" class="ml-1 btn btn-dark" value="Delete discount">
                    <input id="clearDeleteDiscount" type="button" class="ml-2 btn btn-dark" value="Clear">
                </div>
                <div class="col-12 justify-content-center" id="discountTempDelete">

                </div>

            </form>

            <form class="justify-content-center row pt-1 pb-1" id="deleteOrderForm" novalidate>
                <input type="hidden" name="command" value="deleteOrderAdmin">
                <label class="sr-only" for="deleteOrderIdInput">User</label>
                <div class="input-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="deleteOrderIdInput" name="deleteId"
                           placeholder="Enter wanted order Id" required>
                    <input id="requestDeleteOrder" type="button" class="ml-1 btn btn-light" value="Delete order">
                    <input id="clearDeleteOrder" type="button" class="ml-2 btn btn-light" value="Clear">
                </div>

                <div class="col-12 justify-content-center" id="orderTempDelete">

                </div>

            </form>

            <form class="justify-content-center row pt-1 pb-1" id="deleteHotelForm" novalidate>
                <input type="hidden" name="command" value="deleteHotelAdmin">
                <label class="sr-only" for="deleteHotelIdInput">Hotel</label>
                <div class="input-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="deleteHotelIdInput" name="deleteId"
                           placeholder="Enter wanted hotel Id" required>
                    <input id="requestDeleteHotel" type="button" class="ml-1 btn btn-dark" value="Delete hotel">
                    <input id="clearDeleteHotel" type="button" class="ml-2 btn btn-dark" value="Clear">
                </div>

                <div class="col-12 justify-content-center" id="hotelTempDelete">

                </div>

            </form>


            <form class="justify-content-center row pt-1 pb-1" id="deleteHotelroomForm" novalidate>
                <label class="sr-only" for="deleteHotelroomIdInput">Hotelroom</label>
                <div class="input-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="deleteHotelroomIdInput" name="deleteId"
                           placeholder="Enter wanted hotelroom Id" required>
                    <input id="requestDeleteHotelroom" type="button" class="ml-1 btn btn-light" value="Delete hotelroom">
                    <input id="clearDeleteHotelroom" type="button" class="ml-2 btn btn-light" value="Clear">
                </div>

                <div class="col-12 justify-content-center" id="hotelroomTempDelete">

                </div>

            </form>

        </div>
    </div>
</div>
