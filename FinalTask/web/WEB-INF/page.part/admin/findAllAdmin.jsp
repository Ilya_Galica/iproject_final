<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="<c:url value="/js/admin/findAll.js"/>"></script>
<div class="card">
    <div class="card-header" id="headingOne">
        <h2 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">
                Look information
            </button>
        </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#adminAction">
        <div class="card-body ">

            <form action="<c:url value="/"/>" method="post">
                <input type="hidden" name="command" value="findAllUserAdmin">
                <button id="requestFindAllUser" type="button" class="btn btn-light" data-toggle="modal"
                        data-target="#showAllUserModal">
                    Users
                </button>
                <!-- User Modal -->

                <div class="modal fade" id="showAllUserModal" tabindex="-1" role="dialog"
                     aria-labelledby="showAllUserModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="showAllUserModalTitle">Users</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table id="showAllUserTable" class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Surname</th>
                                        <th scope="col">Patronymic</th>
                                        <th scope="col">Telephone</th>
                                        <th scope="col">Job</th>
                                        <th scope="col">Role</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

            <!-- Discount Modal  -->

            <form action="<c:url value="/"/>" method="post">
                <input type="hidden" name="command" value="findAllDiscountAdmin">
                <button id="requestFindAllDiscount" type="button" class="btn btn-dark" data-toggle="modal"
                        data-target="#showAllDiscountModal">
                    Discounts
                </button>

                <!-- User Modal -->

                <div class="modal fade" id="showAllDiscountModal" tabindex="-1" role="dialog"
                     aria-labelledby="showAllDiscountModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="showAllDiscountModalTitle">Discounts</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table id="showAllDiscountTable" class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">User's job</th>
                                        <th scope="col">Discount</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

            <!-- Order Modal -->

            <form action="<c:url value="/"/>" method="post">
                <input type="hidden" name="command" value="findAllOrderAdmin">
                <button id="requestFindAllOrder" type="button" class="btn btn-light" data-toggle="modal"
                        data-target="#showAllOrderModal">
                    Orders
                </button>


                <div class="modal fade" id="showAllOrderModal" tabindex="-1" role="dialog"
                     aria-labelledby="showAllOrderModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="showAllOrderModalTitle">Orders</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table id="showAllOrderTable" class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Discount Id</th>
                                        <th scope="col">User Id</th>
                                        <th scope="col">Arrival date</th>
                                        <th scope="col">Departure date</th>
                                        <th scope="col">Room Id</th>
                                        <th scope="col">RESULTS</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

            <!--  Hotel Modal -->

            <form action="<c:url value="/"/>" method="post">
                <input type="hidden" name="command" value="findAllHotelAdmin">
                <button id="requestFindAllHotel" type="button" class="btn btn-dark" data-toggle="modal"
                        data-target="#showAllHotelModal">
                    Hotels
                </button>


                <div class="modal fade" id="showAllHotelModal" tabindex="-1" role="dialog"
                     aria-labelledby="showAllHotelModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="showAllHotelModalTitle">Hotels</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table id="showAllHotelTable" class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Hotel name</th>
                                        <th scope="col">City</th>
                                        <th scope="col">Country</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

            <!-- Hotelroom Modal -->

            <form action="<c:url value="/"/>" method="post">

                <input type="hidden" name="command" value="findAllHotelroomAdmin">
                <button id="requestFindAllHotelroom" type="button" class="btn btn-light" data-toggle="modal"
                        data-target="#showAllHotelroomModal">
                    Hotelrooms
                </button>


                <div class="modal fade" id="showAllHotelroomModal" tabindex="-1" role="dialog"
                     aria-labelledby="showAllHotelroomModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable  modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="showAllHotelroomModalTitle">Hotelrooms</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table id="showAllHotelroomTable" class="table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Room number</th>
                                        <th scope="col">Hotel Id</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Room capacity</th>
                                        <th scope="col">Comfort level</th>
                                        <th scope="col">Description</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>