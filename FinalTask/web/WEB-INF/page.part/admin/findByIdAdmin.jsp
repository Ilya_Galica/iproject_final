<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="<c:url value="/js/admin/findById.js"/>"></script>
<div class="card">
    <div class="card-header" id="headingTwo">
        <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                    data-target="#collapseTwo"
                    aria-expanded="false" aria-controls="collapseTwo">
                Look up information by id
            </button>
        </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#adminAction">
        <div class="card-body">
            <form class="justify-content-center row pt-1 pb-1 needs-validation" id="findUserForm"
                  novalidate>
                <label class="sr-only" for="findUserIdInput">User</label>
                <div class="input-group justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="findUserIdInput" name="findId"
                           placeholder="Enter wanted user Id" required>
                    <input id="requestFindUser" type="button" class="ml-1 btn btn-light" value="Find user">
                    <input id="clearFindUser" type="button" class="ml-2 btn btn-light" value="Clear">
                </div>
                <div class="col-12 justify-content-center" id="userTemp">

                </div>
            </form>

            <form class="justify-content-center row pt-1 pb-1" id="findDiscountForm" novalidate>
                <label class="sr-only" for="findDiscountIdInput">User</label>
                <div class="form-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="findDiscountIdInput" name="findId"
                           placeholder="Enter wanted discount Id" required>
                    <input id="requestFindDiscount" type="button" class="ml-1 btn btn-dark" value="Find discount">
                    <input id="clearFindDiscount" type="button" class="ml-2 btn btn-dark" value="Clear">
                </div>
                <div class="col-12 justify-content-center" id="discountTemp">

                </div>

            </form>

            <form class="justify-content-center row pt-1 pb-1" id="findOrderForm" novalidate>
                <label class="sr-only" for="findOrderIdInput">User</label>
                <div class="input-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="findOrderIdInput" name="findId"
                           placeholder="Enter wanted order Id" required>
                    <div class="invalid-feedback">
                        Only digits
                    </div>
                    <input id="requestFindOrder" type="button" class="ml-1 btn btn-light" value="Find order">
                    <input id="clearFindOrder" type="button" class="ml-2 btn btn-light" value="Clear">
                </div>

                <div class="col-12 justify-content-center" id="orderTemp">

                </div>

            </form>

            <form class="justify-content-center row pt-1 pb-1 needs-validation" id="findHotelForm" novalidate>
                <label class="sr-only" for="findHotelIdInput">User</label>
                <div class="input-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="findHotelIdInput" name="findId"
                           placeholder="Enter wanted hotel Id" required>
                    <div class="invalid-feedback">
                        Only digits
                    </div>
                    <input id="requestFindHotel" type="button" class="ml-1 btn btn-dark" value="Find hotel">
                    <input id="clearFindHotel" type="button" class="ml-2 btn btn-dark" value="Clear">
                </div>

                <div class="col-12 justify-content-center" id="hotelTemp">

                </div>

            </form>


            <form class="justify-content-center row pt-1 pb-1 needs-validation" id="findHotelroomForm" novalidate>
                <label class="sr-only" for="findHotelroomIdInput">User</label>
                <div class="input-group mb-2 justify-content-center row">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Id</div>
                    </div>
                    <input pattern="[\d]+" type="text" id="findHotelroomIdInput" name="findId"
                           placeholder="Enter wanted hotelroom Id" required>
                    <div class="invalid-feedback">
                        Only digits
                    </div>
                    <input id="requestFindHotelroom" type="button" class="ml-1 btn btn-light" value="Find hotelroom">
                    <input id="clearFindHotelroom" type="button" class="ml-2 btn btn-light" value="Clear">
                </div>

                <div class="col-12 justify-content-center" id="hotelroomTemp">

                </div>

            </form>
        </div>
    </div>
</div>
