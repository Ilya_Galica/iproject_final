<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <div class="modal fade" id="emailExistModal" tabindex="-1" role="dialog" aria-labelledby="emailExistModalText"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><fmt:message key="warning"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="userMessage" class="modal-body">
                    <fmt:message key="owner.of.email"/>
                </div>
                <div class="input-group input-group-sm mb-3 col-10">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm"> <fmt:message
                            key="enter"/></span>
                    </div>
                    <input id="confirmEmailInput" type="text" class="form-control" aria-label="Small"
                           aria-describedby="inputGroup-sizing-sm">
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmEmail" class="btn btn-primary" data-dismiss="modal"><fmt:message
                            key="check"/>
                    </button>
                </div>
            </div>
        </div>
    </div>
</fmt:bundle>