<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="<c:url value="/css/library/bootstrap/bootstrap.css"/>">

<script src="<c:url value="/js/changeLang.js"/>"></script>

<script src="<c:url value="/js/authentication.js"/>"></script>

<script src="<c:url value="/js/admin/notPass.js"/>"></script>

<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>

<link href="<c:url value="/css/library/fontawesome/css/all.css"/>" rel="stylesheet">

<script src="<c:url value="/js/bootstrapValidator.js"/>"></script>


<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <header class="sticky-top navbar navbar-expand-lg navbar-light bg-primary col-lg-12 col-md-12 col-sm-12 m-0 p-0">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNavbar"
                aria-controls="headerNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse row justify-content-around m-0" id="headerNavbar">
            <a href="<c:url value="/index.jsp"/>" class="mt-1">
                <h2 class="logo navbar-brand text-white">
                    <i class="fas fa-umbrella-beach"></i>OneDirection</h2>
            </a>
            <div class="row">
                <div class="dropdown p-0">
                    <a class="dropdown-toggle nav-item text-white p-0 col-2" href="#" role="button"
                       id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-language"></i><fmt:message key="change.language"/>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <form  id="changeLanguage">
                                <%--<input type="hidden" name="command" value="changeLanguage">--%>
                            <input type="button" name="localeRU" class="dropdown-item" id="ru" value="ru">
                            <input type="button" name="localeEN" class="dropdown-item" id="en" value="en">
                        </form>
                    </div>
                </div>
                <button class="nav-item btn-link text-white ml-2" type="button"
                        data-toggle="modal" data-target="#registrate"
                        id="signUpButton">
                    <i class="fas fa-user"></i><fmt:message key="authorisation"/>
                </button>
                <div class="dropdown p-0" id="userDropdownDiv" style="display: none">
                    <button class="nav-item text-white dropdown-toggle p-0 ml-2" id="authenticationButton"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <form id="profileExitForm" method="post" action="<c:url value="/user"/>">
                            <input class="dropdown-item" type="button" id="profileExitInput" form="profileExitForm"
                                   value="<fmt:message key="exit"/>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <jsp:include page="registratePage.jsp"/>
</fmt:bundle>

