<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer class="row justify-content-center m-0">
    <a class="social-media" href="">
        <i class="fab fa-vk"></i>
    </a>
    <a class="social-media" href="">
        <i class="fab fa-facebook-f"></i>
    </a>
    <a class="social-media" href="">
        <i class="fab fa-google"></i>
    </a>
    <a class="social-media" href="https://www.instagram.com/ilya9724/">
        <i class="fab fa-instagram"></i>
    </a>
    <a class="social-media" href="">
        <i class="fab fa-youtube"></i>
    </a>
</footer>