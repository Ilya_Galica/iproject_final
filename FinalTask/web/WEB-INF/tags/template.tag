<%--@elvariable id="user" type="dao.entity.impl.User"--%>
<!DOCTYPE html>
<%@tag description="Template Site tag" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="title" fragment="true" %>

<head>
    <title>
        <jsp:invoke fragment="title"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="Pragma" content="no-cache">

    <meta http-equiv="Cache-Control" content="no-cache">

    <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

    <link rel="shortcut icon" href="<c:url value="/favicon.ico"/>"/>

    <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/css/library/jquery-ui.css"/>">

    <link rel="stylesheet" href="<c:url value="/css/autocomplete.css"/>">

    <script src="<c:url value="/js/library/jquery-ui/jquery-ui.js"/> "></script>

    <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

    <script src="<c:url value="/js/library/bootstrap/bootstrap.js"/>"></script>

    <link href="<c:url value="/css/library/fontawesome/css/all.css"/>" rel="stylesheet">

    <link rel="stylesheet" href="<c:url value="/css/library/bootstrap/bootstrap.css"/>">

    <script src="<c:url value="/js/bootstrapValidator.js"/>"></script>

</head>

<body>
<c:set var="pageURL" value="/index"/>

<header class="sticky-top navbar navbar-expand-lg navbar-light bg-primary col-lg-12 col-md-12 col-sm-12 m-0 p-0">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNavbar"
            aria-controls="headerNavbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse row justify-content-around m-0" id="headerNavbar">
        <a href="<c:url value="#"/>" class="mt-1">
            <h2 class="logo navbar-brand text-white">
                <i class="fas fa-umbrella-beach"></i>OneDirection</h2>
        </a>
        <div class="row">
            <div class="dropdown p-0">
                <button class="nav-item text-white dropdown-toggle p-0 ml-2" id="authenticationButton"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">${user.name}</button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <form id="profileExitForm" method="post" action="controller">
                        <input type="hidden" name="command" value="profileExit">
                        <input type="hidden" form="profileExitForm" name="pageName" value="${pageURL}">
                        <input class="dropdown-item" type="button" id="profileExitInput" form="profileExitForm"
                               value="Exit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>

<jsp:doBody/>


</body>
