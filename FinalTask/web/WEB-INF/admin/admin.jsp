<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags" %>
<header:template>
    <jsp:attribute name="title">Admin</jsp:attribute>

    <jsp:body>
<c:if test="${sessionScope.user==null}">
   <
</c:if>
        <span id="isAl" hidden>${sessionScope.user}</span>
        <script src="<c:url value="/js/admin/notPass.js"/>"></script>
        <div class="accordion" id="adminAction">

            <jsp:include page="/WEB-INF/page.part/admin/findAllAdmin.jsp"/>

            <jsp:include page="/WEB-INF/page.part/admin/findByIdAdmin.jsp"/>

            <jsp:include page="/WEB-INF/page.part/admin/deleteByIdAdmin.jsp"/>

            <jsp:include page="/WEB-INF/page.part/admin/createRecordAdmin.jsp"/>

        </div>
    </jsp:body>
</header:template>

