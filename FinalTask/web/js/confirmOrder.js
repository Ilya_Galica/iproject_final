jQuery(function ($) {
    const ORDER = 'order';

    var confirmModal = $('#confirmModal');


    if (getCookie(ORDER) === "" && sessionStorage.getItem(ORDER) === null) {
        sessionStorage.setItem(ORDER, ORDER);
        setCookie(ORDER, ORDER, 20);
    } else if (getCookie(ORDER) === "" && sessionStorage.getItem(ORDER) !== null) {
        sessionStorage.removeItem(ORDER);
        $.ajax({
            method: "POST",
            url: "/controller",
            cache: false,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
            data: {
                ajaxCommand: "deleteOrder",
            },
            success: function () {
                window.location.reload();
                // window.location.replace("/index.jsp");
            }
        });
    }

    function setCookie(cname, cvalue, mins) {
        var d = new Date();
        d.setTime(d.getTime() + (mins * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    var inFormOrLink = false;

    $('#confirmOrder').on('click', function () {
        $.ajax({
            method: "POST",
            url: "/controller",
            cache: false,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
            data: {
                ajaxCommand: "confirmOrder",
            },
            success: function (responseText) {
                confirmModal.modal('show');
                console.log(responseText);
                $(window).off("beforeunload");
                sessionStorage.removeItem('order');
                setCookie('order', 'order', 0);
                confirmModal.on('hidden.bs.modal', function () {
                    window.location.replace("/index.jsp");
                });
            },
            error: function (request) {
                alert(request + " Что-то тут не так")
            }
        });
    });

    // $(window).on('unload', function () {
    //     $.ajax({
    //         method: "POST",
    //         url: "/controller",
    //         cache: false,
    //         contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
    //         data: {
    //             ajaxCommand: "deleteOrder",
    //         }
    //     });
    //     setCookie('order', 'order', 0);
    // });
    //
    // $(document).on("submit", "form", function(event){
    //     window.onbeforeunload = null;
    // });
    //
    // $(window).on("beforeunload", function () {
    //     return "text";
    // });



});