$(document).ready(function ($) {
    var selectExpected = $('span#amountControl');
    var relevantValue = selectExpected.text();
    if (relevantValue !== "") {
        $('select#amount option[value="' + relevantValue + '"]').prop('selected', true);
    }

    $(function () {
        $("#city").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/resources/json/city.json',
                    data: request,
                    dataType: "json",
                    success: function (data) {
                        response($.ui.autocomplete.filter(data, request.term));
                    },
                    error: function () {
                        response([]);
                    }
                })
            }
        });
    });
});
