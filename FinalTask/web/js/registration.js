const FOCUS = 'focus';
const BLUR = 'blur';
const CLICK = 'click';
const REGISTRATION = '.registration-visability';
const LOGIN = '.login-visability';
const SWIPE = '.swipe';
const telephone = "#telephoneInput";

jQuery(document).ready(function ($) {
    $(function () {
        function maskPhone() {
            var country = $('#country option:selected').val();
            switch (country) {
                case "ru":
                    $(telephone).mask("+7(999)999-99-99").focus();
                    break;
                case "ua":
                    $(telephone).mask("+380(999)999-99-99").focus();
                    break;
                case "by":
                    $(telephone).mask("+375(99)999-99-99").focus();
                    break;
            }
        }

        maskPhone();
        $('#country').change(function () {
            maskPhone();

        });
    });
});


jQuery(document).ready(function ($) {


});


jQuery(function ($) {
    $(".validation-data input").on(FOCUS, function () {
        $(this).addClass(FOCUS);
    })
});

jQuery(function ($) {
    $(".validation-data input").on(BLUR, function () {
        if ($(this).val() === "") {
            $(this).removeClass(FOCUS);
        }
    })
});

jQuery(function ($) {
    checkFunction(SWIPE, LOGIN, REGISTRATION);
});

var checkFunction = function (swipeClassName, hideClassName, registrateClassName) {
    $(swipeClassName).on(CLICK, function () {
        $(hideClassName).toggle();
        $(registrateClassName).toggle();
    })
};

jQuery(function ($) {
    $('#registration').on('submit', function () {
        var pass = $('.registration-visability .validation-data input[name="password"]').val();
        var confPass = $('.registration-visability .validation-data input[name="confirmPassword"]').val();
        if (pass !== confPass) {
            $('')
        } else {
            alert('pass');
        }
    });
});


jQuery(function ($) {
    $('#addEmail').on(CLICK, function () {
        var parent = $('.extra-email');
        var validationData = $('<div class=\"validation-data extra\"></div>');
        var inputEmail = $('<input type=\"email\" name=\"extraEmail\"'
            + 'pattern=\"^[\w]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$\" required'
            + ' min=\"6\" max=\"64\">');
        var spanPtaceholder = $('<span data-palceholder=\"Extra email\"></span>');
        var spanExplain = $('<span class=\"input-explain\">example@mail.com</span>');
        var removeBtn = $('<input type=\"button\" class=\"removeEmail\" value=\"-\">');
        removeBtn.click(function () {
            $(this).parent().remove();
        });
        validationData.append(inputEmail);
        validationData.append(spanPtaceholder);
        validationData.append(spanExplain);
        validationData.append(removeBtn);
        // validationData.appendTo(parent);
        parent.append(validationData);
    });
});
