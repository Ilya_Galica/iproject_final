jQuery(function ($) {

    $(function () {
        function maskPhone() {
            let country = $('#yourCountry option:selected').val();
            let telephone = $('#yourTelephone');
            switch (country) {
                case "ru":
                    telephone.mask("+7(999)999-99-99").focus();
                    break;
                case "ua":
                    telephone.mask("+380(999)999-99-99").focus();
                    break;
                case "by":
                    telephone.mask("+375(99)999-99-99").focus();
                    break;
            }
        }

        maskPhone();
        $('#yourCountry').change(function () {
            maskPhone();

        });
    });

});



