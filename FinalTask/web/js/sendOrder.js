$(document).ready(function ($) {
    var createOrderBtn = $("#createOrder");

    var createOrder = function (event) {
        var nameInputOrder = $('#nameInputOrder');
        var surnameInputOrder = $('#surnameInputOrder');
        var patronymicInputOrder = $('#patronymicInputOrder');
        var emailInputOrder = $('#emailInputOrder');
        var telephoneInputOrder = $('#telephoneInputOrder');
        var jobInputOrder = $('#jobInputOrder');

        if (document.getElementById('orderForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('orderForm').checkValidity() === true) {
            $.ajax({
                method: "POST",
                url: '/controller',
                cache: false,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
                data: {
                    ajaxCommand: "emailExist",
                    name: nameInputOrder.val(),
                    surname: surnameInputOrder.val(),
                    patronymic: patronymicInputOrder.val(),
                    email: emailInputOrder.val(),
                    telephone: telephoneInputOrder.val(),
                    jobPlace: jobInputOrder.val(),
                },
                success: function (resp) {
                    console.log(resp);
                    if (resp != null) {
                        var response = resp.toString();
                        if (response.includes("ERROR")) {
                            $('<h5 style="color: red">' + response
                                .replace("ERROR", "").replace("\"", "") + '</h5>')
                                .insertAfter('#orderForm h3');
                        } else if (response !== "") {
                            $('#createOrderModal').modal('toggle');
                            $('#emailExistModal').modal('toggle');
                            var confirmEmailBtn = $('#confirmEmail');
                            confirmEmailBtn.on('click', function () {
                                $.ajax({
                                    method: "POST",
                                    url: '/controller',
                                    cache: false,
                                    contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
                                    data: {
                                        ajaxCommand: "emailExist",
                                        inputKey: $('#confirmEmailInput').val(),
                                        name: nameInputOrder.val(),
                                        surname: surnameInputOrder.val(),
                                        patronymic: patronymicInputOrder.val(),
                                        email: emailInputOrder.val(),
                                        telephone: telephoneInputOrder.val(),
                                        jobPlace: jobInputOrder.val(),
                                    },
                                    success: function (response) {
                                        if (response === "\"ok\"") {
                                            $('#wrongConfirm').hide();
                                            sessionStorage.setItem('goForward', 'true')

                                        } else {
                                            $('#createOrderModal').modal('toggle');
                                            $('#emailExistModal').modal('toggle');
                                            $('#wrongConfirm').show();
                                        }
                                    }
                                });
                            });
                        } else {
                            sessionStorage.setItem('goForward', 'true')
                        }
                    }
                },
                error: function (request) {
                    alert(request)
                }
            })
        }
        document.getElementById('orderForm').classList.add('was-validated');
    };

    $(document).ajaxComplete(function () {
        if (sessionStorage.getItem('goForward') === 'true') {
            $('#orderForm').submit();
            sessionStorage.setItem('goForward', 'false')
        }
    });


    createOrderBtn.on('click', createOrder);

    // $(window).on('load',function () {
    //     console.log($('#orderError').text());
    //    if ($('#orderError').text()!==""){
    //        $('#createOrderModal').modal('toggle');
    //    }
    // });

    var createOrderAuthBtn = $('#createOrderAuth');

    var createOrderAuth = function () {
        $.ajax({
            method: "POST",
            url: '/controller',
            cache: false,
            headers: {
                'Cache-Control': 'no-cache',
                'Pragma': 'no-cache',
                'Expires': 0
            },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
            data: {
                ajaxCommand: "emailExist",
                auth: "authentication"
            },
            success: function () {
                sessionStorage.setItem('goForward', 'true')
            },
            error: function (request) {
                alert(request)
            }
        });
    };

    createOrderAuthBtn.on('click', createOrderAuth);
});
jQuery(function ($) {
    $(function () {
        function maskPhone() {
            let country = $('#countryOrder option:selected').val();
            let telephone = $('#telephoneInputOrder');
            switch (country) {
                case "ru":
                    telephone.mask("+7(999)999-99-99").focus();
                    break;
                case "ua":
                    telephone.mask("+380(999)999-99-99").focus();
                    break;
                case "by":
                    telephone.mask("+375(99)999-99-99").focus();
                    break;
            }
        }

        maskPhone();
        $('#countryOrder').change(function () {
            maskPhone();

        });
    });
});