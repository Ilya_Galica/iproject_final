$(document).ready(function ($) {

    var signInBtn = $("#signIn");

    var signUpBtn = $("#signUpBtn");

    var exitBtn = $("#exit");

    var signUpOpenModalBtn = $("#signUpButton");

    var authBtn = $("#userDropdownDiv");

    var emailInputLogin;

    var passwordInputLogin;

    var autoCheck = "check";
    var signIn = function (event) {
        var authenticationButton = $('#authenticationButton');
        emailInputLogin = $('#emailInputLogin').val();
        passwordInputLogin = $('#passwordInputLogin').val();
        if (document.getElementById('signInForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('signInForm').checkValidity() === true) {
            $.ajax({
                url: "/controller",
                method: "POST",
                data: {
                    ajaxCommand: "signIn",
                    loginEmail: emailInputLogin,
                    password: passwordInputLogin
                },
                success: function (resp) {
                    if (resp != null) {
                        if (resp.toString().includes("ERROR")) {
                            $('#signInForm h5').remove();
                            console.log("Ne och" + resp);
                            $('<h5 style="color: red">' + resp.toString()
                                .replace("ERROR", "").replace("\"", "") + '</h5>')
                                .insertAfter('#signInForm h3');
                        } else {
                            var response = JSON.parse(resp);
                            console.log(response.role);
                            if (response.role === 'USER') {
                                if ($('input').is('#showHotelroomPageURL')) {
                                    location.reload();
                                }
                                authenticationButton.text(response.name);
                                authBtn.show();
                                signUpOpenModalBtn.hide();
                                $('#registrate').modal('hide')
                            } else {
                                window.location.replace('/admin.html');
                            }
                        }
                    }
                },
                error: function (resp) {
                    console.log("Ne och" + resp.toString())
                }
            });
        }
        document.getElementById('signInForm').classList.add('was-validated');
    };
    signInBtn.on("click", signIn);

    exitBtn.on("click", signIn);

    var signInDoc = function () {
        var authenticationButton = $('#authenticationButton');
        emailInputLogin = $('#emailInputLogin').val();
        passwordInputLogin = $('#passwordInputLogin').val();

        $.ajax({
            url: "/controller",
            method: "POST",
            data: {
                ajaxCommand: "signIn",
                autoCheck: autoCheck
            },
            success: function (resp) {
                console.log(resp);
                if (resp.toString() === "Авторизация"
                    || resp.toString() === "Authorisation") {
                    authenticationButton.text(resp.toString());
                    if ($('input').is('#personalUserPageCheck')) {
                        window.location.reload();
                    }
                } else {
                    authenticationButton.text(resp.toString());
                    authBtn.show();
                    signUpOpenModalBtn.hide();
                }
            },
            error: function (resp) {
                console.log("Ne och" + resp.toString())
            }
        });
    };
    $(document).ready(signInDoc);


    var signUp = function (event) {

        var nameInput = $('#nameInput').val();
        var surnameInput = $('#surnameInput').val();
        var inputPatronymic = $('#inputPatronymic').val();
        var emailInputRegister = $('#emailInputRegister').val();
        var passwordInputRegister = $('#passwordInputRegister').val();
        var telephoneInput = $('#telephoneInput').val();
        var jobInput = $('#jobInput').val();

        if (document.getElementById('signUpForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('signUpForm').checkValidity() === true) {
            var authenticationButton = $('#authenticationButton');
            $.ajax({
                url: "/controller",
                method: "POST",
                data: {
                    ajaxCommand: "signUp",
                    name: nameInput,
                    surname: surnameInput,
                    patronymic: inputPatronymic,
                    email: emailInputRegister,
                    password: passwordInputRegister,
                    telephone: telephoneInput,
                    jobPlace: jobInput
                },
                success: function (resp) {
                    console.log(resp);
                    if (resp != null) {
                        if (resp.toString().includes("ERROR")) {
                            $('#signUpForm h5').remove();
                            console.log("Ne och" + resp);
                            $('<h5 style="color: red">' + resp.toString()
                                .replace("ERROR", "").replace("\"", "") + '</h5>')
                                .insertAfter('#signUpForm h3');
                        } else if (resp !== "") {
                            $('#registrate').modal('toggle');
                            $('#emailExistModal').modal('toggle');
                            var confirmEmailBtn = $('#confirmEmail');

                            confirmEmailBtn.on('click', function () {
                                $.ajax({
                                    method: "POST",
                                    url: '/controller',
                                    cache: false,
                                    contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
                                    data: {
                                        ajaxCommand: "signUp",
                                        inputKey: $('#confirmEmailInput').val(),
                                        name: nameInput,
                                        surname: surnameInput,
                                        patronymic: inputPatronymic,
                                        email: emailInputRegister,
                                        password: passwordInputRegister,
                                        telephone: telephoneInput,
                                        jobPlace: jobInput
                                    },
                                    success: function (resp) {
                                        var registrate = $('#registrate');
                                        if (resp !== "" && resp.toString().length !== 9) {
                                            if ($('input').is('#showHotelroomPageURL')) {
                                                location.reload();
                                            }
                                            response = JSON.parse(resp);
                                            authenticationButton.text(response.name);
                                            authBtn.show();
                                            signUpOpenModalBtn.hide();
                                            registrate.modal('hide');
                                        } else {
                                            registrate.modal('toggle');
                                            $('#wrongConfirm').show();
                                        }

                                    }
                                });
                            });
                        } else {
                            var response = JSON.parse(resp);
                            if ($('input').is('#showHotelroomPageURL')) {
                                location.reload();
                            }
                            authenticationButton.text(response.name);
                            authBtn.show();
                            signUpOpenModalBtn.hide();
                            $('#registrate').modal('hide');
                        }
                    }
                },
                error: function (resp) {
                    console.log("Ne och" + resp);
                    $('<h3 style="color: red">' + resp + '</h3>').insertAfter('#signUpForm:first-child');
                }
            });
        }
        document.getElementById('signUpForm').classList.add('was-validated');
    };

    signUpBtn.on("click", signUp);

    exitBtn.on("click", signUp);

    var showPasswordClass = $('.show-password');

    var visiblePasswordClass = $('.visible-password');

    showPasswordClass.on('mousedown', function () {
        showPasswordClass.find("i").removeClass('fa-eye').addClass('fa-eye-slash');
        visiblePasswordClass.prop("type", "text");
    });

    showPasswordClass.on('mouseup', function () {
        showPasswordClass.find("i").removeClass('fa-eye-slash').addClass('fa-eye');
        visiblePasswordClass.prop("type", "password");
    });

    if ($('#currentPage').text() === '/hotels/hotelroom/order/confirmOrderPage.jsp') {
        $('#userDropdownDiv').on('click', function (event) {
            event.stopPropagation();
            event.preventDefault();
        })
    }
});

