jQuery(function ($) {
    var $start = $('#start-date-time-datepicker'),
        $end1 = $('#end-date-time-datepicker');

    let locale = $('#pageLanguage').val();

    $start.datetimepicker({
        format: 'DD/MM/YYYY',
        locale: locale,
        minDate: moment().startOf('d')
    });

    $end1.datetimepicker({
        minDate: $start.on("change.datetimepicker", function (e) {
            $end1.datetimepicker('minDate', e.date);
            console.log(Date.now().toString());
        }),
        locale: locale,
        format: 'DD/MM/YYYY'
    });

});