$(document).ready(function ($) {
    var exit = function () {
        $.ajax({
            url: "/user",
            method: "POST",
            data: {
                ajaxCommand: "profileExit",
            },
            success: function () {
                location.replace("/index.jsp");
            }
        });
    };

    $('#profileExitInput').on('click', exit);

});