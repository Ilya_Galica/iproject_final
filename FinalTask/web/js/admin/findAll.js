
$(document).ready(function ($) {
    var findAllUserBtn = $('#requestFindAllUser');

    var findAllUser = function sendData(name) {
        console.log(name);
        $.ajax({
            url: "controller",
            method: "POST",
            data: {
                ajaxCommand: "findAllUserAdmin"
            },
            success: function (resp) {
                var table = $('#showAllUserTable');
                $('#showAllUserTable .recentDelete').remove();
                $.each(resp, function (index, item) {
                    $('<tr class="recentDelete">').appendTo(table)
                        .append($('<th>').text(item.id))
                        .append($('<td>').text(item.email))
                        .append($('<td>').text(item.name))
                        .append($('<td>').text(item.surname))
                        .append($('<td>').text(item.patronymic))
                        .append($('<td>').text(item.telephone))
                        .append($('<td>').text(item.password))
                        .append($('<td>').text(item.jobPlace))
                });
            }
        });
    };

    findAllUserBtn.on('click', findAllUser);

    var findAllDiscountBtn = $('#requestFindAllDiscount');

    var findAllDiscount = function sendData(name) {
        console.log(name);
        $.ajax({
            url: "controller",
            method: "POST",
            data: {
                ajaxCommand: "findAllDiscountAdmin"
            },
            success: function (resp) {
                var table = $('#showAllDiscountTable tbody');
                $('#showAllDiscountTable .recentDelete').remove();
                $.each(resp, function (index, item) {
                    $('<tr class="recentDelete">').appendTo(table)
                        .append($('<th>').text(item.id))
                        .append($('<td>').text(item.userJobPlace))
                        .append($('<td>').text(item.discountValueForJobPlace))
                });
            }
        });
    };

    findAllDiscountBtn.on('click', findAllDiscount);

    var findAllOrderBtn = $('#requestFindAllOrder');

    var findAllOrder = function sendData(name) {
        console.log(name);
        $.ajax({
            url: "controller",
            method: "POST",
            data: {
                ajaxCommand: "findAllOrderAdmin"
            },
            success: function (resp) {
                var table = $('#showAllOrderTable tbody');
                $('#showAllOrderTable .recentDelete').remove();
                $.each(resp, function (index, item) {
                    $('<tr class="recentDelete">').appendTo(table)
                        .append($('<th>').text(item.id))
                        .append($('<td>').text((item.discount != null)
                            ? item.discount.id
                            : ""))
                        .append($('<td>').text(item.user.id))
                        .append($('<td>').text(item.arrivalData))
                        .append($('<td>').text(item.departureData))
                        .append($('<td>').text(item.hotelroom.id))
                        .append($('<td>').text(item.totalPrice))
                });
            }
        });
    };

    findAllOrderBtn.on('click', findAllOrder);

    var findAllHotelBtn = $('#requestFindAllHotel');

    var findAllHotel = function sendData(name) {
        $.ajax({
            url: "controller",
            method: "POST",
            data: {
                ajaxCommand: "findAllHotelAdmin"
            },
            success: function (resp) {
                var table = $('#showAllHotelTable tbody');
                $('#showAllHotelTable .recentDelete').remove();
                $.each(resp, function (index, item) {
                    $('<tr class="recentDelete">').appendTo(table)
                        .append($('<th>').text(item.id))
                        .append($('<td>').text(item.name))
                        .append($('<td>').text(item.city))
                        .append($('<td>').text(item.country))
                });
            }
        });
    };

    findAllHotelBtn.on('click', findAllHotel);

    var findAllHotelroomBtn = $('#requestFindAllHotelroom');

    var findAllHotelroom = function sendData(name) {
        console.log(name);
        $.ajax({
            url: "controller",
            method: "POST",
            data: {
                ajaxCommand: "findAllHotelroomAdmin"
            },
            success: function (resp) {
                var table = $('#showAllHotelroomTable tbody');
                $('#showAllHotelroomTable .recentDelete').remove();
                $.each(resp, function (index, item) {
                    $('<tr class="recentDelete">').appendTo(table)
                        .append($('<th>').text(item.id))
                        .append($('<th>').text(item.number))
                        .append($('<td>').text((item.hotel != null)
                            ? item.hotel.id
                            : ""))
                        .append($('<td>').text(item.price))
                        .append($('<td>').text(item.peopleCapacity))
                        .append($('<td>').text(item.comfort))
                        .append($('<td>').text(item.description))
                });
            }
        });
    };

    findAllHotelroomBtn.on('click', findAllHotelroom);
});