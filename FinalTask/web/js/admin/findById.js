$(document).ready(function ($) {
    var findUserBtn = $('#requestFindUser');

    var findUser = function sendData(event) {
        if (document.getElementById('findUserForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('findUserForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "findUserAdmin",
                    findId: $('#findUserIdInput').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#userTemp');
                    $('#userTemp table').remove();
                    $('#userTemp h6').remove();
                    if (resp != null && $('#findUserIdInput').val() !== "") {
                        var table = $('<table class="table">');
                        var tbody = $('<tbody>');
                        var thead = $('<thead class="thead-dark">');
                        $('<tr class="recentDelete">').appendTo(thead)
                            .append($('<th scope="col">').text('Id'))
                            .append($('<th scope="col">').text('Email'))
                            .append($('<th scope="col">').text('Name'))
                            .append($('<th scope="col">').text('Surname'))
                            .append($('<th scope="col">').text('Patronymic'))
                            .append($('<th scope="col">').text('Telephone'))
                            .append($('<th scope="col">').text('Job'))
                            .append($('<th scope="col">').text('Role'));
                        thead.appendTo(table);
                        tbody.appendTo(table);
                        $.each(resp, function (index, item) {
                            console.log(resp);
                            console.log(index);
                            $('<tr class="recentDelete">').appendTo(tbody)
                                .append($('<th>').text(item.id))
                                .append($('<td>').text(item.email))
                                .append($('<td>').text(item.name))
                                .append($('<td>').text(item.surname))
                                .append($('<td>').text(item.patronymic))
                                .append($('<td>').text(item.telephone))
                                .append($('<td>').text(item.password))
                                .append($('<td>').text(item.jobPlace))
                        });
                        table.appendTo(requiredDiv);
                    } else {
                        requiredDiv.append($('<h6>').text("Didn't find such record"))
                    }
                }
            });
        }
        document.getElementById('findUserForm').classList.add('was-validated');
    };

    findUserBtn.on('click', findUser);

    var findDiscountBtn = $('#requestFindDiscount');

    var findDiscount = function sendData(event) {
        if (document.getElementById('findDiscountForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('findDiscountForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "findDiscountAdmin",
                    findId: $('#findDiscountIdInput').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#discountTemp');
                    $('#discountTemp table').remove();
                    $('#discountTemp h6').remove();
                    if (resp != null && $('#findDiscountIdInput').val() !== "") {
                        var table = $('<table class="table">');
                        var tbody = $('<tbody>');
                        var thead = $('<thead class="thead-light">');
                        $('<tr class="recentDelete">').appendTo(thead)
                            .append($('<th scope="col">').text('Id'))
                            .append($('<th scope="col">').text('User job'))
                            .append($('<th scope="col">').text('Discount'));
                        thead.appendTo(table);
                        tbody.appendTo(table);
                        $.each(resp, function (index, item) {
                            $('<tr class="recentDelete">').appendTo(table)
                                .append($('<th>').text(item.id))
                                .append($('<td>').text(item.userJobPlace))
                                .append($('<td>').text(item.discountValueForJobPlace))
                        });
                        table.appendTo(requiredDiv);
                    } else {
                        requiredDiv.append($('<h6>').text("Didn't find such record"))
                    }
                }
            });
        }
        document.getElementById('findDiscountForm').classList.add('was-validated');
    };

    findDiscountBtn.on('click', findDiscount);

    var findOrderBtn = $('#requestFindOrder');

    var findOrder = function sendData(event) {
        if (document.getElementById('findOrderForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('findOrderForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "findOrderAdmin",
                    findId: $('#findOrderIdInput').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#orderTemp');
                    $('#orderTemp table').remove();
                    $('#orderTemp h6').remove();
                    if (resp != null && $('#findOrderIdInput').val() !== "") {
                        var table = $('<table class="table">');
                        var tbody = $('<tbody>');
                        var thead = $('<thead class="thead-dark">');
                        $('<tr class="recentDelete">').appendTo(thead)
                            .append($('<th scope="col">').text('Id'))
                            .append($('<th scope="col">').text('Discount Id'))
                            .append($('<th scope="col">').text('User Id'))
                            .append($('<th scope="col">').text('Arrival date'))
                            .append($('<th scope="col">').text('Departure date'))
                            .append($('<th scope="col">').text('Room Id'))
                            .append($('<th scope="col">').text('RESULT'));
                        thead.appendTo(table);
                        tbody.appendTo(table);
                        $.each(resp, function (index, item) {
                            $('<tr class="recentDelete">').appendTo(table)
                                .append($('<th>').text(item.id))
                                .append($('<td>').text((item.discount != null)
                                    ? item.discount.id
                                    : ""))
                                .append($('<td>').text(item.user.id))
                                .append($('<td>').text(item.arrivalData))
                                .append($('<td>').text(item.departureData))
                                .append($('<td>').text(item.hotelroom.id))
                                .append($('<td>').text(item.totalPrice))
                        });
                        table.appendTo(requiredDiv);
                    } else {
                        requiredDiv.append($('<h6>').text("Didn't find such record"))
                    }
                }
            });
        }
        document.getElementById('findOrderForm').classList.add('was-validated');
    };

    findOrderBtn.on('click', findOrder);

    var findHotelBtn = $('#requestFindHotel');

    var findHotel = function sendData(event) {
        if (document.getElementById('findHotelForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('findHotelForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "findHotelAdmin",
                    findId: $('#findHotelIdInput').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#hotelTemp');
                    $('#hotelTemp table').remove();
                    $('#hotelTemp h6').remove();
                    if (resp != null && $('#findHotelIdInput').val() !== "") {
                        var table = $('<table class="table">');
                        var tbody = $('<tbody>');
                        var thead = $('<thead class="thead-light">');
                        $('<tr class="recentDelete">').appendTo(thead)
                            .append($('<th scope="col">').text('Id'))
                            .append($('<th scope="col">').text('Hotel name'))
                            .append($('<th scope="col">').text('City'))
                            .append($('<th scope="col">').text('Country'));
                        thead.appendTo(table);
                        tbody.appendTo(table);
                        $.each(resp, function (index, item) {
                            $('<tr class="recentDelete">').appendTo(table)
                                .append($('<th>').text(item.id))
                                .append($('<td>').text(item.name))
                                .append($('<td>').text(item.city))
                                .append($('<td>').text(item.country))
                        });
                        table.appendTo(requiredDiv);
                    } else {
                        requiredDiv.append($('<h6>').text("Didn't find such record"))
                    }
                }
            });
        }
        document.getElementById('findHotelForm').classList.add('was-validated');
    };

    findHotelBtn.on('click', findHotel);

    var findHotelroomBtn = $('#requestFindHotelroom');

    var findHotelroom = function sendData(event) {
        if (document.getElementById('findHotelroomForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('findHotelroomForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "findHotelroomAdmin",
                    findId: $('#findHotelroomIdInput').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#hotelroomTemp');
                    $('#hotelroomTemp table').remove();
                    $('#hotelroomTemp h6').remove();
                    if (resp != null && $('#findHotelroomIdInput').val() !== "") {
                        var table = $('<table class="table">');
                        var tbody = $('<tbody>');
                        var thead = $('<thead class="thead-dark">');
                        $('<tr class="recentDelete">').appendTo(thead)
                            .append($('<th scope="col">').text('Id'))
                            .append($('<th scope="col">').text('Room number'))
                            .append($('<th scope="col">').text('Hotel Id'))
                            .append($('<th scope="col">').text('Price'))
                            .append($('<th scope="col">').text('Room capacity'))
                            .append($('<th scope="col">').text('Comfort'))
                            .append($('<th scope="col">').text('Description'));
                        thead.appendTo(table);
                        tbody.appendTo(table);
                        $.each(resp, function (index, item) {
                            $('<tr class="recentDelete">').appendTo(table)
                                .append($('<th>').text(item.id))
                                .append($('<th>').text(item.number))
                                .append($('<td>').text((item.hotel != null)
                                    ? item.hotel.id
                                    : ""))
                                .append($('<td>').text(item.price))
                                .append($('<td>').text(item.peopleCapacity))
                                .append($('<td>').text(item.comfort))
                                .append($('<td>').text(item.description))
                        });
                        table.appendTo(requiredDiv);
                    } else {
                        requiredDiv.append($('<h6>').text("Didn't find such record"))
                    }
                }
            });
        }
        document.getElementById('findHotelroomForm').classList.add('was-validated');
    };

    findHotelroomBtn.on('click', findHotelroom);

    $('#clearFindUser').on('click', function () {
        $('#userTemp table').remove();
        $('#userTemp h6').remove();
    });
    $('#clearFindDiscount').on('click', function () {
        $('#discountTemp table').remove();
        $('#discountTemp h6').remove();
    });
    $('#clearFindOrder').on('click', function () {
        $('#orderTemp table').remove();
        $('#orderTemp h6').remove();
    });
    $('#clearFindHotel').on('click', function () {
        $('#hotelTemp table').remove();
        $('#hotelTemp h6').remove();
    });
    $('#clearFindHotelroom').on('click', function () {
        $('#hotelroomTemp table').remove();
        $('#hotelroomTemp h6').remove();
    });

    $(function () {
        $("#findUserIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteUserCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        response(data)
                    }
                })
            }
        });
    });
    $(function () {
        $("#findDiscountIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteDiscountCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
    $(function () {
        $("#findOrderIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteOrderCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
    $(function () {
        $("#findHotelIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteHotelCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
    $(function () {
        $("#findHotelroomIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteHotelroomCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
});