$(document).ready(function ($) {
    var createDiscountBtn = $('#requestCreateDiscount');

    var createDiscount = function sendData(event) {
        if (document.getElementById('createDiscountForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('createDiscountForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "createDiscountAdmin",
                    insertUserJobPlace: $('#insertUserJobPlace').val(),
                    insertDiscountValueForJobPlace: $('#insertDiscountValueForJobPlace').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#discountTempCreate');
                    $('#discountTempCreate h6').remove();
                    if (resp != null) {
                        requiredDiv.append($('<h6>').text(resp.toString()))
                    }
                }
            });
        }
        document.getElementById('createDiscountForm').classList.add('was-validated');
    };

    createDiscountBtn.on('click', createDiscount);

    var createHotelroomBtn = $('#requestCreateHotelroom');

    var createHotelroom = function sendData(event) {
        if (document.getElementById('createHotelroomForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('createHotelroomForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "createHotelroomAdmin",
                    insertNumber: $('#insertNumber').val(),
                    insertHotelId: $('#insertHotelId').val(),
                    insertPrice: $('#insertPrice').val(),
                    insertCapacity: $('#insertCapacity').val(),
                    insertComfort: $('#insertComfort').val(),
                    insertImgURL: $('#insertImgURL').val(),
                    insertDescription: $('#insertDescription').val(),
                    insertDescriptionEn: $('#insertDescriptionEn').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#hotelroomTempCreate');
                    $('#hotelroomTempCreate h6').remove();
                    if (resp != null) {
                        requiredDiv.append($('<h6>').text(resp.toString()))
                    }
                }
            });
        }
        document.getElementById('createHotelroomForm').classList.add('was-validated');
    };

    createHotelroomBtn.on('click', createHotelroom);

    var createHotelBtn = $('#requestCreateHotel');

    var createHotel = function sendData(event) {
        if (document.getElementById('createHotelForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('createHotelForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "createHotelAdmin",
                    insertHotelName: $('#insertHotelName').val(),
                    insertCity: $('#insertCity').val(),
                    insertCountry: $('#insertCountry').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#hotelTempCreate');
                    $('#hotelTempCreate h6').remove();
                    if (resp != null) {
                        requiredDiv.append($('<h6>').text(resp.toString()))
                    }
                }
            });
        }
        document.getElementById('createHotelForm').classList.add('was-validated');
    };

    createHotelBtn.on('click', createHotel);

    $(document).ready(function () {
        $.ajax({
            url: "/",
            method: "POST",
            data: {
                ajaxCommand: "aliveSession"
            },
            success: function (resp) {
                if (resp.toString() === "\"empty\"") {
                    location.reload(true)
                }
            }
        });
    })
});