$(document).ready(function ($) {
    var deleteUserBtn = $('#requestDeleteUser');

    var deleteUser = function sendData(event) {
        if (document.getElementById('deleteUserForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('deleteUserForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "deleteUserAdmin",
                    deleteId: $('#deleteUserIdInput').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#userTempDelete');
                    $('#userTempDelete h6').remove();
                    if (resp != null && $('#deleteUserIdInput').val() !== "") {
                        requiredDiv.append($('<h6>').text(resp.toString()))
                    }
                }
            });
        }
        document.getElementById('deleteUserForm').classList.add('was-validated');
    };

    deleteUserBtn.on('click', deleteUser);

    var deleteDiscountBtn = $('#requestDeleteDiscount');

    var deleteDiscount = function sendData(event) {
        if (document.getElementById('deleteDiscountForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('deleteDiscountForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "deleteDiscountAdmin",
                    deleteId: $('#deleteDiscountIdInput').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#discountTempDelete');
                    $('#discountTempDelete h6').remove();
                    if (resp != null && $('#deleteDiscountIdInput').val() !== "") {
                        requiredDiv.append($('<h6>').text(resp.toString()))
                    }
                }
            });
        }
        document.getElementById('deleteDiscountForm').classList.add('was-validated');
    };

    deleteDiscountBtn.on('click', deleteDiscount);

    var deleteOrderBtn = $('#requestDeleteOrder');

    var deleteOrder = function sendData(event) {
        if (document.getElementById('deleteOrderForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('deleteOrderForm').checkValidity() === true) {
            $.ajax({
                url: "controller",
                method: "POST",
                data: {
                    ajaxCommand: "deleteOrderAdmin",
                    deleteId: $('#deleteOrderIdInput').val()
                },
                success: function (resp) {
                    var requiredDiv = $('#orderTempDelete');
                    $('#orderTempDelete h6').remove();
                    if (resp != null && $('#deleteOrderIdInput').val() !== "") {
                        requiredDiv.append($('<h6>').text(resp.toString()))
                    }
                }
            });
        }
        document.getElementById('deleteOrderForm').classList.add('was-validated');
    };

    deleteOrderBtn.on('click', deleteOrder);

    var deleteHotelBtn = $('#requestDeleteHotel');

    var deleteHotel = function sendData(event) {
        if (document.getElementById('deleteHotelForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('deleteHotelForm').checkValidity() === true) {
        $.ajax({
            url: "controller",
            method: "POST",
            data: {
                ajaxCommand: "deleteHotelAdmin",
                deleteId: $('#deleteHotelIdInput').val()
            },
            success: function (resp) {
                var requiredDiv = $('#hotelTempDelete');
                $('#hotelTempDelete h6').remove();
                if (resp != null && $('#deleteHotelIdInput').val() !== "") {
                    requiredDiv.append($('<h6>').text(resp.toString()))
                }
            }
        });
        }
        document.getElementById('deleteHotelForm').classList.add('was-validated');
    };

    deleteHotelBtn.on('click', deleteHotel);

    var deleteHotelroomBtn = $('#requestDeleteHotelroom');

    var deleteHotelroom = function sendData(event) {
        if (document.getElementById('deleteHotelroomForm').checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        if (document.getElementById('deleteHotelroomForm').checkValidity() === true) {
        $.ajax({
            url: "controller",
            method: "POST",
            data: {
                ajaxCommand: "deleteHotelroomAdmin",
                deleteId: $('#deleteHotelroomIdInput').val()
            },
            success: function (resp) {
                var requiredDiv = $('#hotelroomTempDelete');
                $('#hotelroomTempDelete h6').remove();
                if (resp != null && $('#deleteHotelroomIdInput').val() !== "") {
                    requiredDiv.append($('<h6>').text(resp.toString()))
                }
            }
        });
        }
        document.getElementById('deleteHotelroomForm').classList.add('was-validated');
    };

    deleteHotelroomBtn.on('click', deleteHotelroom);

    $('#clearDeleteUser').on('click', function () {
        $('#userTempDelete h6').remove();
    });
    $('#clearDeleteDiscount').on('click', function () {
        $('#discountTempDelete h6').remove();
    });
    $('#clearDeleteOrder').on('click', function () {
        $('#orderTempDelete h6').remove();
    });
    $('#clearDeleteHotel').on('click', function () {
        $('#hotelTempDelete h6').remove();
    });
    $('#clearDeleteHotelroom').on('click', function () {
        $('#hotelroomTempDelete h6').remove();
    });

    $(function () {
        $("#deleteUserIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteUserCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
    $(function () {
        $("#deleteDiscountIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteDiscountCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
    $(function () {
        $("#deleteOrderIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteOrderCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
    $(function () {
        $("#deleteHotelIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteHotelCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
    $(function () {
        $("#deleteHotelroomIdInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'controller',
                    method: 'POST',
                    data: {
                        term: request.term,
                        ajaxCommand: 'autocompleteHotelroomCommand'
                    },
                    dataType: "json",
                    success: function (data) {
                        response(data)
                    }
                })
            }
        });
    });
});