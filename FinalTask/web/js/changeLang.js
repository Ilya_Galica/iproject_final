jQuery(document).ready(function ($) {
    console.log("isNow");
    var htmlLang = $('html').attr('lang');
    var locationLang = sessionStorage.getItem('language');
    if (locationLang !== null && htmlLang !== locationLang) {
        window.location.reload();
    }
    var changeRu = function () {
        $.ajax({
            method: "POST",
            url: '/controller',
            cache: false,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
            data: {
                ajaxCommand: "changeLanguage",
                localeRU: $('#ru').val()
            },
            success: function () {
                sessionStorage.setItem("language", "ru");
                location.reload(true);
            },
            error: function (request) {
                alert(request)
            }
        });
    };

    $('#ru').on('click', changeRu);

    var changeEn = function () {
        $.ajax({
            method: "POST",
            url: '/controller',
            contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
            data: {
                ajaxCommand: "changeLanguage",
                localeEN: $('#en').val()
            },
            success: function () {
                sessionStorage.setItem("language", "en");
                location.reload(true);
            },
            error: function (request) {
                alert(request)
            }
        });
    };

    $('#en').on('click', changeEn);
});