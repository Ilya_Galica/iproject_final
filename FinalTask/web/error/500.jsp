<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<html lang="${sessionScope.language}" dir="ltr">
<head>
    <title>500 Error</title>

    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>

    <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

    <link href="<c:url value="/css/library/fontawesome/css/all.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/content.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/footer.css"/>" rel="stylesheet">

    <script src="<c:url value="/js/library/bootstrap/bootstrap.js"/>"></script>
</head>
<body class="m-0 p-0 d-flex flex-column">
<jsp:include page="/WEB-INF/page.part/header.jsp"/>
<c:set var="language" value="${sessionScope.language}"/>
<div class="flex-grow-1">
    Request from ${pageContext.errorData.requestURI} is failed
    <br/>
    Servlet name: ${pageContext.errorData.servletName}
    <br/>
    Status code: ${pageContext.errorData.statusCode}
    <br/>
    Exception: ${pageContext.exception}
    <br/>
    Message from exception: ${pageContext.exception.message}
</div>
    <jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</body>
</html>