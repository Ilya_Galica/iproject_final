<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<html lang="${sessionScope.language}" dir="ltr">
<head>
    <title>404 Error</title>

    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>

    <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

    <link href="<c:url value="/css/library/fontawesome/css/all.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/content.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/footer.css"/>" rel="stylesheet">

    <script src="<c:url value="/js/library/bootstrap/bootstrap.js"/>"></script>
</head>
<body class="d-flex flex-column">
<jsp:include page="/WEB-INF/page.part/header.jsp"/>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <h1 class="text-center flex-grow-1" style="font-size: 70px">
        <fmt:message key="page.not.found"/>
    </h1>
    <jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</fmt:bundle>
</body>
</html>
