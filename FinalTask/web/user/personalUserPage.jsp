<%--@elvariable id="code" type="java.lang.String"--%>
<%--@elvariable id="user" type="dao.entity.impl.User"--%>
<%--<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <html lang="${sessionScope.language}" dir="ltr">
    <head>

        <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

        <script src="<c:url value="/js/library/bootstrap/bootstrap.js"/>"></script>

        <title>Ваша личная страница</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta http-equiv="Pragma" content="no-cache">

        <meta http-equiv="Cache-Control" content="no-cache">

        <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

        <link rel="shortcut icon" href="<c:url value="/favicon.ico"/>"/>

        <link href="<c:url value="/css/contentShowRequestHotels.css"/>" rel="stylesheet">

        <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

        <script src="<c:url value="/js/changePersonalParams.js"/>"></script>

        <link href="<c:url value="/css/footer.css"/>" rel="stylesheet">

        <script src="<c:url value="/js/bootstrapValidator.js"/>"></script>

        <script src="<c:url value="/js/library/jquery.maskedinput.min.js"/>"></script>


    </head>
    <c:set value="/index" var="pageURL"/>
    <body class="m-0 p-0 d-flex flex-column">
    <jsp:include page="/WEB-INF/page.part/personalPageHeader.jsp"/>

    <%--<c:if test="${sessionScope.user == null}">--%>
    <%--<jsp:forward page="/index.jsp"/>--%>
    <%--</c:if>--%>
    <%--@declare id="exitform"--%>
    <%--@declare id="signInForm"--%>
    <%--@declare id="signupform"--%>
    <%--@declare id="changelanguage"--%>
    <input type="hidden" form="exitForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="signInForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="signUpForm" name="pageName" value="${pageURL}">
    <input id="personalUserPageCheck" type="hidden" form="changeLanguage" name="pageName"
           value="/hotels/user/personalUserPage"/>
    <div class="row justify-content-around m-0 p-0 flex-grow-1">
        <div class="col-lg-5 col-md-5 col-sm-11 mt-3">
            <c:choose>
            <c:when test="${sessionScope.userOrders!=null}">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">№</th>
                    <th scope="col"><fmt:message key="arrival"/></th>
                    <th scope="col"><fmt:message key="departure"/></th>
                    <th scope="col"><fmt:message key="hotel.name"/></th>
                    <th scope="col"><fmt:message key="city"/></th>
                    <th scope="col"><fmt:message key="hotelroom.number"/></th>
                </tr>
                </thead>
                <tbody>
                <c:set var="index" value="0"/>
                <c:forEach items="${sessionScope.userOrders}" var="order">
                    <tr>
                        <c:set var="index" value="${index+1}"/>
                        <th scope="row">${index}</th>
                        <td>${order.arrivalData}</td>
                        <td>${order.departureData}</td>
                        <td>${order.hotelroom.hotel.name}</td>
                        <td>${order.hotelroom.hotel.city}</td>
                        <td>${order.hotelroom.number}</td>
                    </tr>
                </c:forEach>
                </c:when>
                </c:choose>
                </tbody>
            </table>
        </div>

        <div class="col-lg-5 col-md-5 col-sm-11 mt-3">
            <div class="login-form">
                    ${requestScope.changeData}
                <form action="<c:url value="/user"/>" method="post" class="needs-validation" id="changeForm" novalidate>
                    <input type="hidden" name="command" value="changeData">
                    <div class="row">
                        <div class="form-group col-9">
                            <label for="yourEmail"><fmt:message key="change.email"/></label>

                            <input type="email" class="form-control" id="yourEmail" placeholder="Email"
                                   name="email" value="${sessionScope.user.email}">
                            <div class="valid-feedback">
                                <fmt:message key="right.email"/>
                            </div>
                            <div class="invalid-feedback">
                                <fmt:message key="not.right.email"/>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary col-2 align-self-center mt-3" id="changeEmail"
                               value="<fmt:message key="change"/>">
                    </div>
                </form>
                <div class="card">
                    <div class="card-body justify-content-center">
                        <h5 class="card-title"><fmt:message key="change.pass.condition"/></h5>

                        <form action="/user" method="post" class="needs-validation" novalidate>
                            <input type="hidden" name="command" value="changeData">
                            <div class="row">
                                <div class="form-group col-9">
                                    <label for="yourPassword"> <fmt:message key="can.change.password"/></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text show-password"><i
                                                class='far fa-eye'></i></span>
                                        </div>
                                        <input type="password" class="form-control visible-password old" id="yourPassword"
                                               placeholder=" <fmt:message key="password.old"/>"
                                               name="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(\w){6,30}">
                                        <div class="valid-feedback">
                                            <fmt:message key="right.password"/>
                                        </div>
                                        <div class="invalid-feedback">
                                            <fmt:message key="not.right.password"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4>${requestScope.passwordChanges}</h4>
                            <div class="row">
                                <div class="form-group col-9">
                                    <label for="yourPasswordNew"> <fmt:message key="new.password"/></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text show-password"><i
                                                class='far fa-eye'></i></span>
                                        </div>
                                        <input type="password" class="form-control visible-password new"
                                               id="yourPasswordNew"
                                               placeholder=" <fmt:message key="new.password"/>"
                                               name="passwordNew" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(\w){6,30}">
                                        <div class="valid-feedback">
                                            <fmt:message key="right.password"/>
                                        </div>
                                        <div class="invalid-feedback">
                                            <fmt:message key="not.right.password"/>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary col-2 align-self-center mt-3"
                                       id="changePassNew"
                                       value="<fmt:message key="change"/>">
                            </div>
                        </form>
                    </div>
                </div>
                <form action="<c:url value="/user"/>" method="post" class="needs-validation" novalidate>
                    <input type="hidden" name="command" value="changeData">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="yourCountry"><fmt:message key="country"/></label>
                            <select id="yourCountry" class="form-control">
                                <option value="ua"><fmt:message key="ukraine"/> +380</option>
                                <option value="by" selected><fmt:message key="belarus"/> +375</option>
                            </select>
                        </div>
                        <div class=" col-md-6">
                            <label for="yourTelephone"> <fmt:message key="can.change.telephone"/></label>
                            <input type="text" class="form-control" id="yourTelephone"
                                   placeholder=" <fmt:message key="telephone"/>"
                                   name="telephone" value="${sessionScope.user.telephone}"
                                   pattern="((80)?|(\+375)?)(\(\d{2}\)|\d{2})((\d{3}-\d{2}-\d{2})|\d{7})">
                            <div class="valid-feedback">
                                <fmt:message key="right.telephone"/>
                            </div>
                            <div class="invalid-feedback">
                                <fmt:message key="not.right.telephone"/>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary col-2 align-self-center mt-3" id="changeTel"
                               value="<fmt:message key="change"/>">
                    </div>
                </form>
                <form action="<c:url value="/user"/>" method="post" class="needs-validation" novalidate>
                    <input type="hidden" name="command" value="changeData">
                    <div class="row align-content-center">
                            <%--<form id="changeJobForm" class="needs-validation" novalidate>--%>
                        <div class="form-group col-9">
                            <label for="yourWork"><fmt:message key="can.change.job"/></label>
                            <input type="text" class="form-control" id="yourWork"
                                   placeholder=" <fmt:message key="job"/>"
                                   name="job" aria-describedby="yourWork" value="${sessionScope.user.jobPlace}">
                            <small id="jobInputHelp" class="text-muted">
                                <fmt:message key="job.opportunity"/>
                            </small>
                        </div>
                        <input type="submit" class="btn btn-primary col-2 align-self-center mb-3" id="changeJob"
                               value="<fmt:message key="change"/>">
                            <%--</form>--%>
                    </div>
                </form>
            </div>
        </div>

    </div>
</fmt:bundle>

<jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</body>
</html>
