<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="${sessionScope.language}" dir="ltr">
<head>
    <%--@declare id="exitform"--%>
    <%--@declare id="signinform"--%>
    <%--@declare id="signupform"--%>
    <%--@declare id="changelanguage"--%>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>

    <link rel="shortcut icon" href="<c:url value="/favicon.ico"/>"/>

    <link href="css/header.css" rel="stylesheet">

    <link href="css/content.css" rel="stylesheet">

    <link href="css/footer.css" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/js/library/jquery-ui/jquery-ui.css"/>">

    <script src="<c:url value="/js/library/jquery-ui/jquery-ui.js"/> "></script>

    <script src="<c:url value="/js/peopleAmountControl.js"/>"></script>

    <script src="<c:url value="/js/library/bootstrap/bootstrap.js"/>"></script>

    <script src="<c:url value="js/library/moment.js"/>"></script>

    <script type="text/javascript"
            src="<c:url value="js/library/http_cdnjs.cloudflare.com_ajax_libs_tempusdominus-bootstrap-4_5.0.0-alpha14_js_tempusdominus-bootstrap-4.js"/>"></script>

    <link rel="stylesheet"
          href="<c:url value="css/library/tempusdominus-bootstrap-4.min.css"/>"/>

    <script src="<c:url value="js/datepicker.js"/>"></script>
    <title>
        Бронирование отелей. Букиг. Лучшие цены - OneDirection
    </title>
</head>
<c:set value="index" var="pageURL"/>
<body class="m-0 p-0">

<input type="hidden" form="exitForm" name="pageName" value="${pageURL}">
<input type="hidden" form="signInForm" name="pageName" value="${pageURL}">
<input type="hidden" form="signUpForm" name="pageName" value="${pageURL}">
<input type="hidden" form="changeLanguage" name="pageName" value="${pageURL}">
<jsp:include page="WEB-INF/page.part/header.jsp"/>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <jsp:include page="/WEB-INF/page.part/emailAlreadyExist.jsp"/>
    <div class="content col-12 m-0 ">
        <input id="pageLanguage" type="hidden" value="${language}">
            <%--<form>--%>
        <form action="<c:url value="/hotels"/>" method="get">
            <input type="hidden" name="command" value="searchHotels">
            <div class="flex-text row justify-content-center m-0 ">
                <h1 class=" col-lg-8 col-sm-12"><fmt:message key="search.hotels"/></h1>

                <h3 class="col-lg-7 col-sm-12"><fmt:message key="book.hotel"/></h3>
            </div>
            <div class="flex-conteiner row justify-content-center align-items-center  m-0 pb-5">

                    <%--                   Arrival city                      --%>

                <div class="first-input col-lg-5 col-sm-8 p-0 m-0">
                        <%--suppress XmlDuplicatedId --%>
                    <input type="text" name="city" class="input-data pl-2 col-12" id="city"
                           placeholder="<fmt:message key="enter.city"/>" required>
                </div>

                    <%--                    Arrival date                    --%>

                <div class='input-group date second-input col-lg-2 col-sm-4 row p-0 m-0'
                     id="start-date-time-datepicker"
                     data-target-input="nearest">
                        <%--suppress XmlDuplicatedId --%>
                    <input type="text" name="arrival"
                           class="form-control datetimepicker-input col-10 col-lg-10 pl-2 m-0"
                           id='arrival' placeholder="<fmt:message key="arrival"/>"
                           data-target="#start-date-time-datepicker"
                           pattern="([0-9]{2})\/([0-9]{2})\/([0-9]{4})" required/>
                    <div class="input-group-append col-2 p-0 m-0 col-lg-2"
                         data-target="#start-date-time-datepicker"
                         data-toggle="datetimepicker">
                        <div class="input-group-text col-12 m-0 p-0 d-flex justify-content-center"><i
                                class="fa fa-calendar"></i></div>
                    </div>
                </div>

                    <%--                   Departure date                    --%>

                <div class='input-group date third-input col-lg-2 col-sm-4 row p-0 m-0'
                     id="end-date-time-datepicker"
                     data-target-input="nearest">
                        <%--suppress XmlDuplicatedId --%>
                    <input type="text" name="departure"
                           class="form-control datetimepicker-input col-10 col-lg-10 pl-2 m-0"
                           id="departure" placeholder="<fmt:message key="departure"/>"
                           data-target="#end-date-time-datepicker"
                           pattern="([0-9]{2})\/([0-9]{2})\/([0-9]{4})" required/>
                    <div class="input-group-append col-2 p-0 m-0 col-lg-2"
                         data-target="#end-date-time-datepicker"
                         data-toggle="datetimepicker">
                        <div class="input-group-text col-12 m-0 p-0 d-flex justify-content-center"><i
                                class="fa fa-calendar"></i></div>
                    </div>
                </div>

                    <%--                   People amount                      --%>

                <div class="forth-input col-lg-2 col-sm-8  p-0 m-0">
                        <%--suppress XmlDuplicatedId --%>
                    <span id="amountControl" hidden>${sessionScope.dataContainer.peopleCapacity}</span>
                    <select name="amount" class="input-data col-lg-12 col-md-12 pl-2 m-0" id="amount">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                </div>

            </div>
            <div class="submit-btn row justify-content-center  m-0 pb-5">
                <input id="findHotels" class="col-lg-2 col-sm-8 ml-2 mr-2" type="submit"
                       value="<fmt:message key="find"/>">
            </div>
        </form>
    </div>
</fmt:bundle>
<jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</body>

