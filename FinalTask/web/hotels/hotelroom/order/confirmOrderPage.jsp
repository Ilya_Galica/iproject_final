<%--@elvariable id="order" type="dao.entity.impl.Order"--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html lang="${sessionScope.language}" dir="ltr">
<head>
    <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>
    <%--@declare id="exitform"--%>
    <%--@declare id="signInForm"--%>
    <%--@declare id="signupform"--%>
    <%--@declare id="changelanguage"--%>

    <link rel="icon" href="data:,">

    <meta http-equiv="Pragma" content="no-cache">

    <meta http-equiv="Cache-Control" content="no-cache">

    <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <%--<jsp:useBean id="hotelroom" scope="session" type="dao.entity.impl.Hotelroom"/>--%>

    <c:set var="discount" value="${order.discount.discountValueForJobPlace}"/>

    <title>Подтверждение заказа</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="<c:url value="/js/confirmOrder.js"/>"></script>

    <link rel="shortcut icon" href="<c:url value="/favicon.ico"/>"/>

    <link href="<c:url value="/css/contentShowRequestHotels.css"/>" rel="stylesheet">

    <script src="<c:url value="/js/library/bootstrap/bootstrap.js"/>"></script>

    <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/footer.css"/>" rel="stylesheet">

    <c:set value="hotels/order/confirmOrderPage" var="pageURL"/>
</head>
<body class="m-0 p-0 d-flex flex-column">
<input type="hidden" form="exitForm" name="pageName" value="${pageURL}">
<input type="hidden" form="signInForm" name="pageName" value="${pageURL}">
<input type="hidden" form="signUpForm" name="pageName" value="${pageURL}">
<input type="hidden" form="changeLanguage" name="pageName" value="${pageURL}">

<jsp:include page="/WEB-INF/page.part/header.jsp"/>

<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <div class="card mt-3 mr-0 flex-grow-1">

        <jsp:include page="/WEB-INF/page.part/confirmOrderModal.jsp"/>
        <div class="row justify-content-center mt-3 mr-0">
            <div class="card mb-3 col-lg-8 col-md-8 col-sm-11">
                <div class="card-body">
                    <h4 class="card-title"><fmt:message key="your.check.order"/></h4>
                    <form id="confirmOrderForm">

                        <p id="userNameP">${order.user.surname} ${order.user.name} ${order.user.patronymic},</p>
                        <p><fmt:message key="your.hotel"/> ${order.hotelroom.hotel.name}</p>
                        <p><fmt:message key="your.room.number"/> ${order.hotelroom.number}</p>
                        <p><fmt:message key="your.arrival"/> ${dataContainer.arrivalData}
                            <fmt:message key="your.departure"/> ${dataContainer.departureData}</p>
                        <p><fmt:message key="your.discount"/>
                            <strong>${order.discount.discountValueForJobPlace}%</strong></p>
                        <p><fmt:message key="your.order.cost"/> <strong>${order.totalPrice}$</strong></p>
                        <p><fmt:message key="your.confirm"/> <strong>${order.user.email}</strong></p>

                    </form>
                    <div class="justify-content-center mt-3 row">
                        <input class="submit-btn" id="confirmOrder" type="button"
                               value="<fmt:message key="your.confirm.button"/>"
                               form="confirmOrderForm">
                    </div>
                </div>
            </div>
        </div>


    </div>
</fmt:bundle>
<jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</body>
</html>