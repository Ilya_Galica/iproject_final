<%--@elvariable id="dataContainer" type="dao.entity.HotelSearchDataContainer"--%>
<%--@elvariable id="hotelroom" type="dao.entity.impl.Hotelroom"--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="${sessionScope.language}" dir="ltr">
<head>
    <%--@declare id="exitform"--%>
    <%--@declare id="signinform"--%>
    <%--@declare id="signupform"--%>

        <link rel="icon" href="data:,">

    <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

    <script src="<c:url value="/js/sendOrder.js"/>"></script>

    <script src="<c:url value="/js/library/jquery.maskedinput.min.js"/>"></script>

    <%--<jsp:useBean id="hotelroom" scope="session" type="dao.entity.impl.Hotelroom"/>--%>
    <%--<jsp:useBean id="dataContainer" scope="session" type="dao.entity.HotelSearchDataContainerr"/>--%>
    <title>Комнаты, находящиеся в ${hotelroom.hotel.name}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="<c:url value="/css/contentShowRequestHotels.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

        <link rel="shortcut icon" href="<c:url value="/favicon.ico"/>"/>

    <link href="<c:url value="/css/footer.css"/>" rel="stylesheet">

    <script src="<c:url value="/js/library/bootstrap/bootstrap.js"/>"></script>

    <script src="<c:url value="/js/bootstrapValidator.js"/>"></script>

    <c:set value="hotels/hotelroom/showHotelroomPage" var="pageURL"/>
</head>
<body class="m-0 p-0">
<input type="hidden" form="exitForm" name="pageName" value="${pageURL}">
<input type="hidden" form="signInForm" id="showHotelroomPageURL" name="pageName" value="${pageURL}">
<input type="hidden" form="signUpForm" name="pageName" value="${pageURL}">
<input type="hidden" form="changeLanguage" name="pageName" value="${pageURL}">
<%--<jsp:include page="/WEB-INF/page.part/header.jsp"/>--%>
<c:import url="/WEB-INF/page.part/header.jsp"/>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value="ru"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <jsp:include page="/WEB-INF/page.part/emailAlreadyExist.jsp"/>

    <%--<form action="<c:url value="hotels/hotelroom/confirmOrder"/>" method="post" id="createOrderForm">--%>
        <jsp:include page="/WEB-INF/page.part/createOrderModal.jsp"/>
        <input type="hidden" name="command" value="createOrder">
        <div class="row justify-content-center mt-3 mr-0">
            <div class="card mb-3 col-lg-8 col-md-8 col-sm-11">
                <img class="card-img-top rounded img-fluid mt-3" src="${hotelroom.imageURL}"
                     alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">${hotelroom.hotel.name}</h5>
                        <%--<p class="card-text">${hotelroom.description}</p>--%>
                    <h6 style="color: red">${notAvailableHotelroom}</h6>
                    <c:choose>
                        <c:when test="${language eq 'ru'}">
                            <p class="card-text">${hotelroom.description}</p>
                        </c:when>
                        <c:otherwise>
                            <p class="card-text">${hotelroom.descriptionEn}</p>
                        </c:otherwise>
                    </c:choose>
                    <p><fmt:message key="one.night.cost"/>: <strong>${hotelroom.price}</strong></p>
                    <p><fmt:message key="your.order.cost"/>:
                        <strong>${sessionScope.dayAmount * hotelroom.price}</strong>,
                        <fmt:message key="arrival.to.room"/>: ${dataContainer.arrivalData}
                        <fmt:message key="departure.from.room"/>: ${dataContainer.departureData}</p>
                    <div class="justify-content-center mt-3 row">
                        <c:choose>
                            <c:when test="${sessionScope.user==null}">
                                <input class="submit-btn" type="button"
                                       value="<fmt:message key="create.order"/>"
                                       data-toggle="modal" data-target="#createOrderModal">
                            </c:when>
                            <c:otherwise>
                                <form id="createOrderAuthForm">

                                    <input type="hidden" name="command" value="createOrderAuth">
                                    <input class="submit-btn" type="button" id="createOrderAuth"
                                           value="<fmt:message key="create.order"/>">
                                </form>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>
    <%--</form>--%>
</fmt:bundle>



<jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</body>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"--%>
<%--integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"--%>
<%--crossorigin="anonymous"></script>--%>
<%--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"--%>
<%--integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"--%>
<%--crossorigin="anonymous"></script>--%>
</html>
