<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="${sessionScope.language}" dir="ltr">
<head>
    <%--@declare id="exitform"--%>
    <%--@declare id="signinform"--%>
    <%--@declare id="signupform"--%>
    <%--@declare id="changelanguage"--%>
    <c:set value="hotels/showRequestHotels" var="pageName"/>
    <meta charset="utf-8">


    <link href="<c:url value="/css/header.css"/>" rel="stylesheet">

    <link rel="shortcut icon" href="<c:url value="/favicon.ico"/>"/>

    <link href="<c:url value="/css/contentShowRequestHotels.css"/>" rel="stylesheet">

    <link href="<c:url value="/css/footer.css"/>" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="<c:url value="/js/library/jQuery%203.4.1.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/js/library/jquery-ui/jquery-ui.css"/>">

    <script src="<c:url value="/js/library/jquery-ui/jquery-ui.js"/> "></script>

    <script src="<c:url value="/js/peopleAmountControl.js"/>"></script>

    <script src="<c:url value="/js/library/moment.js"/>"></script>

    <script src="<c:url value="/js/library/bootstrap/bootstrap.js"/>"></script>

    <script type="text/javascript"
            src="<c:url value="/js/library/http_cdnjs.cloudflare.com_ajax_libs_tempusdominus-bootstrap-4_5.0.0-alpha14_js_tempusdominus-bootstrap-4.js"/>"></script>

    <link rel="stylesheet"
          href="<c:url value="/css/library/tempusdominus-bootstrap-4.min.css"/>"/>

    <script src="<c:url value="/js/datepicker.js"/>"></script>
    <title>Результаты поиска отелей</title>
</head>
<c:set var="pageURL" value="hotels/showRequestHotels"/>
<body class="m-0 p-0 d-flex flex-column">

<jsp:include page="/WEB-INF/page.part/header.jsp"/>
<c:set var="language" value="${sessionScope.language}"/>
<c:if test="${sessionScope.language==null}">
    <c:set var="language" value='ru'/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:bundle basename="applicationLanguage">
    <jsp:include page="/WEB-INF/page.part/emailAlreadyExist.jsp"/>
    <input type="hidden" form="exitForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="signInForm" id="showHotelsPageURL" name="pageName" value="${pageURL}">
    <input type="hidden" form="signUpForm" name="pageName" value="${pageURL}">
    <input type="hidden" form="changeLanguage" name="pageName" value="${pageURL}">
    <input id="pageLanguage" type="hidden" value="${language}">
    <div class="row m-0 p-0 flex-grow-1">
        <div class="col-lg-3 col-md-3 col-sm-12 ml-lg-5 mt-lg-4 mt-sm-0 ml-sm-0 justify-self-start">
            <div class="card mb-lg-4 mb-md-4 mb-sm-2 ">
                <div class="card-body col-12">
                    <h5 class="card-title"><fmt:message key="change.search.params"/></h5>
                    <form action="<c:url value="/hotels"/>" method="get">
                        <input type="hidden" name="servlet" value="/hotels">
                        <input type="hidden" name="command" value="searchHotels">

                        <div class="first-input col-12 p-0 m-0">
                            <input type="text" name="city" class="input-data col-12 pl-2" id="city"
                                   placeholder="<fmt:message key="enter.city"/>"
                                   value="${sessionScope.dataContainer.city}">
                        </div>


                        <div class='input-group date second-input col-12 row p-0 m-0'
                             id="start-date-time-datepicker"
                             data-target-input="nearest">
                            <input type="text" name="arrival"
                                   class="form-control datetimepicker-input col-10 col-lg-10 pl-2 m-0"
                                   id="arrival" placeholder="<fmt:message key="arrival"/>"
                                   value="${sessionScope.dataContainer.arrivalData}"
                                   data-target="#start-date-time-datepicker"
                                   pattern="([0-9]{2})\/([0-9]{2})\/([0-9]{4})"/>
                            <div class="input-group-append col-2 p-0 m-0 col-lg-2"
                                 data-target="#start-date-time-datepicker"
                                 data-toggle="datetimepicker">
                                <div class="input-group-text col-12 m-0 p-0 d-flex justify-content-center"><i
                                        class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                        <div class='input-group date third-input col-12 row p-0 m-0'
                             id="end-date-time-datepicker"
                             data-target-input="nearest">
                            <input type="text" name="departure"
                                   class="form-control datetimepicker-input col-10 col-lg-10 pl-2 m-0"
                                   id="departure" placeholder="<fmt:message key="departure"/>"
                                   value="${sessionScope.dataContainer.departureData}"
                                   data-target="#end-date-time-datepicker"
                                   pattern="([0-9]{2})\/([0-9]{2})\/([0-9]{4})"/>
                            <div class="input-group-append col-2 p-0 m-0 col-lg-2"
                                 data-target="#end-date-time-datepicker"
                                 data-toggle="datetimepicker">
                                <div class="input-group-text col-12 m-0 p-0 d-flex justify-content-center"><i
                                        class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                        <div class="forth-input col-12  p-0 m-0">
                            <span id="amountControl" hidden>${sessionScope.dataContainer.peopleCapacity}</span>
                            <select name="amount" class="input-data col-lg-12 col-md-12 pl-2 m-0" id="amount">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="justify-content-center mt-3 row">
                            <input class="submit-btn" type="submit" value="<fmt:message key="find"/>">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-lg-8 col-sm-12 ml-lg-5  ml-sm-0">
            <c:set var="hotelroomListPosition" value="${0}" scope="request"/>

            <h3>${requestScope.badHotelroomsRequest}</h3>
                <%--<h3>${sessionScope.hotelrooms.size}</h3>--%>
            <c:forEach items="${sessionScope.hotelrooms}" var="hotelroom">
                <form action="<c:url value="/hotels/hotelroom"/>" method="get">
                    <input type="hidden" name="command" value="hotelroomPage">
                    <input type="hidden" name="hotelroomPosition" value="${hotelroomListPosition}">
                    <c:set var="hotelroomListPosition" value="${hotelroomListPosition+1}" scope="request"/>
                    <div class="card col-12 row mt-lg-4 mt-md-4 mt-sm-2" style="max-height: 20rem;">
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <img src="${hotelroom.imageURL}" class="img-fluid mt-3">
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12" style="max-height: 13rem;">
                            <div class="card-header"><strong>${hotelroom.hotel.name}</strong></div>
                            <div class="card-body row">
                                <p class="card-text">
                                    <fmt:message key="good.hotel"/>
                                    <strong>${hotelroom.hotel.city}</strong>
                                    <fmt:message key="cost"/> <strong>${hotelroom.price} </strong>,
                                    <fmt:message key="capacity"/>
                                    <strong>${hotelroom.peopleCapacity}</strong>
                                </p>
                                <input type="submit" class="submit-btn" value="<fmt:message key="send"/>">
                            </div>
                        </div>
                    </div>
                </form>
            </c:forEach>
            <div class="row justify-content-center mt-lg-4 mt-md-4 mt-sm-2">
                <jsp:useBean id="pagination" scope="session" type="dao.entity.Pagination"/>
                <div>
                    <nav aria-label="Navigation for countries">
                        <ul class="pagination">
                            <c:if test="${ pagination.currentPage != 1 &&  pagination.currentPage!=null}">
                                <li class="page-item">
                                    <form action="<c:url value="/hotels"/>" method="get">
                                        <input type="hidden" name="command" value="pagination">
                                        <input type="hidden" name="currentPage"
                                               value="${ pagination.currentPage-1}">
                                        <input type="submit" class="page-link" value="&laquo;">
                                    </form>
                                </li>
                            </c:if>

                            <c:forEach begin="1" end="${ pagination.paginationAmount}" var="i">
                                <c:choose>
                                    <c:when test="${ pagination.currentPage eq i}">
                                        <li class="page-item active">
                                            <form action="<c:url value="/hotels"/>" method="get">
                                                <input type="hidden" name="command" value="pagination">
                                                <input type="submit" class="page-link" name="currentPage"
                                                       value="${i}">
                                            </form>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <form action="<c:url value="/hotels"/>" method="get">

                                                <input type="hidden" name="command" value="pagination">
                                                <input type="submit" class="page-link" name="currentPage"
                                                       value="${i}">
                                            </form>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                            <c:if test="${ pagination.currentPage lt  pagination.paginationAmount}">
                                <li class="page-item">
                                    <form action="<c:url value="/hotels"/>" method="get">
                                        <input type="hidden" name="command" value="pagination">
                                        <input type="hidden" name="currentPage"
                                               value="${ pagination.currentPage+1}">
                                        <input type="submit" class="page-link" value="&raquo;">
                                    </form>
                                </li>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</fmt:bundle>
<jsp:include page="/WEB-INF/page.part/footer.jsp"/>
</body>
</html>
