create table "discount"
(
  "id"                           integer(10) not null
    primary key,
  "user_job_place"               varchar(30) not null,
  "discount_value_for_job_place" integer(10) not null
);

create table "hotel"
(
  "id"      integer(10) not null
    primary key,
  "name"    varchar(45) not null,
  "city"    varchar(45) not null,
  "country" varchar(45) not null
);

create table "hotelroom"
(
  "id"             integer(10)  not null
    primary key,
  "number"         integer(10)  not null,
  "hotel"          integer(10)  not null,
  "price"          integer(10)  not null,
  "capacity"       integer(10)  not null,
  "comfort"        enum ('lux', 'halflux', 'common'),
  "image_url"      varchar(100) not null,
  "description"    clob(max)    not null,
  "description_en" clob(max),
  constraint "hotel"
  foreign key ("hotel") references "hotel"
);

create table "user"
(
  "id"         bigint(19) not null
    primary key,
  "email"      varchar(35),
  "name"       varchar(45),
  "surname"    varchar(45),
  "patronymic" varchar(45),
  "telephone"  varchar(17),
  "password"   varchar(45),
  "job_place"  varchar(45),
  "role"       enum ('user', 'admin') default 'user' not null
);

create table "order"
(
  "id"             bigint(19) default (NEXT VALUE FOR "hotel_booking"."SYSTEM_SEQUENCE_658A7818_75A9_4422_A5C0_BB9A09008B57")
    primary key,
  "discount"       integer(10),
  "owner"          integer(10),
  "arrival_data"   date(10),
  "departure_data" date(10),
  "hotelroom"      integer(10),
  "total_price"    integer(10),
  constraint "client"
  foreign key ("owner") references "user",
  constraint "discount"
  foreign key ("discount") references "discount",
  constraint "room"
  foreign key ("hotelroom") references "hotelroom"
);

create index "client_idx"
  on "order" ("owner");

create index "discount_idx"
  on "order" ("discount");

create unique index "user_id_uindex"
  on "user" ("id");