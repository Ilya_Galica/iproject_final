create table discount
(
  id                           int auto_increment
  comment 'Идентификационный номер скидки',
  user_job_place               varchar(30) not null
  comment 'Место работы клиента
',
  discount_value_for_job_place int(4)      not null
  comment 'Скидка для определенного места работы',
  constraint id_UNIQUE
  unique (id)
)
  comment 'Таблица представляющая совокупность параметров системы лояльности';

alter table discount
  add primary key (id);

create table hotel
(
  id      int(3) auto_increment
  comment 'Идентификационный номер отеля',
  name    varchar(45) not null
  comment 'Название отеля',
  city    varchar(45) not null
  comment 'Город, в котором находится отель',
  country varchar(45) not null
  comment 'Страна, в которой находится отель',
  constraint id_UNIQUE
  unique (id)
)
  comment 'Таблица представляющая совокупность параметров отеля';

alter table hotel
  add primary key (id);

create table hotelroom
(
  id             int auto_increment
  comment 'Идентификационный номер комнаты отеля',
  number         int(4)                            not null,
  hotel          int(3)                            not null
  comment 'Привязка к отелю',
  price          int(7)                            not null
  comment 'Цена комнаты',
  capacity       int(3)                            not null
  comment 'Вместительность комнаты',
  comfort        enum ('lux', 'halflux', 'common') null
  comment 'Уровень комфорта номера',
  image_url      varchar(100)                      not null,
  description    text                              not null,
  description_en text                              null,
  constraint id_UNIQUE
  unique (id),
  constraint hotel
  foreign key (hotel) references hotel (id)
)
  comment 'Таблица представляющая совокупность  параметров номера(комнаты) отеля';

create index hotel_idx
  on hotelroom (hotel);

alter table hotelroom
  add primary key (id);

create table user
(
  id         int auto_increment,
  email      varchar(35)                           not null
  comment 'Email посетителя',
  name       varchar(45)                           not null
  comment 'Имя посетителя',
  surname    varchar(45)                           not null
  comment 'Фамилия посетителя',
  patronymic varchar(45)                           not null
  comment 'Отчество посетителя',
  telephone  varchar(17)                           not null
  comment 'Телефон посетителя',
  password   varchar(45)                           null
  comment 'Пароль посетителя',
  job_place  varchar(45)                           null
  comment 'Место, где работает посетитель',
  role       enum ('admin', 'user') default 'user' not null,
  constraint id_UNIQUE
  unique (id),
  constraint сonsumer_email_UNIQUE
  unique (email),
  constraint сonsumer_telephone_UNIQUE
  unique (telephone)
)
  comment 'Таблица представляющая совокупность параметров возможного покупателя';

alter table user
  add primary key (id);

create table `order`
(
  id             int auto_increment
  comment 'Идентификационный номер заказа',
  discount       int    null
  comment 'Скидка',
  owner          int    null
  comment 'Житель комнаты',
  arrival_data   date   null
  comment 'Дата заселения',
  departure_data date   null
  comment 'Дата выселения',
  hotelroom      int    null,
  total_price    int(7) null
  comment 'Общая цена заказа',
  constraint order_id_UNIQUE
  unique (id),
  constraint client
  foreign key (owner) references user (id),
  constraint discount
  foreign key (discount) references discount (id),
  constraint room
  foreign key (hotelroom) references hotelroom (id)
)
  comment 'Таблица представляющая совокупность параметров заказа';

create index client_idx
  on `order` (owner);

create index discount_idx
  on `order` (discount);

create index room_idx
  on `order` (hotelroom);

alter table `order`
  add primary key (id);
