package controller;

import controller.comand.ajax.AjaxCommand;
import controller.comand.ajax.DirectorAjaxCommand;
import controller.comand.java.ActionFactoryCommand;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Objects;

import static constant.ControllerConstant.*;

@WebServlet(value = "/", name = "controller")
public class Controller extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        if (Objects.isNull(cookies)) {
            resp.encodeURL("&JSESSIONID=" + req.getSession().getId());
        }
        if (req.getParameter(COMMAND) != null) {
            processRequestCommand(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Cookie[] cookies = req.getCookies();
        if (Objects.isNull(cookies)) {
            resp.encodeURL("&JSESSIONID=" + req.getSession().getId());
        }
        if (req.getParameter(AJAX_COMMAND) != null) {
            processRequestAjaxCommand(req, resp);
        }
        if (req.getParameter(COMMAND) != null) {
            resp.sendRedirect(req.getRequestURI());
        }
    }


    private void processRequestCommand(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userCommand = request.getParameter(COMMAND);
        String resultPage = ActionFactoryCommand.getForwardPage(userCommand, request) + JSP;
        request.getRequestDispatcher(resultPage).forward(request, response);
    }

    private void processRequestAjaxCommand(HttpServletRequest request, HttpServletResponse response) {
        String userCommand;
        userCommand = request.getParameter(AJAX_COMMAND);
        DirectorAjaxCommand directorCommand = DirectorAjaxCommand.getInstance();
        AjaxCommand command = directorCommand.getCommand(userCommand);
        command.execute(request, response);
    }
}
