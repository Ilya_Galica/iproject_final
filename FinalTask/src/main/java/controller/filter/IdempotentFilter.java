package controller.filter;

import controller.wrapper.RequestWrapper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static constant.ControllerConstant.*;

@WebFilter(filterName = "IdempotentFilter", urlPatterns = "/")
public class IdempotentFilter implements Filter {
    private RequestWrapper lastRequest;
    private int count;

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        RequestWrapper request = new RequestWrapper((HttpServletRequest) servletRequest);
        if (request.getParameter(AJAX_COMMAND) != null && request.getParameter(COMMAND) == null) {
            filterChain.doFilter(request, servletResponse);
        } else if (request.getParameter(COMMAND) != null && request.getParameter(AJAX_COMMAND) == null) {
            lastRequest = new RequestWrapper(request);
            count++;
            filterChain.doFilter(request, servletResponse);
        } else if ((servletRequest.getParameter(COMMAND) != null
                && request.getMethod().equals(HTTP_GET) && count == 0)
                || request.getRequestURI().contains(FAVICON)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            count = 0;
            if (lastRequest != null) {
                request.setBody(lastRequest.getBody());
            }
            filterChain.doFilter(request, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
