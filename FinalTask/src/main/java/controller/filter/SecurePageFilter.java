package controller.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static constant.ControllerConstant.*;

@WebFilter(filterName = "SecurePageFilter")
public class SecurePageFilter implements Filter {

    private List<String> servletPathQueue = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        String servletPath = request.getServletPath();
        if ((request.getRequestURI().contains(HOTELS_PACKAGE + HOTELROOM_PACKAGE + ORDER)
                && session.getAttribute(ORDER) == null
                && request.getAttribute(GO_NEXT) == null)
                || (request.getRequestURI().contains("/" + USER)
                && session.getAttribute(USER) == null)) {
            ((HttpServletResponse) servletResponse).sendError(403);
        } else {
            if (servletPath.contains("/" + HOTELS_PACKAGE + HOTELROOM_PACKAGE + ORDER)) {
                ((HttpServletResponse) servletResponse).setHeader("Cache-Control", "no-cache");
                ((HttpServletResponse) servletResponse).setHeader("Pragma", "no-cache");
                ((HttpServletResponse) servletResponse).setDateHeader("Expires", 0);
            }
            servletPathQueue.removeIf(record -> record.equals(servletPath));
            servletPathQueue.add(servletPath);
            if (servletPathQueue.size() >= 2 && servletPathQueue
                    .get(servletPathQueue.size() - 2).equals("/" + ADMIN + HTML)) {
                request.getSession().removeAttribute(USER);
            }
            filterChain.doFilter(request, servletResponse);
        }

    }

    @Override
    public void destroy() {

    }
}
