package controller.util;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import static constant.ControllerConstant.MAIL_FROM;
import static constant.ControllerConstant.MAIL_SMTP_PASSWORD;

public final class Miler {

    private static final String PATH_TO_EMAIL_PROPERTIES = "/WEB-INF/classes/emailData.properties";
    private static Properties properties = new Properties();

    public static Properties getProperties() {
        return properties;
    }

    public static void loadProperties(HttpServletRequest request) throws IOException {
        properties.load(new FileReader(new File(request
                .getServletContext().getRealPath(PATH_TO_EMAIL_PROPERTIES))));
    }

    public static MimeMessage buildMimeMessage(Session session, String address, String subject, String responseText)
            throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(properties.getProperty(MAIL_FROM)));
        message.addRecipient(Message.RecipientType.TO,
                new InternetAddress(address));
        message.setSubject(subject);
        message.setText(responseText);
        return message;
    }

    public static void doTransport(Session session, MimeMessage message) throws MessagingException {
        Transport transport = session.getTransport();
        transport.connect(properties.getProperty(MAIL_FROM), properties.getProperty(MAIL_SMTP_PASSWORD));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }

    private Miler() {

    }
}
