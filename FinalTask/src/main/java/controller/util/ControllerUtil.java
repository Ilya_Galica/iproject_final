package controller.util;

import dao.entity.impl.User;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import static constant.ControllerConstant.*;

public final class ControllerUtil {

    private static final String DIVIDER = "[#][|][#]";

    private ControllerUtil() {
    }

    public static String encoding(String param) {
        return new String(param.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }

    public static String localeException(String exception, ResourceBundle bundle) {
        String[] exceptionParts;
        if (exception.contains("#|#")) {
            exceptionParts = exception.split(DIVIDER);
            return String.format(bundle.getString(exceptionParts[0]), exceptionParts[1]);
        } else {
            return bundle.getString(exception);
        }
    }

    public static ResourceBundle getResourceBundle(HttpSession session) {
        String language = (String) session.getAttribute(LANGUAGE);
        Locale locale = (language == null || language.equals(RU_LOWER))
                ? new Locale(RU_LOWER, RU_UPPER)
                : new Locale(EN_LOWER, EN_UPPER);
        return ResourceBundle.getBundle(APPLICATION_LANGUAGE, locale);
    }

    public static Session getMailSession(Properties properties) {
        return Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(properties.getProperty(MAIL_FROM),
                                properties.getProperty(MAIL_SMTP_PASSWORD));
                    }
                });
    }

    public static User buildUser(HttpServletRequest request){
        User user = new User();
        user.setName(request.getParameter(NAME));
        user.setSurname(request.getParameter(SURNAME));
        user.setPatronymic(request.getParameter(PATRONYMIC));
        user.setEmail(request.getParameter(EMAIL));
        user.setTelephone(request.getParameter(TELEPHONE));
        user.setPassword(request.getParameter(PASSWORD));
        user.setJobPlace(request.getParameter(JOB_PLACE));
        return user;
    }
}
