package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static constant.ControllerConstant.*;

public class SessionAliveCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (request.getSession().getAttribute(USER) == null) {
                response.getWriter().write(new Gson().toJson(EMPTY));
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static SessionAliveCommand ourInstance = new SessionAliveCommand();

    public static SessionAliveCommand getInstance() {
        return ourInstance;
    }

    private SessionAliveCommand() {
    }
}
