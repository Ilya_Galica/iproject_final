package controller.comand.ajax.impl;

import controller.comand.ajax.AjaxCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static constant.ControllerConstant.*;

public class ProfilePageExitCommand implements AjaxCommand {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.removeAttribute(USER);
        session.removeAttribute(USER_ORDERS);
    }

    private static final ProfilePageExitCommand instance = new ProfilePageExitCommand();

    private ProfilePageExitCommand() {
    }

    public static ProfilePageExitCommand getInstance() {
        return instance;
    }
}
