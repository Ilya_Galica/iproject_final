package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import dao.entity.Entity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.*;
import service.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static constant.ControllerConstant.*;

public class FindAllRecordAdminCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession httpSession = request.getSession();
        String command = request.getParameter(AJAX_COMMAND);
        String json;
        List<Entity> entityList;
        switch (command) {
            case FIND_ALL_USER_ADMIN:
                UserService userService = UserServiceImpl.getInstance();
                entityList = userService.findAll();
                httpSession.setAttribute(USERS, entityList);
                break;
            case FIND_ALL_DISCOUNT_ADMIN:
                DiscountService discountService = DiscountServiceImpl.getInstance();
                entityList = discountService.findAll();
                httpSession.setAttribute(DISCOUNTS, entityList);
                break;
            case FIND_ALL_ORDER_ADMIN:
                OrderService orderService = OrderServiceImpl.getInstance();
                entityList = orderService.findAll();
                httpSession.setAttribute(ORDERS, entityList);
                break;
            case FIND_ALL_HOTEL_ADMIN:
                HotelService hotelService = HotelServiceImpl.getInstance();
                entityList = hotelService.findAll();
                httpSession.setAttribute(HOTELS, entityList);
                break;
            case FIND_ALL_HOTELROOM_ADMIN:
                HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
                entityList = hotelroomService.findAll();
                httpSession.setAttribute(HOTELROOM_LIST, entityList);
                break;
            default:
                entityList = Collections.emptyList();
                break;
        }
        try {
            response.setContentType("application/json");
            json = new Gson().toJson(entityList);
            response.getWriter().write(json);
        } catch (IOException e1) {
            logger.error(e1.getMessage(), e1);
        }
    }

    private static final FindAllRecordAdminCommand instance = new FindAllRecordAdminCommand();

    private FindAllRecordAdminCommand() {
    }

    public static FindAllRecordAdminCommand getInstance() {
        return instance;
    }
}
