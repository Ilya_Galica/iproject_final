package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.ServiceException;
import service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.util.ResourceBundle;

import static constant.ControllerConstant.*;
import static controller.util.ControllerUtil.*;

public class SignInCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String email = request.getParameter(LOGIN_EMAIL);
        String password = request.getParameter(PASSWORD);
        String check = request.getParameter(AUTO_CHECK);
        String json;
        ResourceBundle bundle = getResourceBundle(session);
        try {
            if (!CHECK.equals(check)) {
                UserService userService = UserServiceImpl.getInstance();
                User user = userService.signIn(email, password);
                if (session.getAttribute(USER) == null) {
                    json = new Gson().toJson(user);
                    session.setAttribute(USER, user);
                } else {
                    json = new Gson().toJson(session.getAttribute(USER));
                }
            } else {
                if (session.getAttribute(USER) != null) {
                    json = ((User) session.getAttribute(USER)).getName();
                } else {
                    json = bundle.getString(AUTHORISATION);
                }
            }
            response.getWriter().write(json);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } catch (ServiceException | SuchUserNotFoundException e) {
            logger.error(localeException(e.getMessage(), bundle), e);
            try {
                json = new Gson().toJson(ERROR + localeException(e.getMessage(), bundle));
                response.getWriter().write(json);
            } catch (IOException e1) {
                logger.error(e1.getMessage(), e);
            }
        }
    }

    private static final SignInCommand instance = new SignInCommand();

    private SignInCommand() {
    }

    public static SignInCommand getInstance() {
        return instance;
    }
}
