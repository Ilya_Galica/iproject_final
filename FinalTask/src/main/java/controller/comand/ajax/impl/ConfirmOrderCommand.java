package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import controller.util.ControllerUtil;
import controller.util.Miler;
import dao.entity.impl.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.OrderService;
import service.exception.ServiceException;
import service.impl.OrderServiceImpl;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

import java.util.Optional;
import java.util.ResourceBundle;

import static constant.ControllerConstant.*;
import static controller.util.ControllerUtil.encoding;


public class ConfirmOrderCommand implements AjaxCommand {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession httpSession = request.getSession();
        Order order = (Order) request.getSession().getAttribute(ORDER);
        ResourceBundle bundle = ControllerUtil.getResourceBundle(httpSession);
        String subject = bundle.getString("subject.email.message");
        String responseText = buildResponseText(order, bundle);
        String receiverEmailAddress = order.getUser().getEmail();
        OrderService orderService = OrderServiceImpl.getInstance();
        String json;
        try {
            Miler.loadProperties(request);
            if (((Order) httpSession.getAttribute(ORDER)).getId() == 0) {
                Optional<Order> optionalOrder = orderService.createOrder(order);
                if (optionalOrder.isPresent()) {
                    Session session = ControllerUtil.getMailSession(Miler.getProperties());
                    MimeMessage message = Miler.buildMimeMessage(session, receiverEmailAddress, subject, responseText);
                    Miler.doTransport(session, message);
                    json = new Gson().toJson(
                            String.format(bundle.getString("letter.have.sanded"),
                                    encoding(order.getUser().getName()), order.getUser().getSurname()));
                    response.getWriter().write(json);
                    httpSession.removeAttribute(ORDER);
                } else {
                    json = new Gson().toJson(bundle.getString("empty.order"));
                    response.getWriter().write(json);
                }
            }
        } catch (RuntimeException | IOException e) {
            logger.error(e.getMessage(), e);
        } catch (ServiceException e) {
            logger.error(bundle.getString(e.getMessage()), e);
            handleException(response, bundle, e.getMessage(), e);
        } catch (MessagingException e) {
            logger.error(e.getMessage(), e);
            handleException(response, bundle, "letter.not.send", e);
        }

    }

    private static String buildResponseText(Order order, ResourceBundle bundle) {
        return String.format(bundle.getString("response.text.email.message"),
                order.getUser().getName(), order.getUser().getPatronymic(),
                order.getArrivalData(), order.getDepartureData(),
                order.getHotelroom().getHotel().getName(), order.getHotelroom().getNumber(),
                order.getTotalPrice());
    }

    private static void handleException(HttpServletResponse response,
                                        ResourceBundle bundle, String message,
                                        Exception e) {
        try {
            response.getWriter().write(new Gson().toJson(bundle.getString(message)));
        } catch (IOException e1) {
            logger.error(e.getMessage(), e);
        }
    }

    private static final ConfirmOrderCommand instance = new ConfirmOrderCommand();

    private ConfirmOrderCommand() {
    }

    public static ConfirmOrderCommand getInstance() {
        return instance;
    }

}
