package controller.comand.ajax.impl;

import controller.comand.ajax.AjaxCommand;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.OrderService;
import service.UserService;
import service.exception.ServiceException;
import service.impl.OrderServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static constant.ControllerConstant.*;

public class DeleteOrderCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Order order = (Order) session.getAttribute(ORDER);
        session.removeAttribute(ORDER);
        User user;
        if (session.getAttribute(USER) == null) {
            user = order.getUser();
        } else {
            user = (User) session.getAttribute(USER);
            session.removeAttribute(USER);
        }
        OrderService orderService = OrderServiceImpl.getInstance();
        UserService userService = UserServiceImpl.getInstance();
        try {
            orderService.deleteOrder(order);
            userService.delete(user);
        } catch (ServiceException e) {
            logger.error(e.getMessage(),e);
        }
    }

    private static DeleteOrderCommand ourInstance = new DeleteOrderCommand();

    public static DeleteOrderCommand getInstance() {
        return ourInstance;
    }

    private DeleteOrderCommand() {
    }


}
