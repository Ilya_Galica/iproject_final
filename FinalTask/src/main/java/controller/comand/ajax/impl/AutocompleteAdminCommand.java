package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static constant.ControllerConstant.AJAX_COMMAND;
import static constant.ControllerConstant.TERM;

public class AutocompleteAdminCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        List<String> list = null;
        String term = request.getParameter(TERM);
        String autocomplete = request.getParameter(AJAX_COMMAND);
        switch (autocomplete) {
            case "autocompleteUserCommand":
                UserServiceImpl userService = UserServiceImpl.getInstance();
                list = userService.findAllRelevantId(term);
                break;
            case "autocompleteDiscountCommand":
                DiscountServiceImpl discountService = DiscountServiceImpl.getInstance();
                list = discountService.findAllRelevantId(term);
                break;
            case "autocompleteOrderCommand":
                OrderServiceImpl orderService = OrderServiceImpl.getInstance();
                list = orderService.findAllRelevantId(term);
                break;
            case "autocompleteHotelCommand":
                HotelServiceImpl hotelService = HotelServiceImpl.getInstance();
                list = hotelService.findAllRelevantId(term);
                break;
            case "autocompleteHotelroomCommand":
                HotelroomServiceImpl hotelroomService = HotelroomServiceImpl.getInstance();
                list = hotelroomService.findAllRelevantId(term);
                break;
        }
        String json = new Gson().toJson(list);
        try {
            response.getWriter().write(json);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    private static AutocompleteAdminCommand ourInstance = new AutocompleteAdminCommand();

    public static AutocompleteAdminCommand getInstance() {
        return ourInstance;
    }

    private AutocompleteAdminCommand() {
    }

}
