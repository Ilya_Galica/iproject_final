package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import dao.entity.Entity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.*;
import service.exception.ServiceException;
import service.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static constant.ControllerConstant.*;

public class FindRecordAdminCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter(FIND_ID);
        String command = request.getParameter(AJAX_COMMAND);
        String json;
        Entity entity = null;
        switch (command) {
            case FIND_USER_ADMIN:
                try {
                    UserService userService = UserServiceImpl.getInstance();
                    entity = userService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage(), e);
                }
                break;
            case FIND_HOTEL_ADMIN:
                try {
                    HotelService hotelService = HotelServiceImpl.getInstance();
                    entity = hotelService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage(), e);
                }
                break;
            case FIND_DISCOUNT_ADMIN:
                try {
                    DiscountService discountService = DiscountServiceImpl.getInstance();
                    entity = discountService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage(), e);
                }
                break;
            case FIND_ORDER_ADMIN:
                try {
                    OrderService orderService = OrderServiceImpl.getInstance();
                    entity = orderService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage(), e);
                }
                break;
            case FIND_HOTELROOM_ADMIN:
                try {
                    HotelroomService hotelService = HotelroomServiceImpl.getInstance();
                    entity = hotelService.findById(Integer.parseInt(id));
                } catch (ServiceException e) {
                    logger.error(e.getMessage(), e);
                }
                break;
        }
        try {
            response.setContentType("application/json");
            json = new Gson().toJson((entity != null)
                    ? new ArrayList<>(Collections.singletonList(entity))
                    : null);
            response.getWriter().write(json);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static final FindRecordAdminCommand instance = new FindRecordAdminCommand();

    private FindRecordAdminCommand() {
    }

    public static FindRecordAdminCommand getInstance() {
        return instance;
    }
}
