package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import dao.entity.impl.Discount;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.DiscountService;
import service.HotelService;
import service.HotelroomService;
import service.impl.DiscountServiceImpl;
import service.impl.HotelServiceImpl;
import service.impl.HotelroomServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static constant.ControllerConstant.*;

public class CreateRecordAdminCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        String command = request.getParameter(AJAX_COMMAND);
        String json;
        boolean isCreate = false;
        switch (command) {
            case CREATE_DISCOUNT_ADMIN:
                Discount discount = new Discount();
                discount.setUserJobPlace(request.getParameter(INSERT_USER_JOB_PLACE));
                discount.setDiscountValueForJobPlace(Integer
                        .parseInt(request.getParameter(INSERT_DISCOUNT_FOR_USER_JOB_PLACE)));
                DiscountService discountService = DiscountServiceImpl.getInstance();
                isCreate = discountService.create(discount);
                break;
            case CREATE_HOTELROOM_ADMIN:
                Hotelroom hotelroom = new Hotelroom();
                hotelroom.setNumber(Integer.parseInt(request.getParameter(INSERT_NUMBER)));
                Hotel hotelroomHotel = new Hotel();
                hotelroomHotel.setId(Integer.parseInt(request.getParameter(INSERT_HOTEL_ID)));
                hotelroom.setHotel(hotelroomHotel);
                hotelroom.setPrice(Integer.parseInt(request.getParameter(INSERT_PRICE)));
                hotelroom.setPeopleCapacity(Integer.parseInt(request.getParameter(INSERT_CAPACITY)));
                hotelroom.setComfort(request.getParameter(INSERT_COMFORT));
                hotelroom.setImageURL(request.getParameter(INSERT_IMG_URL));
                hotelroom.setDescription(request.getParameter(INSERT_DESCRIPTION));
                hotelroom.setDescriptionEn(request.getParameter(INSERT_DESCRIPTION_EN));
                HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
                isCreate = hotelroomService.create(hotelroom);
                break;
            case CREATE_HOTEL_ADMIN:
                Hotel hotel = new Hotel();
                hotel.setName(request.getParameter(INSERT_HOTEL_NAME));
                hotel.setCity(request.getParameter(INSERT_CITY));
                hotel.setCountry(request.getParameter(INSERT_COUNTRY));
                HotelService hotelService = HotelServiceImpl.getInstance();
                isCreate = hotelService.create(hotel);
                break;
        }
        try {
            response.setContentType("application/json");
            json = new Gson().toJson((isCreate)
                    ? "Record was created"
                    : "Check your input");
            response.getWriter().write(json);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    private static final CreateRecordAdminCommand instance = new CreateRecordAdminCommand();

    private CreateRecordAdminCommand() {
    }

    public static CreateRecordAdminCommand getInstance() {
        return instance;
    }
}
