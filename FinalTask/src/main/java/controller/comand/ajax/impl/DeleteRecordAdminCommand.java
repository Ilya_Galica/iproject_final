package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import dao.entity.impl.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.*;
import service.impl.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static constant.ControllerConstant.*;

public class DeleteRecordAdminCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter(DELETE_ID);
        String command = request.getParameter(AJAX_COMMAND);
        String json;
        boolean isDelete = false;
        switch (command) {
            case DELETE_USER_ADMIN:
                User user = new User();
                user.setId(Integer.parseInt(id));
                UserService userService = UserServiceImpl.getInstance();
                isDelete = userService.delete(user);
                break;
            case DELETE_DISCOUNT_ADMIN:
                Discount discount = new Discount();
                discount.setId(Integer.parseInt(id));
                DiscountService discountService = DiscountServiceImpl.getInstance();
                isDelete = discountService.delete(discount);
                break;
            case DELETE_HOTELROOM_ADMIN:
                Hotelroom hotelroom = new Hotelroom();
                hotelroom.setId(Integer.parseInt(id));
                HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
                isDelete = hotelroomService.delete(hotelroom);
                break;
            case DELETE_ORDER_ADMIN:
                Order order = new Order();
                order.setId(Integer.parseInt(id));
                OrderService orderService = OrderServiceImpl.getInstance();
                isDelete = orderService.delete(order);
                break;
            case DELETE_HOTEL_ADMIN:
                Hotel hotel = new Hotel();
                hotel.setId(Integer.parseInt(id));
                HotelService hotelService = HotelServiceImpl.getInstance();
                isDelete = hotelService.delete(hotel);
                break;
        }
        try {
            response.setContentType("application/json");
            json = new Gson().toJson((isDelete)
                    ? "Record with id: " + id + " was deleted"
                    : "Check your id");
            response.getWriter().write(json);
        } catch (IOException e1) {
            logger.error(e1);
        }
    }

    private static final DeleteRecordAdminCommand instance = new DeleteRecordAdminCommand();

    private DeleteRecordAdminCommand() {
    }

    public static DeleteRecordAdminCommand getInstance() {
        return instance;
    }
}
