package controller.comand.ajax.impl;

import controller.comand.ajax.AjaxCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static constant.ControllerConstant.*;

public class ChangeLanguageCommand implements AjaxCommand {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        String ru = request.getParameter(LOCALE_RU);
        String en = request.getParameter(LOCALE_EN);
        String language = (ru == null) ? en : ru;
        HttpSession languageSession = request.getSession();
        if (languageSession.getAttribute(LOCALE) == null
                || !languageSession.getAttribute(LOCALE).equals(language)) {
            languageSession.setAttribute(LANGUAGE, language);
        }
    }

    private static final ChangeLanguageCommand instance = new ChangeLanguageCommand();

    private ChangeLanguageCommand() {
    }

    public static ChangeLanguageCommand getInstance() {
        return instance;
    }
}
