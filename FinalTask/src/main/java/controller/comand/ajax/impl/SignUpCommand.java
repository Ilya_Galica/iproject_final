package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import controller.util.ControllerUtil;
import controller.util.Miler;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.OrderService;
import service.UserService;
import service.exception.ServiceException;
import service.impl.OrderServiceImpl;
import service.impl.UserServiceImpl;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.util.ResourceBundle;

import static constant.ControllerConstant.*;
import static controller.util.ControllerUtil.*;

public class SignUpCommand implements AjaxCommand {
    private static final Logger logger = LogManager.getLogger();
    private static final UserService userService = UserServiceImpl.getInstance();
    private boolean emailExist = true;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession httpSession = request.getSession();
        User user = buildUser(request);
        String inputKey = request.getParameter(INPUT_KEY);
        OrderService orderService = OrderServiceImpl.getInstance();
        ResourceBundle bundle = getResourceBundle(request.getSession());
        String json;
        try {
            if ((inputKey == null && orderService.isSuchEmailExist(user))) {
                String secureKey;
                secureKey = orderService.getSecretKeyForDuplicateEmail(user);
                Miler.loadProperties(request);
                Session sess = ControllerUtil.getMailSession(Miler.getProperties());
                MimeMessage message = Miler
                        .buildMimeMessage(sess, user.getEmail(), bundle.getString("secret.key"), secureKey);
                Miler.doTransport(sess, message);
                json = new Gson().toJson(secureKey);
                httpSession.setAttribute(INPUT_KEY_SIGN,secureKey);
                response.getWriter().write(json);
            } else if(inputKey == null && !orderService.isSuchEmailExist(user)
                    || inputKey != null && inputKey
                    .equalsIgnoreCase(String.valueOf(httpSession.getAttribute(INPUT_KEY_SIGN)))){
                if (userService.signUp(user)) {
                    httpSession.setAttribute(USER, user);
                }
                emailExist = true;
                json = new Gson().toJson(user);
                httpSession.removeAttribute(INPUT_KEY_SIGN);
                response.getWriter().write(json);
            }

        } catch (IOException | MessagingException e) {
            logger.error(e.getMessage(), e);
        } catch (ServiceException e) {
            try {
                json = new Gson().toJson(ERROR + localeException(e.getMessage(), bundle));
                response.getWriter().write(json);
            } catch (IOException e1) {
                logger.error(e1.getMessage(), e);
            }
        }
    }

    private static final SignUpCommand instance = new SignUpCommand();

    private SignUpCommand() {
    }

    public static SignUpCommand getInstance() {
        return instance;
    }
}
