package controller.comand.ajax.impl;

import com.google.gson.Gson;
import controller.comand.ajax.AjaxCommand;
import controller.util.Miler;
import dao.entity.HotelSearchDataContainer;
import dao.entity.impl.Hotelroom;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.HotelroomService;
import service.OrderService;
import service.exception.ServiceException;
import service.impl.HotelroomServiceImpl;
import service.impl.OrderServiceImpl;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ResourceBundle;

import static constant.ControllerConstant.*;
import static controller.util.ControllerUtil.*;

public class EmailExistenceCommand implements AjaxCommand {

    private static final Logger logger = LogManager.getLogger();
    private static OrderService orderService = OrderServiceImpl.getInstance();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession httpSession = request.getSession();
        String authentication = request.getParameter(AUTH);
        String inputKey = request.getParameter(INPUT_KEY);
        HotelSearchDataContainer container = (HotelSearchDataContainer) httpSession.getAttribute(DATA_CONTAINER);
        Hotelroom hotelroom = (Hotelroom) httpSession.getAttribute(HOTELROOM);
        User user = (httpSession.getAttribute(USER) != null)
                ? (User) httpSession.getAttribute(USER)
                : buildUser(request);
        Order order = buildOrder(container, user, hotelroom);
        ResourceBundle bundle = getResourceBundle(httpSession);
        HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
        String json;
        boolean isFreeHotelroom = hotelroomService.isFreeHotelroom(container, hotelroom);
        try {
            orderService.countDiscount(order);
            order.setTotalPrice(orderService.countTotalPrice(order));
            if (isFreeHotelroom) {
                if ((inputKey == null && !orderService.isSuchEmailExist(user))
                        || (inputKey != null && inputKey
                        .equalsIgnoreCase(String.valueOf(httpSession.getAttribute(INPUT_KEY))))
                        || httpSession.getAttribute(USER) != null) {
                    httpSession.setAttribute(ORDER, order);
                    request.setAttribute(GO_NEXT, "");
                    json = new Gson().toJson(OK);
                    response.getWriter().write(json);
                    httpSession.removeAttribute(INPUT_KEY);
                } else if ((inputKey == null && orderService.isSuchEmailExist(user) && authentication == null)) {
                    sendMail(request, response, user, bundle);
                }
            } else {
                request.setAttribute(GO_NEXT, "");
            }

        } catch (RuntimeException e) {
            logger.error(e.getMessage(), e);
            catchExceptions(response, e);
        } catch (IOException | MessagingException e) {
            logger.error(e.getMessage(), e);
        } catch (ServiceException e) {
            logger.error(localeException(e.getMessage(), bundle), e);
            try {
                json = new Gson().toJson(localeException(e.getMessage(), bundle));
                response.getWriter().write(json);
            } catch (IOException e1) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    private static Order buildOrder(HotelSearchDataContainer container, User user, Hotelroom hotelroom) {
        Order order = new Order();
        order.setArrivalData(container.getArrivalData());
        order.setDepartureData(container.getDepartureData());
        order.setUser(user);
        order.setHotelroom(hotelroom);
        return order;
    }

    private void catchExceptions(HttpServletResponse response, Exception e) {
        try {
            response.getWriter().write(new Gson().toJson(ERROR + e.getMessage()));
        } catch (IOException e1) {
            logger.error(e.getMessage(), e1);
        }
    }

    private void sendMail(HttpServletRequest request, HttpServletResponse response, User user, ResourceBundle bundle) throws IOException, MessagingException {
        request.getSession().removeAttribute(INPUT_KEY);
        String secureKey;
        secureKey = orderService.getSecretKeyForDuplicateEmail(user);
        Miler.loadProperties(request);
        Session session = getMailSession(Miler.getProperties());
        MimeMessage message = Miler
                .buildMimeMessage(session, user.getEmail(), bundle.getString("secret.key"), secureKey);
        Miler.doTransport(session, message);
        String json = new Gson().toJson(secureKey);
        response.getWriter().write(json);
        request.getSession().setAttribute(INPUT_KEY, secureKey);
    }

    private static final EmailExistenceCommand instance = new EmailExistenceCommand();

    private EmailExistenceCommand() {
    }

    public static EmailExistenceCommand getInstance() {
        return instance;
    }
}
