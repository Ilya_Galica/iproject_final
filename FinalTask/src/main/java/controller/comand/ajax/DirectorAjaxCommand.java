package controller.comand.ajax;

import controller.comand.ajax.impl.*;

import java.util.HashMap;
import java.util.Map;

public class DirectorAjaxCommand {
    private Map<String, AjaxCommand> map = new HashMap<>();
    private static final DirectorAjaxCommand instance = new DirectorAjaxCommand();

    public static DirectorAjaxCommand getInstance() {
        return instance;
    }

    private DirectorAjaxCommand() {
        map.put("confirmOrder", ConfirmOrderCommand.getInstance());
        map.put("emailExist", EmailExistenceCommand.getInstance());
        map.put("signIn", SignInCommand.getInstance());
        map.put("signUp", SignUpCommand.getInstance());
        map.put("findAllUserAdmin", FindAllRecordAdminCommand.getInstance());
        map.put("findAllDiscountAdmin", FindAllRecordAdminCommand.getInstance());
        map.put("findAllOrderAdmin", FindAllRecordAdminCommand.getInstance());
        map.put("findAllHotelAdmin", FindAllRecordAdminCommand.getInstance());
        map.put("findAllHotelroomAdmin", FindAllRecordAdminCommand.getInstance());
        map.put("findDiscountAdmin", FindRecordAdminCommand.getInstance());
        map.put("findUserAdmin", FindRecordAdminCommand.getInstance());
        map.put("findHotelAdmin", FindRecordAdminCommand.getInstance());
        map.put("findOrderAdmin", FindRecordAdminCommand.getInstance());
        map.put("findHotelroomAdmin", FindRecordAdminCommand.getInstance());
        map.put("deleteUserAdmin", DeleteRecordAdminCommand.getInstance());
        map.put("deleteDiscountAdmin", DeleteRecordAdminCommand.getInstance());
        map.put("deleteHotelroomAdmin", DeleteRecordAdminCommand.getInstance());
        map.put("deleteOrderAdmin", DeleteRecordAdminCommand.getInstance());
        map.put("deleteHotelAdmin", DeleteRecordAdminCommand.getInstance());
        map.put("createDiscountAdmin", CreateRecordAdminCommand.getInstance());
        map.put("createHotelroomAdmin", CreateRecordAdminCommand.getInstance());
        map.put("createHotelAdmin", CreateRecordAdminCommand.getInstance());
        map.put("profileExit", ProfilePageExitCommand.getInstance());
        map.put("autocompleteUserCommand", AutocompleteAdminCommand.getInstance());
        map.put("autocompleteDiscountCommand", AutocompleteAdminCommand.getInstance());
        map.put("autocompleteOrderCommand", AutocompleteAdminCommand.getInstance());
        map.put("autocompleteHotelCommand", AutocompleteAdminCommand.getInstance());
        map.put("autocompleteHotelroomCommand", AutocompleteAdminCommand.getInstance());
        map.put("aliveSession", SessionAliveCommand.getInstance());
        map.put("changeLanguage", ChangeLanguageCommand.getInstance());
        map.put("deleteOrder", DeleteOrderCommand.getInstance());
    }

    public AjaxCommand getCommand(String commandKey) {
        return map.get(commandKey);
    }
}
