package controller.comand.ajax;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AjaxCommand {

    void execute(HttpServletRequest request, HttpServletResponse response);
}
