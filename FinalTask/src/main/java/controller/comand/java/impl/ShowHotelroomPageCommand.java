package controller.comand.java.impl;

import static constant.ControllerConstant.*;

import controller.comand.java.Command;
import dao.entity.HotelSearchDataContainer;
import dao.entity.impl.Hotelroom;
import service.HotelroomService;
import service.impl.HotelroomServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ShowHotelroomPageCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String hotelroomPosition = request.getParameter(HOTELROOM_POSITION);
        @SuppressWarnings("unchecked")
        List<Hotelroom> hotelroomList = (List<Hotelroom>) session.getAttribute(HOTELROOMS);
        Hotelroom hotelroom = hotelroomList.get(Integer.parseInt(hotelroomPosition));
        HotelSearchDataContainer container
                = (HotelSearchDataContainer) session.getAttribute(DATA_CONTAINER);
        HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
        String arrivalData = container.getArrivalData();
        String departureData = container.getDepartureData();
        Integer dayAmount = hotelroomService
                .getDayAmount(arrivalData, departureData);
        session.setAttribute(HOTELROOM, hotelroom);
        session.setAttribute(DAY_AMOUNT, dayAmount);
        return "/" + HOTELS_PACKAGE + HOTELROOM_PACKAGE + SHOW_HOTELROOM_PAGE;
    }

    private static final ShowHotelroomPageCommand instance = new ShowHotelroomPageCommand();

    private ShowHotelroomPageCommand() {
    }

    public static ShowHotelroomPageCommand getInstance() {
        return instance;
    }
}
