package controller.comand.java.impl;

import controller.comand.java.Command;
import controller.util.ControllerUtil;
import dao.entity.HotelSearchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotelroom;
import service.HotelroomService;
import service.impl.HotelroomServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.ResourceBundle;

import static constant.ControllerConstant.*;
import static controller.util.ControllerUtil.encoding;

public class SearchHotelCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        HotelSearchDataContainer container;
        Pagination pagination;
        ResourceBundle bundle = ControllerUtil.getResourceBundle(session);
        HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
        if (isRequestAttrInitialised(request)) {
            container = setConteinerData(request);
            pagination = setPagination(request, container, hotelroomService);
        } else {
            container = (HotelSearchDataContainer) request.getSession().getAttribute(DATA_CONTAINER);
            pagination = (Pagination) request.getSession().getAttribute(PAGINATION);
            pagination.setCurrentPage((request.getParameter(CURRENT_PAGE) != null)
                    ? Integer.parseInt(request.getParameter(CURRENT_PAGE))
                    : 1);
        }
        List<Hotelroom> hotelroomList = null;
        if (container != null) {
            hotelroomList =
                    hotelroomService.showAll(container, pagination);
        }
        session.setAttribute(DATA_CONTAINER, container);
        session.setAttribute(PAGINATION, pagination);
        session.setAttribute(HOTELROOMS, hotelroomList);
        if (hotelroomList != null && hotelroomList.isEmpty()) {
            request.setAttribute(BAD_REQUEST, bundle.getString("dont.find.on.your.request"));
        }
        return "/" + HOTELS_PACKAGE + SHOW_REQUEST_HOTELS_PAGE;
    }


    private Pagination setPagination(HttpServletRequest request,
                                     HotelSearchDataContainer container,
                                     HotelroomService hotelroomService) {
        Pagination pagination = new Pagination();
        pagination
                .setCurrentPage((request.getParameter(CURRENT_PAGE) != null)
                        ? Integer.parseInt(request.getParameter(CURRENT_PAGE))
                        : 1);
        pagination
                .setAllPageAmount(hotelroomService
                        .getAmountOfValidHotelrooms(container));
        int paginationAmount = (pagination.getAllPageAmount() % Pagination.HOTEL_PER_PAGE != 0)
                ? (pagination.getAllPageAmount() / Pagination.HOTEL_PER_PAGE) + 1
                : pagination.getAllPageAmount() / Pagination.HOTEL_PER_PAGE;
        pagination.setPaginationAmount(paginationAmount);
        return pagination;
    }

    private HotelSearchDataContainer setConteinerData(HttpServletRequest request) {
        HotelSearchDataContainer container = new HotelSearchDataContainer();
        container.setCity(encoding(request.getParameter(CITY)));
        container.setArrivalData(request.getParameter(ARRIVAL_DATA));
        container.setDepartureData(request.getParameter(DEPARTURE_DATA));
        container.setPeopleCapacity(Integer.parseInt(request.getParameter(PEOPLE_AMOUNT)));
        return container;
    }

    private boolean isRequestAttrInitialised(HttpServletRequest request) {
        return request.getParameter(CITY) != null
                && request.getParameter(ARRIVAL_DATA) != null
                && request.getParameter(DEPARTURE_DATA) != null
                && request.getParameter(PEOPLE_AMOUNT) != null;
    }

    private static final SearchHotelCommand instance = new SearchHotelCommand();

    private SearchHotelCommand() {
    }

    public static SearchHotelCommand getInstance() {
        return instance;
    }
}
