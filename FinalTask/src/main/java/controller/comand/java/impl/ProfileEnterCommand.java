package controller.comand.java.impl;

import static constant.ControllerConstant.*;

import controller.comand.java.Command;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import service.UserService;
import service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ProfileEnterCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String role;
        if (session.getAttribute(USER) != null) {
            role = String.valueOf(((User) session.getAttribute(USER)).getRole());
            if (role != null && role.equalsIgnoreCase(USER)) {
                UserService service = UserServiceImpl.getInstance();
                List<Order> orders = service
                        .showUserOrders(((User) session.getAttribute(USER)).getEmail());
                if (!orders.isEmpty()) {
                    session.setAttribute(USER_ORDERS, orders);
                }
                return "/" + USER_PACKAGE + PERSONAL_USER_PAGE;
            } else {
                return ADMIN;
            }
        } else {
            return "/" + INDEX_PAGE;
        }
    }

    private static final ProfileEnterCommand instance = new ProfileEnterCommand();

    private ProfileEnterCommand() {
    }

    public static ProfileEnterCommand getInstance() {
        return instance;
    }
}
