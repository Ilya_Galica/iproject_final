package controller.comand.java.impl;

import controller.comand.java.Command;
import controller.util.ControllerUtil;
import dao.entity.HotelSearchDataContainer;
import dao.entity.impl.Hotelroom;
import dao.entity.impl.Order;
import service.HotelroomService;
import service.impl.HotelroomServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

import static constant.ControllerConstant.*;

public class CreateOrderCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        ResourceBundle bundle = ControllerUtil.getResourceBundle(session);
        HotelroomService hotelroomService = HotelroomServiceImpl.getInstance();
        HotelSearchDataContainer container = (HotelSearchDataContainer) session
                .getAttribute(DATA_CONTAINER);
        Order order = ((Order) session.getAttribute(ORDER));
        Hotelroom hotelroom;
        if (order != null) {
            hotelroom = order.getHotelroom();
        } else {
            request.setAttribute(NOT_AVAILABLE_HOTELROOM, bundle.getString("not.available.hotelroom"));
            return "/" + HOTELS_PACKAGE + HOTELROOM_PACKAGE + SHOW_HOTELROOM_PAGE;
        }
        boolean isGoForward = hotelroomService.isFreeHotelroom(container, hotelroom);
        if (isGoForward) {
            return "/" + HOTELS_PACKAGE + HOTELROOM_PACKAGE + ORDER_PACKAGE + CONFIRM_ORDER_PAGE;
        } else {
            request.setAttribute(NOT_AVAILABLE_HOTELROOM, bundle.getString("not.available.hotelroom"));
            return "/" + HOTELS_PACKAGE + HOTELROOM_PACKAGE + SHOW_HOTELROOM_PAGE;
        }

    }

    private CreateOrderCommand() {
    }

    private static final CreateOrderCommand instance = new CreateOrderCommand();

    public static CreateOrderCommand getInstance() {
        return instance;
    }
}
