package controller.comand.java.impl;

import controller.comand.java.Command;
import controller.util.ControllerUtil;
import dao.entity.impl.User;
import service.UserService;
import service.exception.ServiceException;
import service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

import static constant.ControllerConstant.*;

public class ChangePersonalDataCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
        String param;
        UserService userService = UserServiceImpl.getInstance();
        HttpSession httpSession = request.getSession();
        boolean changedPassword;

        ResourceBundle bundle = ControllerUtil.getResourceBundle(httpSession);
        User user = (User) httpSession.getAttribute(USER);
        try {
            if (httpSession.getAttribute(USER) == null) {
                return INDEX_PAGE;
            }
            if ((param = request.getParameter(EMAIL)) != null) {
                user = userService.changePersonalInfo(param, EMAIL, user);
            } else if ((param = request.getParameter(PASSWORD)) != null) {
                changedPassword = userService
                        .isChangePersonalInfo(param, request.getParameter(PASSWORD_NEW), user);
                if (changedPassword) {
                    request.setAttribute(PASSWORD_CHANGES, bundle.getString("change.password.sucsess"));
                    httpSession.setAttribute(USER, user);
                }
                return "/" + USER_PACKAGE + PERSONAL_USER_PAGE;
            } else if ((param = request.getParameter(TELEPHONE)) != null) {
                user = userService.changePersonalInfo(param, TELEPHONE, user);
            } else if ((param = request.getParameter(JOB)) != null) {
                user = userService.changePersonalInfo(param, JOB, user);
            } else {
                return INDEX_PAGE;
            }
            httpSession.removeAttribute(USER);
            httpSession.setAttribute(USER, user);
            request.setAttribute(CHANGE_PERSONAL_DATA, bundle
                    .getString("is.change.personal.data.successful"));
        } catch (ServiceException e) {
            request.setAttribute(CHANGE_PERSONAL_DATA, bundle.getString(e.getMessage()));
        }
        return "/" + USER_PACKAGE + PERSONAL_USER_PAGE;
    }

    private static final ChangePersonalDataCommand instance = new ChangePersonalDataCommand();

    private ChangePersonalDataCommand() {
    }

    public static ChangePersonalDataCommand getInstance() {
        return instance;
    }
}
