package controller.comand.java.impl;

import static constant.ControllerConstant.*;

import controller.comand.java.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ProfileExitCommand implements Command {

    public String execute(HttpServletRequest request) {
        String pageName = request.getParameter(PAGE_NAME);
        HttpSession session = request.getSession();
        session.removeAttribute(USER);
        return pageName;
    }

    private static final ProfileExitCommand instance = new ProfileExitCommand();

    private ProfileExitCommand() {
    }

    public static ProfileExitCommand getInstance() {
        return instance;
    }
}
