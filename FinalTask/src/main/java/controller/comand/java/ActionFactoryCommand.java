package controller.comand.java;

import javax.servlet.http.HttpServletRequest;

public class ActionFactoryCommand {
    public static String getForwardPage(String currentCommand,
                                 HttpServletRequest request) {
        DirectorCommand directorCommand = DirectorCommand.getInstance();
        Command command = directorCommand.getCommand(currentCommand);
        return command.execute(request);
    }
}
