package controller.comand.java;

import controller.comand.java.impl.*;

import java.util.HashMap;
import java.util.Map;

class DirectorCommand {
    private Map<String, Command> map = new HashMap<>();
    private static final DirectorCommand instance = new DirectorCommand();

    static DirectorCommand getInstance() {
        return instance;
    }

    private DirectorCommand() {
        map.put("searchHotels", SearchHotelCommand.getInstance());
        map.put("pagination", SearchHotelCommand.getInstance());
        map.put("hotelroomPage", ShowHotelroomPageCommand.getInstance());
        map.put("profileExit", ProfileExitCommand.getInstance());
        map.put("profileEnter", ProfileEnterCommand.getInstance());
        map.put("changeData", ChangePersonalDataCommand.getInstance());
        map.put("createOrder", CreateOrderCommand.getInstance());
    }

    Command getCommand(String commandKey) {
        return map.get(commandKey);
    }
}
