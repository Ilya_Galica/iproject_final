package constant;

public final class ControllerConstant {
    
    public static final String CITY = "city";
    public static final String ARRIVAL_DATA = "arrival";
    public static final String DEPARTURE_DATA = "departure";
    public static final String PEOPLE_AMOUNT = "amount";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String DATA_CONTAINER = "dataContainer";
    public static final String NOT_AVAILABLE_HOTELROOM = "notAvailableHotelroom";
    public static final String USER = "user";
    public static final String USER_ORDERS = "userOrders";
    public static final String ADMIN = "admin";
    public static final String DAY_AMOUNT = "dayAmount";
    public static final String PAGINATION = "pagination";
    public static final String ERROR = "ERROR";
    public static final String TERM = "term";
    public static final String COMMAND = "command";
    public static final String AJAX_COMMAND = "ajaxCommand";
    public static final String INPUT_KEY = "inputKey";
    public static final String INPUT_KEY_SIGN = "inputKeySign";

    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String PATRONYMIC = "patronymic";
    public static final String EMAIL = "email";
    public static final String LOGIN_EMAIL = "loginEmail";
    public static final String CHECK = "check";
    public static final String AUTO_CHECK = "autoCheck";
    public static final String PASSWORD = "password";
    public static final String TELEPHONE = "telephone";
    public static final String JOB_PLACE = "jobPlace";
    public static final String JOB_PLACE_DB = "job_place";
    public static final String JOB = "job";
    public static final String HOTELROOM = "hotelroom";
    public static final String ORDER = "order";
    public static final String PAGE_NAME = "pageName";
    public static final String LANGUAGE = "language";
    public static final String APPLICATION_LANGUAGE = "applicationLanguage";
    public static final String RU_LOWER = "ru";
    public static final String RU_UPPER = "RU";
    public static final String EN_LOWER = "en";
    public static final String EN_UPPER = "EN";
    public static final String LOCALE_RU = "localeRU";
    public static final String LOCALE_EN = "localeEN";
    public static final String LOCALE = "locale";
    public static final String CHANGE_PERSONAL_DATA = "changeData";
    public static final String SECURE_KEY = "secureKey";
    public static final String MAIL_FROM = "mail.from";
    public static final String MAIL_SMTP_PASSWORD = "mail.smtp.password";

    public static final String INSERT_USER_JOB_PLACE = "insertUserJobPlace";
    public static final String INSERT_DISCOUNT_FOR_USER_JOB_PLACE = "insertDiscountValueForJobPlace";
    public static final String INSERT_NUMBER = "insertNumber";
    public static final String INSERT_HOTEL_ID = "insertHotelId";
    public static final String INSERT_PRICE = "insertPrice";
    public static final String INSERT_CAPACITY = "insertCapacity";
    public static final String INSERT_COMFORT = "insertComfort";
    public static final String INSERT_IMG_URL = "insertImgURL";
    public static final String INSERT_DESCRIPTION = "insertDescription";
    public static final String INSERT_DESCRIPTION_EN = "insertDescriptionEn";
    public static final String INSERT_HOTEL_NAME = "insertHotelName";
    public static final String INSERT_CITY = "insertCity";
    public static final String INSERT_COUNTRY = "insertCountry";

    public static final String CREATE_DISCOUNT_ADMIN = "createDiscountAdmin";
    public static final String CREATE_HOTELROOM_ADMIN = "createHotelroomAdmin";
    public static final String CREATE_HOTEL_ADMIN = "createHotelAdmin";

    public static final String DELETE_ID = "deleteId";
    public static final String DELETE_USER_ADMIN = "deleteUserAdmin";
    public static final String DELETE_DISCOUNT_ADMIN = "deleteDiscountAdmin";
    public static final String DELETE_HOTELROOM_ADMIN = "deleteHotelroomAdmin";
    public static final String DELETE_ORDER_ADMIN = "deleteOrderAdmin";
    public static final String DELETE_HOTEL_ADMIN = "deleteHotelAdmin";

    public static final String FIND_ALL_USER_ADMIN = "findAllUserAdmin";
    public static final String FIND_ALL_DISCOUNT_ADMIN = "findAllDiscountAdmin";
    public static final String FIND_ALL_ORDER_ADMIN = "findAllOrderAdmin";
    public static final String FIND_ALL_HOTEL_ADMIN = "findAllHotelAdmin";
    public static final String FIND_ALL_HOTELROOM_ADMIN = "findAllHotelroomAdmin";

    public static final String FIND_ID = "findId";
    public static final String FIND_USER_ADMIN = "findUserAdmin";
    public static final String FIND_HOTEL_ADMIN = "findHotelAdmin";
    public static final String FIND_DISCOUNT_ADMIN = "findDiscountAdmin";
    public static final String FIND_ORDER_ADMIN = "findOrderAdmin";
    public static final String FIND_HOTELROOM_ADMIN = "findHotelroomAdmin";

    public static final String AUTH = "auth";
    public static final String AUTHORISATION = "authorisation";
    public static final String GO_NEXT = "goNext";
    public static final String OK = "ok";
    public static final String EMPTY = "empty";
    public static final String BAD_REQUEST = "badHotelroomsRequest";
    public static final String PASSWORD_NEW = "passwordNew";
    public static final String PASSWORD_CHANGES = "passwordChanges";
    public static final String HOTELROOM_POSITION = "hotelroomPosition";
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static final String FAVICON = "favicon";

    public static final String INDEX_PAGE = "index";
    public static final String PERSONAL_USER_PAGE = "personalUserPage";
    public static final String SHOW_HOTELROOM_PAGE = "showHotelroomPage";
    public static final String CONFIRM_ORDER_PAGE = "confirmOrderPage";
    public static final String SHOW_REQUEST_HOTELS_PAGE = "showRequestHotels";
    public static final String HOTELS_PACKAGE = "hotels/";
    public static final String HOTELROOM_PACKAGE = "hotelroom/";
    public static final String ORDER_PACKAGE = "order/";
    public static final String USER_PACKAGE = "user/";
    public static final String JSP = ".jsp";
    public static final String HTML = ".html";

    public static final String USERS = "users";
    public static final String DISCOUNTS = "discounts";
    public static final String ORDERS = "orders";
    public static final String HOTELS = "hotels";
    public static final String HOTELROOM_LIST = "holetroomList";
    public static final String HOTELROOMS = "hotelrooms";

    private ControllerConstant(){}
}
