package constant;

public final class DAOConstant {

    public static final String USER_JOB_PLACE= "user_job_place";
    public static final String JOB_PLACE= "job_place";
    public static final String DISCOUNT_VALUE_FOR_JOB_PLACE= "discount_value_for_job_place";
    public static final String COUNT_ID= "count_id";
    public static final String HOTELROOM_ID= "hotelroom_id";
    public static final String HOTEL_ID= "hotel_id";
    public static final String TOTAL_PRICE= "total_price";
    public static final String ARRIVAL_DATE= "arrival_data";
    public static final String DEPARTURE_DATE= "departure_data";
    public static final String IMAGE_URL= "image_url";

    public static final String DAYS= "days";
    public static final String USER= "user";
    public static final String NUMBER= "number";
    public static final String HOTEL= "hotel";
    public static final String HOTELROOM= "hotelroom";
    public static final String DESCRIPTION= "description";
    public static final String DESCRIPTION_EN= "description_en";
    public static final String COMFORT = "comfort";
    public static final String CAPACITY = "capacity";
    public static final String PRICE = "price";
    public static final String EMAIL = "email";
    public static final String CITY = "city";
    public static final String OWNER = "owner";
    public static final String COUNTRY = "country";
    public static final String DISCOUNT = "discount";
    public static final String ORDER = "order";
    public static final String URL = "url";
    public static final String ROLE = "role";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String PATRONYMIC = "patronymic";
    public static final String PASSWORD = "password";
    public static final String TELEPHONE = "telephone";
    public static final String ID = "id";


    private DAOConstant(){}
}
