package service;

import dao.entity.Entity;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    Optional<Order> createOrder(Order order) throws ServiceException;

    void deleteOrder(Order order) throws ServiceException;

    int countTotalPrice(Order order);

    void countDiscount(Order order) throws ServiceException;

    List<Entity> findAll();

    Entity findById(int id) throws ServiceException;

    boolean delete(Entity entity);

    boolean create(Entity entity);

    String getSecretKeyForDuplicateEmail(User user);

    boolean isSuchEmailExist(User user);
}
