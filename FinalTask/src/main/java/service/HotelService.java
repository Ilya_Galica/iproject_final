package service;

import dao.entity.Entity;
import service.exception.ServiceException;

import java.util.List;

public interface HotelService {

    List<Entity> findAll();

    Entity findById(int id) throws ServiceException;

    boolean delete(Entity entity);

    boolean create(Entity entity);
}
