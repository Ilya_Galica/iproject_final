package service.impl;

import dao.entity.Entity;
import dao.entity.UserRole;
import dao.entity.exception.DAOException;
import dao.entity.exception.DuplicateEmailException;
import dao.entity.exception.DuplicateTelephoneException;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import dao.impl.UserDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;
import service.exception.ServiceException;
import service.util.CryptoUtil;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static constant.ControllerConstant.*;
import static service.util.ServiceUtil.*;

public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger();
    private static final UserServiceImpl instance = new UserServiceImpl();
    private UserDAOImpl userDAO = UserDAOImpl.getInstance();
    private CryptoUtil cryptoUtil = CryptoUtil.getInstance();

    public void setUserDAO(UserDAOImpl userDAO, CryptoUtil cryptoUtil) {
        this.cryptoUtil = cryptoUtil;
        this.userDAO = userDAO;
    }


    private UserServiceImpl() {

    }

    public static UserServiceImpl getInstance() {
        return instance;
    }

    @Override
    public User signIn(String email, String password) throws ServiceException {
        try {
            validateSignIn(email, password);
            password = cryptoUtil.encrypt(password);
            return userDAO.signIn(email, password).orElse(new User());
        } catch (SuchUserNotFoundException e) {
            throw new ServiceException("exception.user.not.found.check");
        } catch (NoSuchAlgorithmException e) {
            throw new ServiceException("exception.invalid.password#|#" + e.getMessage());
        }
    }

    @Override
    public boolean signUp(User user) throws ServiceException {
        try {
            validateSignUp(user);
            user.setPassword(cryptoUtil.encrypt(user.getPassword()));
            user.setRole(UserRole.USER);
            return userDAO.signUp(user);
        } catch (NoSuchAlgorithmException e) {
            throw new ServiceException("Problems whit password of this user " + user.getEmail());
        }
    }

    @Override
    public User changePersonalInfo(String newParam, String type, User user) throws ServiceException {
        Optional<User> returnedUser;
        if (type.equals(JOB)) {
            type = JOB_PLACE_DB;
        }
        validateChangePersonalData(user);
        returnedUser = userDAO.changePersonalInfo(newParam.replace(" ", "+"), type, user);
        return returnedUser.orElseThrow(() -> new ServiceException("exception.empty.user"));
    }

    @Override
    public boolean isChangePersonalInfo(String oldParam, String newParam, User user) throws ServiceException {
        try {
            oldParam = cryptoUtil.encrypt(oldParam);
            if (user.getPassword().equalsIgnoreCase(oldParam)) {
                validatePassword(newParam);
                newParam = cryptoUtil.encrypt(newParam);
                user.setPassword(newParam);
                return userDAO.isChangePersonalInfo(newParam, user);
            } else {
                throw new ServiceException("exception.invalid.old.password");
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error(e);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return false;
    }

    @Override
    public List<Order> showUserOrders(String email) {
        return userDAO.showUserOrders(email);
    }

    @Override
    public List<Entity> findAll() {
        return userDAO.findAll();
    }

    public List<String> findAllRelevantId(String term) {
        List<Entity> entities = findAll();
        return entities.stream()
                .map(entity -> String.valueOf(entity.getId()))
                .filter(id -> id.contains(term))
                .collect(Collectors.toList());
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = userDAO.findById(id);
        return entity.orElseThrow(() -> new ServiceException("No such user"));
    }

    @Override
    public boolean delete(Entity entity) {
        return userDAO.delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return userDAO.create(entity);
    }


}
