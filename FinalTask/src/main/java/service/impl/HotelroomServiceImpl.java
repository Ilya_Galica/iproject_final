package service.impl;

import dao.entity.Entity;
import dao.entity.HotelSearchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotelroom;
import dao.impl.HotelroomDAOImpl;
import service.HotelroomService;
import service.exception.ServiceException;
import service.util.ServiceUtil;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static service.util.ServiceUtil.*;

public class HotelroomServiceImpl implements HotelroomService {

    private ServiceUtil serviceUtil = ServiceUtil.getInstance();
    private static final HotelroomServiceImpl instance = new HotelroomServiceImpl();
    private HotelroomDAOImpl hotelroomDAO = HotelroomDAOImpl.getInstance();

    public void setHotelroomDAO(HotelroomDAOImpl hotelroomDAO) {
        this.hotelroomDAO = hotelroomDAO;
    }

    private HotelroomServiceImpl() {

    }

    public static HotelroomServiceImpl getInstance() {
        return instance;
    }


    @Override
    public List<Hotelroom> showAll(HotelSearchDataContainer container, Pagination pagination) {
        String anotherLanguageCity
                = serviceUtil.getAnotherLanguageCity(container.getCity());
        if (anotherLanguageCity != null) {
            container.setCity(anotherLanguageCity);
        }
        List<Hotelroom> hotelrooms = hotelroomDAO.showAll(container, pagination);
        container.setArrivalData(reverseDateFromSQL(container.getArrivalData()));
        container.setDepartureData(reverseDateFromSQL(container.getDepartureData()));
        return hotelrooms;
    }

    @Override
    public int getAmountOfValidHotelrooms(HotelSearchDataContainer container) {
        container
                .setArrivalData(reverseDateForSQL(container.getArrivalData()));
        container
                .setDepartureData(reverseDateForSQL(container.getDepartureData()));
        return hotelroomDAO.getAmountOfValidHotelrooms(container);
    }

    @Override
    public int getDayAmount(String arrivalData, String departureData) {
        arrivalData = reverseDateForSQL(arrivalData);
        departureData = reverseDateForSQL(departureData);
        int dayAmount = hotelroomDAO.getDayAmount(arrivalData, departureData);
        return (dayAmount == 0) ? 1 : dayAmount;
    }

    @Override
    public boolean isFreeHotelroom(HotelSearchDataContainer container, Hotelroom hotelroom) {
        container.setArrivalData(reverseDateForSQL(container.getArrivalData()));
        container.setDepartureData(reverseDateForSQL(container.getDepartureData()));
        List<Hotelroom> hotelrooms = hotelroomDAO.checkRoomRelevance(container);
        boolean flag = hotelrooms.stream().anyMatch(localHotelroom -> localHotelroom.equals(hotelroom));
        container.setArrivalData(reverseDateFromSQL(container.getArrivalData()));
        container.setDepartureData(reverseDateFromSQL(container.getDepartureData()));
        return flag;
    }

    public List<String> findAllRelevantId(String term) {
        List<Entity> entities = findAll();
        return entities.stream()
                .map(entity -> String.valueOf(entity.getId()))
                .filter(id -> id.contains(term))
                .collect(Collectors.toList());
    }

    @Override
    public List<Entity> findAll() {
        return hotelroomDAO.findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = hotelroomDAO.findById(id);
        return entity.orElseThrow(() -> new ServiceException("No such hotelroom"));
    }

    @Override
    public boolean delete(Entity entity) {
        return hotelroomDAO.delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return hotelroomDAO.create(entity);
    }


}
