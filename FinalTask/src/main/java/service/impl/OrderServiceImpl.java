package service.impl;

import dao.DiscountDAO;
import dao.HotelroomDAO;
import dao.entity.Entity;
import dao.entity.exception.OrderNotCreateException;
import dao.entity.impl.Discount;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import dao.impl.AbstractDAO;
import dao.impl.DiscountDAOImpl;
import dao.impl.HotelroomDAOImpl;
import dao.impl.OrderDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.OrderService;
import service.util.CryptoUtil;
import service.exception.ServiceException;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import static service.util.ServiceUtil.*;

public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LogManager.getLogger();
    private static final OrderServiceImpl instance = new OrderServiceImpl();
    private OrderDAOImpl orderDAO = OrderDAOImpl.getInstance();

    public void setOrderDAO(OrderDAOImpl orderDAO) {
        this.orderDAO = orderDAO;
    }


    private OrderServiceImpl() {

    }

    public static OrderServiceImpl getInstance() {
        return instance;
    }

    @Override
    public Optional<Order> createOrder(Order order) throws ServiceException {
        try {
            order.setArrivalData(reverseDateForSQL(order.getArrivalData()));
            order.setDepartureData(reverseDateForSQL(order.getDepartureData()));
            return orderDAO.createOrder(order);
        } catch (OrderNotCreateException e) {
            throw new ServiceException("exception.order.not.change#|#" + order.getId());
        }
    }

    public List<String> findAllRelevantId(String term) {
        List<Entity> entities = findAll();
        return entities.stream()
                .map(entity -> String.valueOf(entity.getId()))
                .filter(id -> id.contains(term))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteOrder(Order order) throws ServiceException {
        AbstractDAO dao = OrderDAOImpl.getInstance();
        if (!dao.delete(order)) {
            throw new ServiceException("Order hadn't been deleted");
        }
    }

    @Override
    public int countTotalPrice(Order order) {
        HotelroomDAO hotelroomDAO = HotelroomDAOImpl.getInstance();
        order.setArrivalData(reverseDateForSQL(order.getArrivalData()));
        order.setDepartureData(reverseDateForSQL(order.getDepartureData()));
        int dayAmount
                = hotelroomDAO
                .getDayAmount(order.getArrivalData(),
                        order.getDepartureData());
        order.setArrivalData(reverseDateFromSQL(order.getArrivalData()));
        order.setDepartureData(reverseDateFromSQL(order.getDepartureData()));
        if (order.getDiscount().getDiscountValueForJobPlace() != 0) {
            int totalPrice = ((dayAmount != 0) ? dayAmount : 1) * order.getHotelroom().getPrice();
            return totalPrice - (totalPrice * order.getDiscount().getDiscountValueForJobPlace() / 100);
        } else {
            return ((dayAmount != 0) ? dayAmount : 1) * order.getHotelroom().getPrice();
        }
    }

    @Override
    public void countDiscount(Order order) throws ServiceException {
        validateUserInput(order.getUser());
        String jobPlace = order.getUser().getJobPlace();
        DiscountDAO discountDAO = DiscountDAOImpl.getInstance();
        List<Discount> discountList = discountDAO.getDiscountList();
        discountList.stream()
                .filter(discount -> jobPlace.toUpperCase()
                        .contains(discount.getUserJobPlace()
                                .toUpperCase()))
                .forEach(discount -> order.getUser()
                        .setJobPlace(discount.getUserJobPlace()));
        Optional<Discount> optionalDiscount = discountList.stream()
                .filter(localeDiscount -> localeDiscount.getUserJobPlace()
                        .equals(order.getUser().getJobPlace())).findAny();
        if (optionalDiscount.isPresent()) {
            order.setDiscount(optionalDiscount.get());
        } else {
            Discount discount = new Discount();
            discount.setDiscountValueForJobPlace(0);
            order.setDiscount(discount);
        }
    }

    @Override
    public List<Entity> findAll() {
        return orderDAO.findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = orderDAO.findById(id);
        return entity.orElseThrow(() -> new ServiceException("No such order"));
    }

    @Override
    public boolean delete(Entity entity) {
        return orderDAO.delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return orderDAO.create(entity);
    }

    @Override
    public String getSecretKeyForDuplicateEmail(User user) {
        try {
            Random random = SecureRandom.getInstanceStrong();
            char forRandom = user.getEmail()
                    .charAt(random.nextInt(user.getEmail().length() - 1));
            char symbol = (char) (random.nextInt(30) + forRandom);
            CryptoUtil cryptoUtil = CryptoUtil.getInstance();
            return cryptoUtil.encrypt(String.valueOf(symbol)).substring(0, 7);
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getMessage(), e);
        }
        return "";
    }

    @Override
    public boolean isSuchEmailExist(User user) {
        return orderDAO.isSuchEmailExist(user);
    }


}
