package service.impl;

import dao.entity.Entity;
import dao.impl.HotelDAOImpl;
import service.HotelService;
import service.exception.ServiceException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class HotelServiceImpl implements HotelService {

    private static final HotelServiceImpl instance = new HotelServiceImpl();

    private HotelDAOImpl hotelDAO = HotelDAOImpl.getInstance();

    public void setHotelDAO(HotelDAOImpl hotelDAO) {
        this.hotelDAO = hotelDAO;
    }

    private HotelServiceImpl() {

    }

    public static HotelServiceImpl getInstance() {
        return instance;
    }

    @Override
    public List<Entity> findAll() {
        return hotelDAO.findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = hotelDAO.findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new ServiceException("No such hotel");
        }
    }

    public List<String> findAllRelevantId(String term) {
        List<Entity> entities = findAll();
        return entities.stream()
                .map(entity -> String.valueOf(entity.getId()))
                .filter(id -> id.contains(term))
                .collect(Collectors.toList());
    }

    @Override
    public boolean delete(Entity entity) {
        return hotelDAO.delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return hotelDAO.create(entity);
    }
}
