package service.impl;

import dao.entity.Entity;
import dao.impl.AbstractDAO;
import dao.impl.DiscountDAOImpl;
import service.DiscountService;
import service.exception.ServiceException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DiscountServiceImpl implements DiscountService {

    private static final DiscountServiceImpl instance = new DiscountServiceImpl();
    private AbstractDAO discountDAO = DiscountDAOImpl.getInstance();

    public void setDiscountDAO(AbstractDAO discountDAO) {
        this.discountDAO = discountDAO;
    }

    private DiscountServiceImpl() {

    }

    public static DiscountServiceImpl getInstance() {
        return instance;
    }

    @Override
    public List<Entity> findAll() {
        return discountDAO.findAll();
    }

    @Override
    public Entity findById(int id) throws ServiceException {
        Optional<Entity> entity = discountDAO.findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new ServiceException("No such discount");
        }
    }

    public List<String> findAllRelevantId(String term) {
        List<Entity> entities = findAll();
        return entities.stream()
                .map(entity -> String.valueOf(entity.getId()))
                .filter(id -> id.contains(term))
                .collect(Collectors.toList());
    }

    @Override
    public boolean delete(Entity entity) {
        return discountDAO.delete(entity);
    }

    @Override
    public boolean create(Entity entity) {
        return discountDAO.create(entity);
    }

}
