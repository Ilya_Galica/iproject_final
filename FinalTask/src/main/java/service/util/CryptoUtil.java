package service.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptoUtil {

    private static final CryptoUtil instance = new CryptoUtil();
    private static final String CIPHER_METHOD = "MD5";
    private static final String OUTPUT_FORMAT = "%02X";

    public static CryptoUtil getInstance() {
        return instance;
    }

    private CryptoUtil() {

    }

    public String encrypt(String expectedText)
            throws NoSuchAlgorithmException {
        MessageDigest md5Cipher = MessageDigest.getInstance(CIPHER_METHOD);
        byte[] out = md5Cipher.digest(expectedText.getBytes());
        StringBuilder builder = new StringBuilder();
        for (byte localByte : out) {
            builder.append(String.format(OUTPUT_FORMAT, localByte));
        }
        return builder.toString();
    }

}