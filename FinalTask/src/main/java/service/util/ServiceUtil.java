package service.util;

import dao.entity.impl.User;
import service.exception.ServiceException;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ServiceUtil {

    private static final Map<String, String> cityMap = new HashMap<>();
    private static final ServiceUtil instance = new ServiceUtil();
    private static final String REVERSE_FOR_SQL = "((\\d)*)/((\\d)*)/((\\d)*)";
    private static final String REVERSE_FROM_SQL = "((\\d)*)-((\\d)*)-((\\d)*)";
    private static final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(\\w){6,30}";
    private static final String TELEPHONE_REGEX = "((80)?|(\\+375)?)(\\(\\d{2}\\)|\\d{2})((\\d{3}-\\d{2}-\\d{2})|\\d{7})";
    private static final String SIMPLE_INPUT_REGEX = "[a-zA-Zа-яА-Я]{2,}";

    public static ServiceUtil getInstance() {
        return instance;
    }

    private ServiceUtil() {
        cityMap.put("минск", "minsk");
        cityMap.put("амстердам", "amsterdam");
    }

    public static String reverseDateForSQL(String date) {
        return date.replaceAll( REVERSE_FOR_SQL, "$5-$3-$1");
    }

    public static String reverseDateFromSQL(String date) {
        return date.replaceAll(REVERSE_FROM_SQL, "$5/$3/$1");
    }

    public String getAnotherLanguageCity(String city) {
        return cityMap.get(city.toLowerCase());
    }

    private static boolean validateParameter(String param, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(param);
        return matcher.matches();
    }

    public static boolean validateSignIn(String email, String password) throws ServiceException {
        if (validateParameter(password, PASSWORD_REGEX)
                || password.equals("admin")) {
            return true;
        } else {
            throw new ServiceException("exception.invalid.password.regex#|#" + password);
        }
    }

    public static boolean validateSignUp(User user) throws ServiceException {
        commonValidation(user);
        if (!validateParameter(user.getPassword(), PASSWORD_REGEX)) {
            throw new ServiceException("exception.invalid.password.regex#|# " + user.getPassword());
        }
        user.setJobPlace(user.getJobPlace().replaceAll("\\'", "")
                .replaceAll("\\\"", "")
                .replaceAll("\\)", ""));
        return true;
    }

    public static boolean validateChangePersonalData(User user) throws ServiceException {
        commonValidation(user);
        return true;
    }

    private static void commonValidation(User user) throws ServiceException {
        if (!validateParameter(user.getTelephone().replaceAll("\\s", "+"),
                TELEPHONE_REGEX)) {
            throw new ServiceException("exception.invalid.telephone.regex#|#" + user.getTelephone());
        }
        if (!validateParameter(user.getName(), SIMPLE_INPUT_REGEX)) {
            throw new ServiceException("exception.invalid.name.regex#|#" + user.getName());
        }
        if (!validateParameter(user.getSurname(), SIMPLE_INPUT_REGEX)) {
            throw new ServiceException("exception.invalid.surname.regex#|#" + user.getSurname());
        }
        if (!validateParameter(user.getPatronymic(), SIMPLE_INPUT_REGEX)) {
            throw new ServiceException("exception.invalid.patronymic.regex#|#" + user.getPatronymic());
        }
        user.setJobPlace(user.getJobPlace().replaceAll("\\'", "")
                .replaceAll("\\\"", "")
                .replaceAll("\\)", ""));
    }

    public static boolean validatePassword(String password) throws ServiceException {
        if (!validateParameter(password, PASSWORD_REGEX)) {
            throw new ServiceException("exception.invalid.password.regex#|# " + password);
        }
        return true;
    }

    public static boolean validateUserInput(User user) throws ServiceException {
        if (!validateParameter(user.getTelephone().replaceAll("\\s", "+"),
                TELEPHONE_REGEX)) {
            throw new ServiceException("exception.invalid.telephone.regex#|#" + user.getTelephone());
        }
        if (!validateParameter(user.getName(), SIMPLE_INPUT_REGEX)) {
            throw new ServiceException("exception.invalid.name.regex#|#" + user.getName());
        }
        if (!validateParameter(user.getSurname(), SIMPLE_INPUT_REGEX)) {
            throw new ServiceException("exception.invalid.surname.regex#|#" + user.getSurname());
        }
        if (!validateParameter(user.getPatronymic(), SIMPLE_INPUT_REGEX)) {
            throw new ServiceException("exception.invalid.patronymic.regex#|#" + user.getPatronymic());
        }
        user.setJobPlace(user.getJobPlace().replaceAll("\\'", "")
                .replaceAll("\\\"", "")
                .replaceAll("\\)", ""));
        return true;
    }
}
