package service;

import dao.entity.Entity;
import dao.entity.exception.DuplicateEmailException;
import dao.entity.exception.DuplicateTelephoneException;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import service.exception.ServiceException;

import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

public interface UserService {

    User signIn(String email, String password) throws SuchUserNotFoundException, ServiceException;

    boolean signUp(User user) throws ServiceException;

    User changePersonalInfo(String newParam, String type, User user) throws ServiceException;

    boolean isChangePersonalInfo(String oldParam, String newParam, User user) throws ServiceException;

    List<Order> showUserOrders(String email);

    List<Entity> findAll();

    Entity findById(int id) throws ServiceException;

    boolean delete(Entity entity);

    boolean create(Entity entity);
}
