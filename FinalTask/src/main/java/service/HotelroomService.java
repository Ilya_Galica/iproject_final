package service;

import dao.entity.Entity;
import dao.entity.HotelSearchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotelroom;
import service.exception.ServiceException;

import java.util.List;

public interface HotelroomService {

    List<Hotelroom> showAll(HotelSearchDataContainer container, Pagination pagination);

    int getAmountOfValidHotelrooms(HotelSearchDataContainer container);

    int getDayAmount(String arrivalData, String departureData);

    boolean isFreeHotelroom(HotelSearchDataContainer container, Hotelroom hotelroom);

    List<Entity> findAll();

    Entity findById(int id) throws ServiceException;

    boolean delete(Entity entity);

    boolean create(Entity entity);
}
