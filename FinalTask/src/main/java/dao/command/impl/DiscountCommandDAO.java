package dao.command.impl;

import dao.command.CommandDAO;
import dao.entity.Entity;
import dao.entity.impl.Discount;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static dao.util.DAOUtil.simpleDelete;
import static constant.DAOConstant.*;

public class DiscountCommandDAO implements CommandDAO {
    private String className = Discount.class.getSimpleName();

    @Override
    public List<Entity> findAll(ResultSet resultSet) throws SQLException {
        List<Entity> entityList = new ArrayList<>();
        while (resultSet.next()) {
            Discount discount = new Discount();
            discount.setId(resultSet.getInt(ID));
            discount.setUserJobPlace(resultSet.getString(USER_JOB_PLACE));
            discount.setDiscountValueForJobPlace(resultSet.getInt(DISCOUNT_VALUE_FOR_JOB_PLACE));
            entityList.add(discount);
        }
        return entityList;
    }

    @Override
    public Optional<Entity> findById(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Discount discount = new Discount();
            discount.setId(resultSet.getInt(ID));
            discount.setUserJobPlace(resultSet.getString(USER_JOB_PLACE));
            discount.setDiscountValueForJobPlace(resultSet.getInt(DISCOUNT_VALUE_FOR_JOB_PLACE));
            return Optional.of(discount);
        }
        return Optional.empty();
    }

    @Override
    public boolean delete(Entity entity, Connection connection) throws SQLException {
        return simpleDelete(entity, connection, className);
    }

    @Override
    public boolean create(Entity entity, Connection connection) throws SQLException {
        Discount discount = (Discount) entity;
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO hotel_booking.discount " +
                "(user_job_place, discount_value_for_job_place) VALUES (?, ?);")) {
            statement.setString(1, discount.getUserJobPlace());
            statement.setInt(2, discount.getDiscountValueForJobPlace());
            return statement.executeUpdate() == 1;
        }
    }
}
