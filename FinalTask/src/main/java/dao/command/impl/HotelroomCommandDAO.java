package dao.command.impl;

import dao.ConnectionPool;
import dao.command.CommandDAO;
import dao.entity.Entity;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import dao.impl.HotelDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static constant.DAOConstant.*;

public class HotelroomCommandDAO implements CommandDAO {

    private HotelDAOImpl hotelDao = HotelDAOImpl.getInstance();
    private ConnectionPool pool = ConnectionPool.getInstance();
    private String className = Hotelroom.class.getSimpleName();
    private static final Logger logger = LogManager.getLogger();

    @Override
    public List<Entity> findAll(ResultSet resultSet) throws SQLException {
        List<Entity> entityList = new ArrayList<>();
        while (resultSet.next()) {
            Hotelroom hotelroom = new Hotelroom();
            hotelroom.setId(resultSet.getInt(ID));
            hotelroom.setNumber(resultSet.getInt(NUMBER));
            Hotel hotel = null;
            if (hotelDao.findById(resultSet.getInt(HOTEL)).isPresent()) {
                hotel = (Hotel) hotelDao.findById(resultSet.getInt(HOTEL)).get();
            }
            hotelroom.setHotel(hotel);
            hotelroom.setPrice(resultSet.getInt(PRICE));
            hotelroom.setPeopleCapacity(resultSet.getInt(CAPACITY));
            hotelroom.setComfort(resultSet.getString(COMFORT));
            hotelroom.setImageURL(resultSet.getString(IMAGE_URL));
            hotelroom.setDescription(resultSet.getString(DESCRIPTION));
            hotelroom.setDescriptionEn(resultSet.getString(DESCRIPTION_EN));
            entityList.add(hotelroom);
        }
        return entityList;
    }

    @Override
    public Optional<Entity> findById(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Hotelroom hotelroom = new Hotelroom();
            hotelroom.setId(resultSet.getInt(ID));
            hotelroom.setNumber(resultSet.getInt(NUMBER));
            Hotel hotel = (Hotel) hotelDao.findById(resultSet.getInt(HOTEL)).get();
            hotelroom.setHotel(hotel);
            hotelroom.setPrice(resultSet.getInt(PRICE));
            hotelroom.setPeopleCapacity(resultSet.getInt(CAPACITY));
            hotelroom.setComfort(resultSet.getString(COMFORT));
            hotelroom.setImageURL(resultSet.getString(IMAGE_URL));
            hotelroom.setDescription(resultSet.getString(DESCRIPTION));
            hotelroom.setDescriptionEn(resultSet.getString(DESCRIPTION_EN));
            return Optional.of(hotelroom);
        }
        return Optional.empty();
    }

    @Override
    public boolean delete(Entity entity, Connection connection) {
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement("DELETE FROM hotel_booking.`order` WHERE owner = ?;");
            statement.setInt(1, entity.getId());
            int orderDelete = statement.executeUpdate();
            statement = connection.prepareStatement("DELETE FROM hotel_booking.hotelroom WHERE id = ?;");
            statement.setInt(1, entity.getId());
            int hotelroomDelete = statement.executeUpdate();
            if (hotelroomDelete == 1) {
                connection.commit();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e);
            pool.connectionRollback(connection);
        } finally {
            pool.closePreparedStatement(statement);
        }
        return false;
    }

    @Override
    public boolean create(Entity entity, Connection connection) {
        Hotelroom hotelroom = (Hotelroom) entity;
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO hotel_booking.`hotelroom` " +
                "(number, hotel, price, capacity, comfort, image_url, description, description_en) " +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?);")) {
            statement.setInt(1, hotelroom.getNumber());
            statement.setInt(2, hotelroom.getHotel().getId());
            statement.setInt(3, hotelroom.getPrice());
            statement.setInt(4, hotelroom.getPeopleCapacity());
            statement.setString(5, hotelroom.getComfort());
            statement.setString(6, hotelroom.getImageURL());
            statement.setString(7, hotelroom.getDescription());
            statement.setString(8, hotelroom.getDescriptionEn());
            return statement.executeUpdate() == 1;
        }catch (SQLException e){
            logger.error(e);
        }
        return false;
    }
}
