package dao.command.impl;

import dao.ConnectionPool;
import dao.command.CommandDAO;
import dao.entity.Entity;
import dao.entity.UserRole;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static constant.DAOConstant.*;

public class UserCommandDAO implements CommandDAO {
    private ConnectionPool pool = ConnectionPool.getInstance();
    private String className = User.class.getSimpleName().toLowerCase();
    private static final Logger logger = LogManager.getLogger();

    @Override
    public List<Entity> findAll(ResultSet resultSet) throws SQLException {
        List<Entity> entityList = new ArrayList<>();
        while (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getInt(ID));
            user.setEmail(resultSet.getString(EMAIL));
            user.setName(resultSet.getString(NAME));
            user.setSurname(resultSet.getString(SURNAME));
            user.setPatronymic(resultSet.getString(PATRONYMIC));
            user.setTelephone(resultSet.getString(TELEPHONE));
            user.setPassword(resultSet.getString(PASSWORD));
            user.setJobPlace(resultSet.getString(JOB_PLACE));
            user.setRole((resultSet.getString(ROLE).equals(USER))
                    ? UserRole.USER
                    : UserRole.ADMIN);
            entityList.add(user);
        }
        return entityList;
    }

    @Override
    public Optional<Entity> findById(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getInt(ID));
            user.setEmail(resultSet.getString(EMAIL));
            user.setName(resultSet.getString(NAME));
            user.setSurname(resultSet.getString(SURNAME));
            user.setPatronymic(resultSet.getString(PATRONYMIC));
            user.setTelephone(resultSet.getString(TELEPHONE));
            user.setPassword(resultSet.getString(PASSWORD));
            user.setJobPlace(resultSet.getString(JOB_PLACE));
            user.setRole((resultSet.getString(ROLE).equals(USER))
                    ? UserRole.USER
                    : UserRole.ADMIN);
            return Optional.of(user);
        }

        return Optional.empty();
    }

    @Override
    public boolean delete(Entity entity, Connection connection) {
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement("DELETE FROM `order` WHERE owner = ?;");
            statement.setInt(1, entity.getId());
            statement.executeUpdate();
            statement = connection.prepareStatement("DELETE FROM user WHERE id = ?;");
            statement.setInt(1, entity.getId());
            int userDelete = statement.executeUpdate();
            if (userDelete == 1) {
                connection.commit();
                return true;
            }
        } catch (SQLException e) {
            logger.error(e);
            pool.connectionRollback(connection);
        } finally {
            pool.closePreparedStatement(statement);
        }
        return false;
    }

    @Override
    public boolean create(Entity entity, Connection connection) throws SQLException {
        User user = (User) entity;
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO hotel_booking.`user` " +
                "(id, email, name, surname, patronymic, telephone, password, job_place, role)" +
                " VALUES (?, ? ,?, ?, ?, ?, ?, ?, ?);")) {
            statement.setInt(1, user.getId());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getName());
            statement.setString(4, user.getSurname());
            statement.setString(5, user.getPatronymic());
            statement.setString(6, user.getTelephone());
            statement.setString(7, user.getPassword());
            statement.setString(8, user.getJobPlace());
            statement.setString(9, String.valueOf(user.getRole()).toLowerCase());
            return statement.executeUpdate() == 1;
        }
    }
}
