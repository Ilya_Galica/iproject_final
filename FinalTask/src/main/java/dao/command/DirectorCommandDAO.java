package dao.command;

import dao.command.impl.*;

import java.util.HashMap;
import java.util.Map;

import static constant.DAOConstant.*;

public class DirectorCommandDAO {
    private Map<String, CommandDAO> map = new HashMap<>();
    private static final DirectorCommandDAO instance = new DirectorCommandDAO();

    public static DirectorCommandDAO getInstance() {
        return instance;
    }

    private DirectorCommandDAO() {
        map.put(HOTELROOM, new HotelroomCommandDAO());
        map.put(HOTEL, new HotelCommandDAO());
        map.put(ORDER, new OrderCommandDAO());
        map.put(DISCOUNT, new DiscountCommandDAO());
        map.put(USER, new UserCommandDAO());
    }

    public CommandDAO getCommand(String commandKey) {
        return map.get(commandKey);
    }
}
