package dao;

import dao.entity.impl.Discount;

import java.util.List;

public interface DiscountDAO {
    List<Discount> getDiscountList();
}
