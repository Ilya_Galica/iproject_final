package dao;

import dao.entity.exception.DAOException;
import dao.entity.exception.DuplicateEmailException;
import dao.entity.exception.DuplicateTelephoneException;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.Order;
import dao.entity.impl.User;

import java.util.List;
import java.util.Optional;

public interface UserDAO {
    Optional<User> signIn(String email, String password) throws SuchUserNotFoundException;

    boolean signUp(User user);

    Optional<User> changePersonalInfo(String newParam, String type, User user);

    boolean isChangePersonalInfo(String newParam, User user) throws DAOException;

    List<Order> showUserOrders(String email);
}
