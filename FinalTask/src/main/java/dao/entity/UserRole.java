package dao.entity;

public enum UserRole {
    USER,
    ADMIN
}
