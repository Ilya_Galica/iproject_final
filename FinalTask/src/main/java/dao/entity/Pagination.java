package dao.entity;

import java.io.Serializable;

public class Pagination implements Serializable {
    public static final int HOTEL_PER_PAGE = 5;

    private int currentPage;

    private int paginationAmount;

    private int allPageAmount;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPaginationAmount() {
        return paginationAmount;
    }

    public void setPaginationAmount(int paginationAmount) {
        this.paginationAmount = paginationAmount;
    }

    public int getAllPageAmount() {
        return allPageAmount;
    }

    public void setAllPageAmount(int allPageAmount) {
        this.allPageAmount = allPageAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pagination that = (Pagination) o;

        if (currentPage != that.currentPage) return false;
        if (paginationAmount != that.paginationAmount) return false;
        return allPageAmount == that.allPageAmount;
    }

    @Override
    public int hashCode() {
        int result = currentPage;
        result = 31 * result + paginationAmount;
        result = 31 * result + allPageAmount;
        return result;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "currentPage=" + currentPage +
                ", paginationAmount=" + paginationAmount +
                ", allPageAmount=" + allPageAmount +
                '}';
    }
}
