package dao.entity.impl;

import dao.entity.Entity;

public class Order extends Entity {
    private Discount discount;
    private User user;
    private String arrivalData;
    private String departureData;
    private Hotelroom hotelroom;
    private int totalPrice;

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getArrivalData() {
        return arrivalData;
    }

    public void setArrivalData(String arrivalData) {
        this.arrivalData = arrivalData;
    }

    public String getDepartureData() {
        return departureData;
    }

    public void setDepartureData(String departureData) {
        this.departureData = departureData;
    }

    public Hotelroom getHotelroom() {
        return hotelroom;
    }

    public void setHotelroom(Hotelroom hotelroom) {
        this.hotelroom = hotelroom;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Order order = (Order) o;

        if (totalPrice != order.totalPrice) return false;
        if (discount != null ? !discount.equals(order.discount) : order.discount != null) return false;
        if (user != null ? !user.equals(order.user) : order.user != null) return false;
        if (arrivalData != null ? !arrivalData.equals(order.arrivalData) : order.arrivalData != null) return false;
        if (departureData != null ? !departureData.equals(order.departureData) : order.departureData != null)
            return false;
        return hotelroom != null ? hotelroom.equals(order.hotelroom) : order.hotelroom == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (discount != null ? discount.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (arrivalData != null ? arrivalData.hashCode() : 0);
        result = 31 * result + (departureData != null ? departureData.hashCode() : 0);
        result = 31 * result + (hotelroom != null ? hotelroom.hashCode() : 0);
        result = 31 * result + totalPrice;
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "discount=" + discount +
                ", user=" + user +
                ", arrivalData='" + arrivalData + '\'' +
                ", departureData='" + departureData + '\'' +
                ", hotelroom=" + hotelroom +
                ", totalPrice=" + totalPrice +
                "} " + super.toString();
    }
}
