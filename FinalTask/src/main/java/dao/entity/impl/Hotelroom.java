package dao.entity.impl;

import dao.entity.Entity;

public class Hotelroom extends Entity {
    private int number;
    private Hotel hotel;
    private int price;
    private int peopleCapacity;
    private String comfort;
    private String imageURL;
    private String description;
    private String descriptionEn;

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPeopleCapacity() {
        return peopleCapacity;
    }

    public void setPeopleCapacity(int peopleCapacity) {
        this.peopleCapacity = peopleCapacity;
    }

    public String getComfort() {
        return comfort;
    }

    public void setComfort(String comfort) {
        this.comfort = comfort;
    }


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Hotelroom hotelroom = (Hotelroom) o;

        if (number != hotelroom.number) return false;
        if (price != hotelroom.price) return false;
        if (peopleCapacity != hotelroom.peopleCapacity) return false;
        if (hotel != null ? !hotel.equals(hotelroom.hotel) : hotelroom.hotel != null) return false;
        if (comfort != null ? !comfort.equals(hotelroom.comfort) : hotelroom.comfort != null) return false;
        if (imageURL != null ? !imageURL.equals(hotelroom.imageURL) : hotelroom.imageURL != null) return false;
        if (description != null ? !description.equals(hotelroom.description) : hotelroom.description != null)
            return false;
        return descriptionEn != null ? descriptionEn.equals(hotelroom.descriptionEn) : hotelroom.descriptionEn == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + number;
        result = 31 * result + (hotel != null ? hotel.hashCode() : 0);
        result = 31 * result + price;
        result = 31 * result + peopleCapacity;
        result = 31 * result + (comfort != null ? comfort.hashCode() : 0);
        result = 31 * result + (imageURL != null ? imageURL.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (descriptionEn != null ? descriptionEn.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Hotelroom{" +
                "number=" + number +
                ", hotel=" + hotel +
                ", price=" + price +
                ", peopleCapacity=" + peopleCapacity +
                ", comfort='" + comfort + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", description='" + description + '\'' +
                ", descriptionEn='" + descriptionEn + '\'' +
                "} " + super.toString();
    }
}
