package dao.entity.exception;

public class DuplicateTelephoneException extends Exception {
    public DuplicateTelephoneException() {
    }

    public DuplicateTelephoneException(String s) {
        super(s);
    }

    public DuplicateTelephoneException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DuplicateTelephoneException(Throwable throwable) {
        super(throwable);
    }

    public DuplicateTelephoneException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
