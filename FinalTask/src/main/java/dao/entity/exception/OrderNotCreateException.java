package dao.entity.exception;

public class OrderNotCreateException extends Exception {
    public OrderNotCreateException() {
        super();
    }

    public OrderNotCreateException(String s) {
        super(s);
    }

    public OrderNotCreateException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public OrderNotCreateException(Throwable throwable) {
        super(throwable);
    }

    protected OrderNotCreateException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
