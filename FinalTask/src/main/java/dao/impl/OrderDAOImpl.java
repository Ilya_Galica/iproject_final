package dao.impl;

import dao.ConnectionPool;
import dao.OrderDAO;
import dao.entity.exception.OrderNotCreateException;
import dao.entity.impl.Discount;
import dao.entity.impl.User;
import dao.entity.impl.Hotelroom;
import dao.entity.impl.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static constant.DAOConstant.*;

public class OrderDAOImpl extends AbstractDAO implements OrderDAO {

    private static final OrderDAOImpl instance = new OrderDAOImpl();

    private OrderDAOImpl() {
        setClassName(Order.class.getSimpleName().toLowerCase());
    }

    public static OrderDAOImpl getInstance() {
        return instance;
    }

    private ConnectionPool pool = ConnectionPool.getInstance();


    private static final Logger logger = LogManager.getLogger();

    private static final String SELECT_USER =
            "SELECT id FROM hotel_booking.user WHERE email = ? ;  ";

    private static final String INSERT_USER =
            "INSERT INTO hotel_booking.user (email, name,  surname,  patronymic,  telephone,  password,  job_place) " +
                    "VALUES ( ? ,?, ?, ?, ?, ?, ?);  ";
    private static final String INSERT_EMAIL = "INSERT INTO hotel_booking.order (hotel_booking.order.owner)  " +
            "SELECT id FROM hotel_booking.user WHERE email = ?; ";
    private static final String UPDATE_DISCOUNT_ORDER = "UPDATE hotel_booking.order new_order  " +
            "SET new_order.discount = (SELECT id FROM discount WHERE user_job_place = ?) " +
            "WHERE id = LAST_INSERT_ID(); ";
    private static final String LAST_ORDER_ID = "SELECT LAST_INSERT_ID() AS id;";
    private static final String UPDATE_HOTELROOM_ORDER = "UPDATE hotel_booking.order new_order  " +
            "SET new_order.hotelroom = (SELECT id FROM hotel_booking.hotelroom WHERE hotel = ? AND number = ?) " +
            "WHERE id = LAST_INSERT_ID();";
    private static final String UPDATE_TOTAL_PRICE = "UPDATE hotel_booking.order new_order  " +
            "SET new_order.total_price = ? " +
            "WHERE id = LAST_INSERT_ID(); ";
    private static final String DUPLICATE_CHECK = "SELECT * FROM hotel_booking.`order` WHERE arrival_data=?" +
            " and departure_data=? and hotelroom = ?";
    private static final String UPDATE_DATA_ORDER =
            "UPDATE hotel_booking.order SET arrival_data= ?, departure_data = ? " +
                    "WHERE id = LAST_INSERT_ID(); ";
    private static final String SELECT_ALL = "SELECT * FROM hotel_booking.`order` " +
            "LEFT JOIN hotel_booking.discount d on hotel_booking.`order`.discount = d.id " +
            "JOIN hotel_booking.hotelroom h on hotel_booking.`order`.hotelroom = h.id " +
            "JOIN hotel_booking.user u on hotel_booking.`order`.owner = u.id " +
            "WHERE hotel_booking.`order`.id = ?; ";


    @Override
    public Optional<Order> createOrder(Order order) throws OrderNotCreateException {
        Connection connection = null;
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            checkCurrentUserExistence(connection, order.getUser());
            isInsertUserByEmailIntoOrder(connection, order.getUser());
            setUpdateOrderByDiscount(connection, order.getUser());
            setUpdateOrderByHotelroom(connection, order.getHotelroom());
            checkDuplicateOrder(connection, order);
            int lastInsertedId = getLastOrderId(connection);
            setUpdateOrderByTotalPrice(connection, order);
            setUpdateOrderByData(connection,
                    order.getArrivalData(), order.getDepartureData());
            order = getWholeOrder(connection, order, lastInsertedId);
            connection.commit();
            return Optional.ofNullable(order);
        } catch (SQLException | RuntimeException e) {
            logger.error(e);
            pool.connectionRollback(connection);
            throw new OrderNotCreateException();
        } finally {
            pool.putBackConnection(connection);
        }
    }

    @Override
    public boolean isSuchEmailExist(User user) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement(SELECT_USER);
            statement.setString(1, user.getEmail());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
        }
        return false;
    }


    private void checkCurrentUserExistence(Connection connection, User user) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(SELECT_USER);
            statement.setString(1, user.getEmail());
            resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                isInsertIntoUser(connection, user);
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }
    }

    private boolean isInsertIntoUser(Connection connection, User user) {
        try (PreparedStatement statement = connection.prepareStatement(INSERT_USER)) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getName());
            statement.setString(3, user.getSurname());
            statement.setString(4, user.getPatronymic());
            statement.setString(5, user.getTelephone());
            statement.setString(6, user.getPassword());
            statement.setString(7, user.getJobPlace());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
    }

    private boolean isInsertUserByEmailIntoOrder(Connection connection, User user) {
        try (PreparedStatement statement = connection.prepareStatement(INSERT_EMAIL)) {
            statement.setString(1, user.getEmail());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    private void setUpdateOrderByDiscount(Connection connection, User user) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_DISCOUNT_ORDER);
            statement.setString(1, user.getJobPlace());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
        }

    }

    private int getLastOrderId(Connection connection) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(LAST_ORDER_ID);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(ID);
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }
        return 0;
    }


    private void setUpdateOrderByHotelroom(Connection connection, Hotelroom hotelroom) {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_HOTELROOM_ORDER)) {
            statement.setInt(1, hotelroom.getHotel().getId());
            statement.setInt(2, hotelroom.getNumber());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    private void setUpdateOrderByTotalPrice(Connection connection, Order order) {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_TOTAL_PRICE)) {
            statement.setInt(1, order.getTotalPrice());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    private void setUpdateOrderByData(Connection connection, String arrivalData, String departureData) {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_DATA_ORDER)) {
            statement.setString(1, arrivalData);
            statement.setString(2, departureData);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    private void checkDuplicateOrder(Connection connection, Order order) throws OrderNotCreateException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(DUPLICATE_CHECK);
            statement.setString(1, order.getArrivalData());
            statement.setString(2, order.getDepartureData());
            statement.setInt(3, order.getHotelroom().getId());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                throw new OrderNotCreateException("We have such order");
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }
    }

    private Order getWholeOrder(Connection connection, Order order, int id) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(SELECT_ALL);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Discount discount = new Discount();
                discount.setDiscountValueForJobPlace(resultSet.getInt(DISCOUNT_VALUE_FOR_JOB_PLACE));
                order.setDiscount(discount);
                order.setTotalPrice(resultSet.getInt(TOTAL_PRICE));
                order.setId(resultSet.getInt(ID));
                return order;
            }
        } catch (SQLException | RuntimeException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }
        return null;
    }
}
