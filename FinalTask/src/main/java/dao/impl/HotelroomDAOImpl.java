package dao.impl;

import dao.ConnectionPool;
import dao.HotelroomDAO;
import dao.entity.HotelSearchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static constant.DAOConstant.*;

public class HotelroomDAOImpl extends AbstractDAO implements HotelroomDAO {

    private HotelroomDAOImpl() {
        setClassName(Hotelroom.class.getSimpleName().toLowerCase());
    }

    public static HotelroomDAOImpl getInstance() {
        return instance;
    }

    private static final HotelroomDAOImpl instance = new HotelroomDAOImpl();

    private static final Logger logger = LogManager.getLogger();

    private static final String VALID_REQUEST_CONDITION = "FROM hotel_booking.hotelroom " +
            "       JOIN hotel_booking.hotel ON hotelroom.hotel = hotel.id " +
            "       LEFT JOIN   hotel_booking.`order` ON hotelroom.id = hotel_booking.order.hotelroom " +
            "WHERE (hotelroom.id NOT IN (SELECT hotel_booking.hotelroom.id AS id " +
            "                            FROM hotel_booking.`order` " +
            "                                        LEFT JOIN hotelroom  ON `order`.hotelroom = hotelroom.id " +
            "                            WHERE (arrival_data BETWEEN ? AND ?) " +
            "                               OR (departure_data BETWEEN ? AND ?)) " +
            "  OR ( arrival_data IS NULL AND departure_data IS NULL)) " +
            "  AND hotel.city = ? " +
            "  AND capacity >= ?";

    private static final String ALL_VALID_REQUESTED_HOTELROOMS = "SELECT DISTINCT" +
            "       hotelroom.price       AS price, " +
            "       hotelroom.capacity    AS capacity, " +
            "       hotelroom.comfort     AS comfort, " +
            "       hotelroom.number      AS number, " +
            "       hotelroom.image_url   AS url, " +
            "       hotelroom.description AS description, " +
            "       hotelroom.description_en AS description_en, " +
            "       hotelroom.id          AS hotelroom_id, " +
            "       hotel.city            AS city, " +
            "       hotel.name            AS name, " +
            "       hotel.id              AS hotel_id " + VALID_REQUEST_CONDITION + ";";

    private static final String ALL_VALID_REQUESTED_HOTELROOMS_LIMIT = "SELECT DISTINCT" +
            "       hotelroom.price       AS price, " +
            "       hotelroom.capacity    AS capacity, " +
            "       hotelroom.comfort     AS comfort, " +
            "       hotelroom.number      AS number, " +
            "       hotelroom.image_url   AS url, " +
            "       hotelroom.description AS description, " +
            "       hotelroom.description_en AS description_en, " +
            "       hotelroom.id          AS hotelroom_id, " +
            "       hotel.city            AS city, " +
            "       hotel.name            AS name, " +
            "       hotel.id              AS hotel_id " + VALID_REQUEST_CONDITION +
            "  LIMIT ?,?;";


    private static final String VALID_HOTELROOMS_AMOUNT = "SELECT  " +
            "count(DISTINCT hotelroom.id) AS count_id " + VALID_REQUEST_CONDITION + ";";


    private static final String DAYS_AMOUNT = "SELECT DATEDIFF(?, ?) AS days";

    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public List<Hotelroom> showAll(HotelSearchDataContainer container, Pagination pagination) {
        List<Hotelroom> hotelroomList = new ArrayList<>();
        int start = (pagination.getCurrentPage() != 1)
                ? (pagination.getCurrentPage() - 1) * Pagination.HOTEL_PER_PAGE
                : 0;
        int finish = start + Pagination.HOTEL_PER_PAGE;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement(ALL_VALID_REQUESTED_HOTELROOMS_LIMIT);
            statement.setString(1, container.getArrivalData());
            statement.setString(2, container.getDepartureData());
            statement.setString(3, container.getArrivalData());
            statement.setString(4, container.getDepartureData());
            statement.setString(5, container.getCity());
            statement.setInt(6, container.getPeopleCapacity());
            statement.setInt(7, start);
            statement.setInt(8, finish);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                fillHotelroomList(resultSet, hotelroomList);
            }
            return hotelroomList;
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return Collections.emptyList();
    }

    public int getAmountOfValidHotelrooms(HotelSearchDataContainer container) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement
                    = connection.prepareStatement(VALID_HOTELROOMS_AMOUNT);
            statement.setString(1, container.getArrivalData());
            statement.setString(2, container.getDepartureData());
            statement.setString(3, container.getArrivalData());
            statement.setString(4, container.getDepartureData());
            statement.setString(5, container.getCity());
            statement.setInt(6, container.getPeopleCapacity());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(COUNT_ID);
            }
        } catch (SQLException e) {
            logger.error(e.getStackTrace());
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
        }
        return 0;
    }

    @Override
    public List<Hotelroom> checkRoomRelevance(HotelSearchDataContainer container) {
        List<Hotelroom> hotelrooms = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement(ALL_VALID_REQUESTED_HOTELROOMS);
            statement.setString(1, container.getArrivalData());
            statement.setString(2, container.getDepartureData());
            statement.setString(3, container.getArrivalData());
            statement.setString(4, container.getDepartureData());
            statement.setString(5, container.getCity());
            statement.setInt(6, container.getPeopleCapacity());
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                fillHotelroomList(resultSet, hotelrooms);
            }
            return hotelrooms;
        } catch (SQLException e) {
            logger.error(e.getStackTrace());
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return Collections.emptyList();
    }

    @Override
    public int getDayAmount(String arrivalData, String departureData) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement
                    = connection.prepareStatement(DAYS_AMOUNT);
            statement.setString(2, arrivalData);
            statement.setString(1, departureData);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(DAYS);
            }
        } catch (SQLException e) {
            logger.error(e.getStackTrace());
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return 0;
    }

    private void fillHotelroomList(ResultSet resultSet, List<Hotelroom> hotelrooms) throws SQLException {
        Hotelroom hotelroom = new Hotelroom();
        hotelroom.setComfort(resultSet.getString(COMFORT));
        hotelroom.setPeopleCapacity(resultSet.getInt(CAPACITY));
        hotelroom.setPrice(resultSet.getInt(PRICE));
        hotelroom.setId(resultSet.getInt(HOTELROOM_ID));
        hotelroom.setNumber(resultSet.getInt(NUMBER));
        hotelroom.setImageURL(resultSet.getString(URL));
        hotelroom.setDescription(resultSet.getString(DESCRIPTION));
        hotelroom.setDescriptionEn(resultSet.getString(DESCRIPTION_EN));
        Hotel hotel = new Hotel();
        hotel.setName(resultSet.getString(NAME));
        hotel.setCity(resultSet.getString(CITY));
        hotel.setId(resultSet.getInt(HOTEL_ID));
        hotelroom.setHotel(hotel);
        hotelrooms.add(hotelroom);
    }
}
