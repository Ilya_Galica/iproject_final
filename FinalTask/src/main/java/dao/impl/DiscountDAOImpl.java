package dao.impl;

import dao.ConnectionPool;
import dao.DiscountDAO;
import dao.entity.impl.Discount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static constant.DAOConstant.*;

public class DiscountDAOImpl extends AbstractDAO implements DiscountDAO {

    private static final DiscountDAOImpl instance = new DiscountDAOImpl();

    private DiscountDAOImpl() {
        setClassName(Discount.class.getSimpleName().toLowerCase());
    }

    public static DiscountDAOImpl getInstance() {
        return instance;
    }

    private static final Logger logger = LogManager.getLogger();

    private ConnectionPool pool = ConnectionPool.getInstance();

    private static final String DISCOUNT_LIST = "SELECT * FROM hotel_booking.discount";

    @Override
    public List<Discount> getDiscountList() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Discount> discountList = new ArrayList<>();
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(DISCOUNT_LIST);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Discount discount = new Discount();
                discount.setUserJobPlace(resultSet.getString(USER_JOB_PLACE));
                discount
                        .setDiscountValueForJobPlace(resultSet
                                .getInt(DISCOUNT_VALUE_FOR_JOB_PLACE));
                discountList.add(discount);
            }
            return discountList;
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return Collections.emptyList();
    }
}
