package dao.impl;

import dao.ConnectionPool;
import dao.GenericDAO;
import dao.command.CommandDAO;
import dao.command.DirectorCommandDAO;
import dao.entity.Entity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDAO implements GenericDAO<Entity> {

    private static DirectorCommandDAO directorCommand = DirectorCommandDAO.getInstance();
    private ConnectionPool pool = ConnectionPool.getInstance();
    private static final Logger logger = LogManager.getLogger();
    private String className;
    private static boolean isMock = false;
    private static CommandDAO mockCommand;
    private static final String FIND_BY_ID = "SELECT * FROM hotel_booking.%s WHERE id=?";
    private static final String FIND_ALL = "SELECT * FROM hotel_booking.%s";
    private static final String UPDATE = "UPDATE %s ";

    public static void setMock(boolean isMock, CommandDAO mockCommand) {
        AbstractDAO.isMock = isMock;
        AbstractDAO.mockCommand = mockCommand;
    }

    void setClassName(String className) {
        this.className = className;
    }

    @Override
    public Optional<Entity> findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement(String.format(FIND_BY_ID, className));
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            CommandDAO command = (isMock)
                    ? mockCommand
                    : directorCommand.getCommand(className);
            return command.findById(resultSet);
        } catch (SQLException e) {
            logger.error("Can't find by id entity", e);
        } finally {
            pool.closeAll(resultSet, statement, connection);
        }
        return Optional.empty();
    }

    @Override
    public List<Entity> findAll() {
        List<Entity> entityList;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = pool.getConnection();
            statement
                    = connection.prepareStatement(String.format(FIND_ALL, className));
            resultSet = statement.executeQuery();
            CommandDAO command = (isMock)
                    ? mockCommand
                    : directorCommand.getCommand(className);
            entityList = command.findAll(resultSet);
            return entityList;
        } catch (SQLException e) {
            logger.error("Can't find entities", e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
        }
        return Collections.emptyList();
    }

    @Override
    public boolean delete(Entity entity) {
        Connection connection = null;
        try {
            connection = pool.getConnection();
            CommandDAO command = (isMock)
                    ? mockCommand
                    : directorCommand.getCommand(className);
            return command.delete(entity, connection);
        } catch (SQLException e) {
            logger.error("Can't delete entity", e);
        } finally {
            pool.putBackConnection(connection);
        }
        return false;
    }

    @Override
    public boolean create(Entity entity) {
        Connection connection = null;
        try {
            connection = pool.getConnection();
            CommandDAO command = (isMock)
                    ? mockCommand
                    : directorCommand.getCommand(className);
            return command.create(entity, connection);
        } catch (SQLException e) {
            logger.error("Can't create entity", e);
        } finally {
            pool.putBackConnection(connection);
        }
        return false;
    }

    @Override
    public Optional<Entity> update(Entity entity) {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement(String.format(UPDATE, className));
            statement.setInt(1, entity.getId());
            if (statement.executeUpdate() == 1) {
                return Optional.of(entity);
            }
        } catch (SQLException e) {
            logger.error("Can't update entity", e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
        }
        return Optional.empty();
    }
}
