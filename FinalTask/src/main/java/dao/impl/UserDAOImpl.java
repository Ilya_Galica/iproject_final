package dao.impl;

import dao.UserDAO;
import dao.ConnectionPool;
import dao.entity.UserRole;
import dao.entity.exception.DAOException;
import dao.entity.exception.DuplicateEmailException;
import dao.entity.exception.DuplicateTelephoneException;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import dao.entity.impl.Order;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static constant.DAOConstant.*;

public class UserDAOImpl extends AbstractDAO implements UserDAO {

    private static final UserDAOImpl instance = new UserDAOImpl();

    private UserDAOImpl() {
        setClassName(User.class.getSimpleName().toLowerCase());
    }

    public static UserDAOImpl getInstance() {
        return instance;
    }

    private static final Logger logger = LogManager.getLogger();

    private static final String SIGN_IN = "SELECT * FROM hotel_booking.user WHERE email = ? AND password= ?";
    private static final String SIGN_UP = "INSERT INTO hotel_booking.user"
            + " (email, name, surname, patronymic, telephone, password, job_place)"
            + " VALUES(?,?,?,?,?,?,?)";
    private static final String SIGN_UP_UPDATE = "UPDATE hotel_booking.user"
            + " SET hotel_booking.user.email=?, hotel_booking.user.name = ?, "
            + "hotel_booking.user.surname = ?, hotel_booking.user.patronymic = ?, "
            + "hotel_booking.user.telephone = ?, hotel_booking.user.password = ?, "
            + "hotel_booking.user.job_place = ? WHERE "
            + "hotel_booking.user.email = ?";
    private static final String EMAIL_DUPLICATE = "SELECT email FROM hotel_booking.user where email=?";
    private static final String TELEPHONE_DUPLICATE = "SELECT  telephone FROM hotel_booking.user WHERE telephone=?";

    private static final String UPDATE_SET = "UPDATE hotel_booking.user set ";
    private static final String UPDATE_WHERE = " = ? where telephone = ? and email = ?";

    private static final String SELECT_FROM = "SELECT * FROM hotel_booking.user ";
    private static final String SELECT_WHERE = " where email = ?";
    private static final String SELECT_WHERE_EMAIL = " where telephone = ?";
    private static final String SELECT_PASSWORD_WHERE_EMAIL = "SELECT password FROM hotel_booking.user WHERE email = ?";
    private static final String UPDATE_PASSWORD_WHERE_EMAIL = "UPDATE hotel_booking.user SET password = ? WHERE email = ?";
    private static final String SELECT_USER_ORDERS = "SELECT hotel_booking.`order`.arrival_data, " +
            "    hotel_booking.`order`.departure_data, " +
            "    hotel_booking.hotelroom.number, " +
            "    hotel_booking.hotel.name, " +
            "    hotel_booking.hotel.city " +
            "    FROM hotel_booking.`order`" +
            "    JOIN hotel_booking.hotelroom ON `order`.hotelroom = hotelroom.id" +
            "    JOIN hotel_booking.hotel ON hotelroom.hotel = hotel.id" +
            "    JOIN hotel_booking.user ON `order`.owner = user.id" +
            "    WHERE hotel_booking.user.email = ?";


    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public Optional<User> signIn(String email, String password) throws SuchUserNotFoundException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection
                    .prepareStatement(SIGN_IN);
            statement.setString(1, email);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setEmail(resultSet.getString(EMAIL));
                user.setName(resultSet.getString(NAME));
                user.setSurname(resultSet.getString(SURNAME));
                user.setPatronymic(resultSet.getString(PATRONYMIC));
                user.setPassword(resultSet.getString(PASSWORD));
                user.setTelephone(resultSet.getString(TELEPHONE));
                user.setJobPlace(resultSet.getString(JOB_PLACE));
                user.setRole(
                        (resultSet.getString(ROLE)
                                .equalsIgnoreCase(String.valueOf(UserRole.ADMIN)))
                                ? UserRole.ADMIN
                                : UserRole.USER);
                return Optional.of(user);
            } else {
                throw new SuchUserNotFoundException();
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
        }
        return Optional.empty();
    }

    @Override
    public boolean signUp(User user) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            boolean notDuplicateEmail
                    = isNotDuplicateEmail(connection, user.getEmail());
            boolean notDuplicateTelephone
                    = isNotDuplicateTelephone(connection, user.getTelephone());
            if (notDuplicateEmail && notDuplicateTelephone) {
                statement = statementSignUpConstruct(connection, user, SIGN_UP);
            } else {
                statement = statementSignUpConstruct(connection, user, SIGN_UP_UPDATE);
                statement.setString(8, user.getEmail());
            }
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
        }
        return false;
    }

    @Override
    public Optional<User> changePersonalInfo(String newParam, String type, User user) {
        Connection connection = null;
        PreparedStatement statement = null;
        PreparedStatement statementSelect = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(UPDATE_SET + type + UPDATE_WHERE);
            statementSelect = connection.prepareStatement(SELECT_FROM + type + SELECT_WHERE);
            switch (type) {
                case EMAIL:
                    statementSelect = connection.prepareStatement(SELECT_FROM + type + SELECT_WHERE_EMAIL);
                    updateParam(user, statement, newParam);
                    resultSet = getUserWithNewParamEmail(user, statementSelect);
                    if (resultSet.next()) {
                        user.setEmail(resultSet.getString(EMAIL));
                    }
                    break;
                case PASSWORD:
                    updateParam(user, statement, newParam);
                    resultSet = getUserWithNewParam(user, statementSelect);
                    if (resultSet.next()) {
                        user.setPassword(resultSet.getString(PASSWORD));
                    }
                    break;
                case TELEPHONE:
                    updateParam(user, statement, newParam);
                    resultSet = getUserWithNewParam(user, statementSelect);
                    if (resultSet.next()) {
                        user.setTelephone(resultSet.getString(TELEPHONE));
                    }
                    break;
                case JOB_PLACE:
                    updateParam(user, statement, newParam);
                    resultSet = getUserWithNewParam(user, statementSelect);
                    if (resultSet.next()) {
                        user.setJobPlace(resultSet.getString(JOB_PLACE));
                    }
                    break;
            }
            connection.commit();
            return Optional.of(user);
        } catch (SQLException e) {
            logger.error(e);
            pool.connectionRollback(connection);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.closePreparedStatement(statementSelect);
            pool.putBackConnection(connection);
        }
        return Optional.empty();
    }

    @Override
    public boolean isChangePersonalInfo(String newParam, User user) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(SELECT_PASSWORD_WHERE_EMAIL);
            statement.setString(1, user.getEmail());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                statement = connection.prepareStatement(UPDATE_PASSWORD_WHERE_EMAIL);
                statement.setString(1, newParam);
                statement.setString(2, user.getEmail());
                int added = statement.executeUpdate();
                connection.commit();
                return (added == 1);
            } else {
                throw new DAOException("Invalid such record don't exist, user email: " + user.getEmail());
            }
        } catch (SQLException e) {
            logger.error(e);
            pool.connectionRollback(connection);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
        }
        return false;
    }

    @Override
    public List<Order> showUserOrders(String email) {
        List<Order> orders = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement(SELECT_USER_ORDERS);
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Hotel hotel = new Hotel();
                hotel.setName(resultSet.getString(NAME));
                hotel.setCity(resultSet.getString(CITY));
                Hotelroom hotelroom = new Hotelroom();
                hotelroom.setNumber(resultSet.getInt(NUMBER));
                hotelroom.setHotel(hotel);
                Order order = new Order();
                order.setArrivalData(resultSet.getString(ARRIVAL_DATE));
                order.setDepartureData(resultSet.getString(DEPARTURE_DATE));
                order.setHotelroom(hotelroom);
                orders.add(order);
            }
            return orders;
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
        }
        return Collections.emptyList();
    }

    private boolean isNotDuplicateEmail(Connection connection, String email)
            throws SQLException {
        return isNotDuplicate(connection,email);
    }

    private boolean isNotDuplicateTelephone(Connection connection, String telephone)
            throws SQLException {
        return isNotDuplicate(connection,telephone);
    }

    private boolean isNotDuplicate(Connection connection, String param) throws SQLException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(TELEPHONE_DUPLICATE);
            statement.setString(1, param);
            resultSet = statement.executeQuery();
            return !resultSet.next();
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }
    }

    private void updateParam(User user,
                             PreparedStatement statement, String newParam) throws SQLException {
        statement.setString(1, newParam);
        statement.setString(2, user.getTelephone());
        statement.setString(3, user.getEmail());
        statement.executeUpdate();
    }

    private ResultSet getUserWithNewParam(User user, PreparedStatement statement) throws SQLException {
        statement.setString(1, user.getEmail());
        return statement.executeQuery();
    }

    private ResultSet getUserWithNewParamEmail(User user, PreparedStatement statement) throws SQLException {
        statement.setString(1, user.getTelephone());
        return statement.executeQuery();
    }

    private PreparedStatement statementSignUpConstruct(Connection connection, User user, String query) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getEmail());
        statement.setString(2, user.getName());
        statement.setString(3, user.getSurname());
        statement.setString(5, user.getTelephone());
        statement.setString(4, user.getPatronymic());
        statement.setString(6, user.getPassword());
        statement.setString(7, user.getJobPlace());
        return statement;
    }


}
