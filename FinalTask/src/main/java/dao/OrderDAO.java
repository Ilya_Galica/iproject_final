package dao;

import dao.entity.exception.OrderNotCreateException;
import dao.entity.impl.User;
import dao.entity.impl.Hotelroom;
import dao.entity.impl.Order;

import java.util.List;
import java.util.Optional;

public interface OrderDAO {
    Optional<Order> createOrder(Order order) throws OrderNotCreateException;

    boolean isSuchEmailExist(User user);
}
