package dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class ConnectionPool {
    private static final Logger logger = LogManager.getLogger();

    private static final int connectionQueueSize = 15;
    private static final BlockingQueue<Connection> connectionQueue = new ArrayBlockingQueue<>(connectionQueueSize);
    private static final BlockingQueue<Connection> givenAwayConQueue = new ArrayBlockingQueue<>(15);
    private static ConnectionPool instance;

    private static final String DB_URL = "jdbc:mysql://localhost:3306/hotel_booking?serverTimezone=UTC&autoReconnect=true&useSSL=false";
    private static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    private static boolean testing = false;
    private static final String TEST_DB_URL = "jdbc:h2:tcp://localhost/~/hotel_booking;MODE=MySQL;DATABASE_TO_LOWER=TRUE";
    private static final String TEST_DB_USER = "sa";
    private static final String TEST_DB_PASSWORD = "";

    public static void setTesting(boolean flag) {
        testing = flag;
    }

    public static ConnectionPool getInstance() {
        Lock locker = new ReentrantLock();
        locker.lock();
        Connection connection;
        try {
            if (instance == null) {
                instance = new ConnectionPool();
                Class.forName(DB_DRIVER);
                for (int i = 0; i < connectionQueueSize; i++) {
                    connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
                    connectionQueue.add(connection);
                }
            }
        } catch (ClassNotFoundException | SQLException e) {
            logger.error(e);
        } finally {
            locker.unlock();
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        Lock locker = new ReentrantLock();
        Connection connection = null;
        locker.lock();
        try {
            if (testing) {
                connection = DriverManager.getConnection(TEST_DB_URL, TEST_DB_USER, TEST_DB_PASSWORD);
            } else {
                connection = connectionQueue.take();
                givenAwayConQueue.add(connection);
            }
        } catch (InterruptedException e) {
            throw new SQLException("Error connection to the data sourse ", e);
        } finally {
            locker.unlock();
        }
        return connection;
    }


    public void putBackConnection(Connection connection) {
        Lock locker = new ReentrantLock();
        locker.lock();
        try {
            if (connection != null) {
                if (testing) {
                    connection.close();
                } else {
                    connection = givenAwayConQueue.take();
                    connectionQueue.add(connection);
                }
            }
        } catch (InterruptedException | SQLException e) {
            logger.error(e);
        } finally {
            locker.unlock();
        }

    }


    public void closePreparedStatement(PreparedStatement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public void closeResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public void connectionRollback(Connection connection) {
        try {
            connection.rollback();
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public void closeAll(ResultSet resultSet, PreparedStatement statement, Connection connection) {
        closeResultSet(resultSet);
        closePreparedStatement(statement);
        putBackConnection(connection);
    }
}
