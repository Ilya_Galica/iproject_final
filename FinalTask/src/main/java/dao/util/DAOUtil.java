package dao.util;

import dao.ConnectionPool;
import dao.entity.Entity;
import dao.entity.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAOUtil {
    private static ConnectionPool pool = ConnectionPool.getInstance();

    public static boolean simpleDelete(Entity entity, Connection connection, String className) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM hotel_booking."
                + className.toLowerCase() + " where hotel_booking." + className.toLowerCase() + ".id = ?")) {
            statement.setInt(1, entity.getId());
            if (statement.executeUpdate() == 1) {
                return true;
            }
        }
        return false;
    }


    public static boolean isSuchRecordExist(Connection connection, int id, String className) throws DAOException, SQLException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement("SELECT id FROM hotel_booking." + className + " where id = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                throw new DAOException("Element from" + className + " with such id = " + id + " not exist");
            }
            return true;
        } finally {
            pool.closeResultSet(resultSet);
            pool.closePreparedStatement(statement);
        }
    }
}
