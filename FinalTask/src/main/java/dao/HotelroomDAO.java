package dao;

import dao.entity.HotelSearchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotelroom;

import java.util.List;

public interface HotelroomDAO {

    List<Hotelroom> showAll(HotelSearchDataContainer container, Pagination pagination);

    int getAmountOfValidHotelrooms(HotelSearchDataContainer container);

    List<Hotelroom> checkRoomRelevance(HotelSearchDataContainer container);

    int getDayAmount(String arrivalData, String departureData);
}
