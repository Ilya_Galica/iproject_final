package dao.impl;

import dao.ConnectionPool;
import dao.entity.HotelSearchDataContainer;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import static org.testng.Assert.*;

public class HotelroomDAOImplTest {
    private HotelroomDAOImpl hotelroomDAO = HotelroomDAOImpl.getInstance();

    @BeforeClass
    public static void setUpClass() {
        ConnectionPool.setTesting(true);
    }

    @Test
    public void testGetAmountOfValidHotelrooms() {
        HotelSearchDataContainer container = new HotelSearchDataContainer();
        container.setCity("Minsk");
        container.setArrivalData("2019-08-13");
        container.setDepartureData("2019-08-13");
        container.setPeopleCapacity(2);
        int actual = hotelroomDAO.getAmountOfValidHotelrooms(container);
        int expected = 0;
        assertEquals(actual, expected);
    }

    @Test
    public void testGetDayAmount() {
        int actual = hotelroomDAO.getDayAmount("2019-08-13", "2019-08-15");
        int expected = 0;
        assertEquals(actual, expected);
    }

    @AfterClass
    public void removeTestPool() {
        ConnectionPool.setTesting(false);
    }
}