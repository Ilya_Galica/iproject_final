package dao.impl;

import dao.ConnectionPool;
import dao.entity.impl.Discount;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class DiscountDAOImplTest {
    @BeforeClass
    public static void setUpClass() {
        ConnectionPool.setTesting(true);
    }

    @Test
    public void testGetDiscountList() {
        DiscountDAOImpl discountDAO = DiscountDAOImpl.getInstance();
        List<Discount> discountList = discountDAO.getDiscountList();
        Discount actual = discountList.get(0);
        Discount expected = new Discount();
        expected.setUserJobPlace("Школа");
        expected.setDiscountValueForJobPlace(5);
        assertEquals(actual, expected);
    }

    @AfterClass
    public void removeTestPool(){
        ConnectionPool.setTesting(false);
    }
}