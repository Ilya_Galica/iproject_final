package dao.impl;

import dao.ConnectionPool;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class AbstractDAOTest {
    @BeforeClass
    public static void setUpClass() {
        ConnectionPool.setTesting(true);
    }

    @Test
    public void testFindById() {
        HotelroomDAOImpl hotelroomDAO = HotelroomDAOImpl.getInstance();
        hotelroomDAO.findById(11);
    }

    @AfterClass
    public void removeTestPool() {
        ConnectionPool.setTesting(false);
    }
}