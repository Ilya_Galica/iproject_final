package dao.impl;

import dao.ConnectionPool;
import dao.UserDAO;
import dao.entity.impl.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.util.CryptoUtil;

import java.security.NoSuchAlgorithmException;
import java.sql.*;

import static org.testng.Assert.*;

public class UserDAOImplTest {

    private static final Logger logger = LogManager.getLogger();
    private static UserDAO userDAO = UserDAOImpl.getInstance();

    @BeforeClass
    public static void setUpClass() {
        ConnectionPool.setTesting(true);
    }


    @Test
    public void testSignUpTrue() {
        CryptoUtil util = CryptoUtil.getInstance();
        User user = new User();
        user.setEmail("ilyagalica@gmail.com");
        try {
            user.setPassword(util.encrypt("Le1234567"));
            boolean real = userDAO.signUp(user);
            assertTrue(real);
        } catch (NoSuchAlgorithmException e) {
            logger.error(e);
        }
    }

    @Test
    public void testSignUp() {
        User user = new User();
        user.setEmail("firstDeleteTestEmail@mail.ru");
        user.setName("test");
        user.setSurname("surTest");
        user.setPatronymic("patTest");
        user.setTelephone("+375(29)678-65-76");
        user.setPassword("1234561");
        user.setJobPlace("No");
        assertFalse(userDAO.signUp(user));
    }

    @Test
    public void testChangePersonalInfo() {
        User user = new User();
        user.setEmail("firstDeleteTestEmail@mail.ru");
        user.setName("test");
        user.setSurname("surTest");
        user.setPatronymic("patTest");
        user.setTelephone("+375(29)678-65-76");
        user.setPassword("1234561");
        user.setJobPlace("No");
        User real = null;
        if (userDAO.changePersonalInfo("secondDeleteTestEmail@mail.ru", "email", user).isPresent()) {
            real = userDAO
                    .changePersonalInfo("secondDeleteTestEmail@mail.ru",
                            "email", user).get();
        }
        user.setEmail("secondDeleteTestEmail@mail.ru");
        assertEquals(real, user);

    }

    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM hotel_booking.user WHERE email=?");
            statement.setString(1, "ilyagalica@gmail.com");
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
            ConnectionPool.setTesting(false);
        }
    }
}