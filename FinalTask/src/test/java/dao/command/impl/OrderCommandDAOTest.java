package dao.command.impl;

import dao.ConnectionPool;
import dao.entity.Entity;
import dao.entity.UserRole;
import dao.entity.impl.*;
import dao.impl.AbstractDAO;
import dao.impl.OrderDAOImpl;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class OrderCommandDAOTest {

    private AbstractDAO abstractDAO = OrderDAOImpl.getInstance();

    @BeforeClass
    public static void setUpClass() {
        ConnectionPool.setTesting(true);
    }

    @Test
    public void testCreate() {
        Order expected = (Order) buildEntity();
        boolean actual = abstractDAO.create(expected);
        assertTrue(actual);
    }

    @Test
    public void testFindAll() {
        List<Entity> list = abstractDAO.findAll();
        Order actual;
        if (list.isEmpty()) {
            actual = null;
        } else {
            actual = (Order) list.get(0);
        }
        Order expected = (Order) buildEntity();
        assertNotEquals(actual, expected);
    }

    @Test
    public void testFindById() {
        Order actual = null;
        if (abstractDAO.findById(0).isPresent()) {
            actual = (Order) abstractDAO.findById(0).get();
        }
        Order expected = (Order) buildEntity();
        assertNotEquals(actual, expected);
    }

    @Test
    public void testDelete() {
        Order expected = new Order();
        expected.setId(1000);
        boolean actual = abstractDAO.delete(expected);
        assertFalse(actual);
    }


    private Entity buildEntity() {
        Order expected = new Order();
        User user = new User();
        user.setId(1);
        user.setEmail("fifth@mail.com");
        user.setName("fifth");
        user.setSurname("secondfifth");
        user.setPatronymic("patronomicfifth");
        user.setTelephone("3752956781234");
        user.setPassword("456123");
        user.setJobPlace("EPAM");
        user.setRole(UserRole.USER);
        expected.setUser(user);
        Discount discount = new Discount();
        discount.setId(1);
        discount.setUserJobPlace("Школа");
        discount.setDiscountValueForJobPlace(5);
        expected.setDiscount(discount);
        expected.setArrivalData("2020-03-30");
        expected.setDepartureData("2020-04-02");
        Hotelroom hotelroom = new Hotelroom();
        hotelroom.setId(1);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("forHotelroom");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        hotelroom.setHotel(hotel);
        hotelroom.setNumber(1);
        hotelroom.setPrice(0);
        hotelroom.setPeopleCapacity(0);
        hotelroom.setImageURL("");
        hotelroom.setDescription("Nothing");
        hotelroom.setDescriptionEn("Nothing");
        expected.setHotelroom(hotelroom);
        expected.setTotalPrice(-1);
        return expected;
    }
}