package dao.command.impl;

import dao.ConnectionPool;
import dao.entity.Entity;
import dao.entity.impl.Discount;
import dao.impl.AbstractDAO;
import dao.impl.DiscountDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.*;

public class DiscountCommandDAOTest {
    private AbstractDAO abstractDAO = DiscountDAOImpl.getInstance();
    private static final Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUpClass() {
        ConnectionPool.setTesting(true);
    }

    @Test
    public void testCreate() {
        Discount discount = new Discount();
        discount.setUserJobPlace("Ничего");
        discount.setDiscountValueForJobPlace(0);
        boolean actual = abstractDAO.create(discount);
        assertTrue(actual);
    }

    @Test
    public void testFindAll() {
        List<Entity> list = abstractDAO.findAll();
        Discount actual = (Discount) list.get(0);
        Discount expected = new Discount();
        expected.setId(0);
        expected.setUserJobPlace("Ничего");
        expected.setDiscountValueForJobPlace(0);
        assertEquals(actual, expected);
    }

    @Test
    public void testFindById() {
        Discount actual = null;
        if (abstractDAO.findById(0).isPresent()) {
            actual = (Discount) abstractDAO.findById(0).get();
        }
        Discount expected = new Discount();
        expected.setId(0);
        expected.setUserJobPlace("Ничего");
        expected.setDiscountValueForJobPlace(0);
        assertEquals(actual, expected);
    }

    @Test
    public void testDelete() {
        Discount discount = new Discount();
        discount.setId(1000);
        boolean actual = abstractDAO.delete(discount);
        assertFalse(actual);
    }

    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM hotel_booking.discount WHERE discount_value_for_job_place= ? " +
                    " and user_job_place = ? ");
            statement.setInt(1, 0);
            statement.setString(2, "Ничего");
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
            ConnectionPool.setTesting(false);
        }
    }
}