package dao.command.impl;

import dao.ConnectionPool;
import dao.entity.Entity;
import dao.entity.UserRole;
import dao.entity.impl.*;
import dao.impl.AbstractDAO;
import dao.impl.UserDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.*;

public class UserCommandDAOTest {

    private AbstractDAO abstractDAO = UserDAOImpl.getInstance();
    private static final Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUpClass() {
        ConnectionPool.setTesting(true);
    }

    @Test
    public void testCreate() {
        User expected = new User();
        expected.setEmail("newUser@mail.ru");
        expected.setName("newUser@mail.ru");
        expected.setSurname("newUser@mail.ru");
        expected.setPatronymic("newUser@mail.ru");
        expected.setTelephone("+375(29)567-81-34");
        expected.setPassword("1234567");
        expected.setJobPlace("No");
        expected.setRole(UserRole.USER);
        boolean actual = abstractDAO.create(expected);
        assertTrue(actual);
    }

    @Test
    public void testFindAll() {
        List<Entity> list = abstractDAO.findAll();
        User actual = (User) list.get(0);
        User expected = new User();
        expected.setId(0);
        expected.setEmail("newUser@mail.ru");
        expected.setName("newUser@mail.ru");
        expected.setSurname("newUser@mail.ru");
        expected.setPatronymic("newUser@mail.ru");
        expected.setTelephone("+375(29)567-81-34");
        expected.setPassword("1234567");
        expected.setJobPlace("No");
        expected.setRole(UserRole.USER);
        assertEquals(actual, expected);
    }

    @Test
    public void testFindById() {
        User actual = null;
        if (abstractDAO.findById(0).isPresent()) {
            actual = (User) abstractDAO.findById(0).get();
        }
        User expected = new User();
        expected.setId(0);
        expected.setEmail("newUser@mail.ru");
        expected.setName("newUser@mail.ru");
        expected.setSurname("newUser@mail.ru");
        expected.setPatronymic("newUser@mail.ru");
        expected.setTelephone("+375(29)567-81-34");
        expected.setPassword("1234567");
        expected.setJobPlace("No");
        expected.setRole(UserRole.USER);
        assertEquals(actual, expected);
    }

    @Test
    public void testDelete() {
        User expected = new User();
        expected.setId(1000);
        boolean actual = abstractDAO.delete(expected);
        assertFalse(actual);
    }

    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM hotel_booking.user WHERE email = ? ");
            statement.setString(1, "newUser@mail.ru");
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
            ConnectionPool.setTesting(false);
        }
    }
}