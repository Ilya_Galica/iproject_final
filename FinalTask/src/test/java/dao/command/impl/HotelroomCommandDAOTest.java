package dao.command.impl;

import dao.ConnectionPool;
import dao.entity.Entity;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import dao.impl.AbstractDAO;
import dao.impl.HotelroomDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.*;

public class HotelroomCommandDAOTest {

    private AbstractDAO abstractDAO = HotelroomDAOImpl.getInstance();
    private static final Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUpClass() {
        ConnectionPool.setTesting(true);
    }

    @Test
    public void testCreate() {
        Hotelroom expected = new Hotelroom();
        expected.setNumber(2000);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("forHotelroom");
        expected.setHotel(hotel);
        expected.setPrice(2000);
        expected.setPeopleCapacity(0);
        expected.setComfort("halflux");
        expected.setImageURL("Nothing");
        expected.setDescription("Nothing");
        expected.setDescriptionEn("Nothing");
        boolean actual = abstractDAO.create(expected);
        assertTrue(actual);
    }

    @Test
    public void testFindAll() {
        List<Entity> list = abstractDAO.findAll();
        Hotelroom actual = (Hotelroom) list.get(1);
        Hotelroom expected = new Hotelroom();
        expected.setId(1);
        expected.setNumber(1);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("forHotelroom");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        expected.setHotel(hotel);
        expected.setImageURL("");
        expected.setPrice(0);
        expected.setDescription("Nothing");
        expected.setDescriptionEn("Nothing");
        expected.setPeopleCapacity(0);
        assertEquals(actual, expected);
    }

    @Test
    public void testFindById() {
        Hotelroom actual = null;
        if (abstractDAO.findById(1).isPresent()) {
            actual = (Hotelroom) abstractDAO.findById(1).get();
        }
        Hotelroom expected = new Hotelroom();
        expected.setId(1);
        expected.setNumber(1);
        Hotel hotel = new Hotel();
        hotel.setId(1);
        hotel.setName("forHotelroom");
        hotel.setCity("Minsk");
        hotel.setCountry("Belarus");
        expected.setHotel(hotel);
        expected.setImageURL("");
        expected.setPrice(0);
        expected.setDescription("Nothing");
        expected.setDescriptionEn("Nothing");
        expected.setPeopleCapacity(0);
        assertEquals(actual, expected);
    }

    @Test
    public void testDelete() {
        Hotelroom expected = new Hotelroom();
        expected.setId(1000);
        boolean actual = abstractDAO.delete(expected);
        assertFalse(actual);
    }



    @AfterClass
    void deleteInserted() {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM hotel_booking.hotelroom WHERE capacity = ? " +
                    " and number = ? and price = ?");
            statement.setInt(1, 0);
            statement.setInt(2, 2000);
            statement.setInt(3, 2000);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            pool.closePreparedStatement(statement);
            pool.putBackConnection(connection);
            ConnectionPool.setTesting(false);
        }
    }
}