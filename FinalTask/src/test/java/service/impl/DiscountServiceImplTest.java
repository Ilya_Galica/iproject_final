package service.impl;

import dao.command.impl.DiscountCommandDAO;
import dao.entity.Entity;
import dao.entity.impl.Discount;
import dao.impl.AbstractDAO;
import dao.impl.DiscountDAOImpl;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.exception.ServiceException;

import java.sql.SQLException;
import java.util.*;

import static org.testng.Assert.*;

public class DiscountServiceImplTest {
    @Mock
    private AbstractDAO discountDAO = Mockito.mock(AbstractDAO.class);

    @Mock
    private DiscountCommandDAO commandDAO = Mockito.mock(DiscountCommandDAO.class);

    private DiscountServiceImpl discountService = DiscountServiceImpl.getInstance();


    @BeforeClass
    public void setSettings() {
        discountService.setDiscountDAO(discountDAO);
        AbstractDAO.setMock(true, commandDAO);
    }


    @Test
    public void testFindAll() throws SQLException {
        Discount expDiscount = new Discount();
        expDiscount.setId(2);
        expDiscount.setUserJobPlace("Больница");
        expDiscount.setDiscountValueForJobPlace(10);
        List<Entity> exp = Collections.singletonList(expDiscount);
        Mockito.doReturn(exp).when(discountDAO).findAll();
        Mockito.doReturn(exp).when(commandDAO).findAll(Mockito.any());
        List<Entity> act = discountService.findAll();
        assertEquals(act, exp);
    }

    @Test
    public void testFindById() throws ServiceException, SQLException {
        Discount expDiscount = new Discount();
        expDiscount.setId(2);
        expDiscount.setUserJobPlace("Больница");
        expDiscount.setDiscountValueForJobPlace(10);
        Optional<Entity> daoDiscount = Optional.of(expDiscount);
        Mockito.doReturn(daoDiscount).when(discountDAO).findById(Mockito.eq(2));
        Mockito.doReturn(daoDiscount).when(commandDAO).findById(Mockito.any());
        Discount actDiscount = (Discount) discountService.findById(2);
        assertEquals(actDiscount, expDiscount);
    }

    @Test
    public void testDelete() throws SQLException {
        Discount discount = new Discount();
        discount.setId(2);
        discount.setUserJobPlace("Больница");
        discount.setDiscountValueForJobPlace(10);
        Mockito.doReturn(true).when(discountDAO).delete(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).delete(Mockito.any(), Mockito.any());
        boolean actDiscount = discountService.delete(discount);
        assertTrue(actDiscount);
    }

    @Test
    public void testCreate() throws SQLException {
        Discount discount = new Discount();
        discount.setId(2);
        discount.setUserJobPlace("Больница");
        discount.setDiscountValueForJobPlace(10);
        Mockito.doReturn(true).when(discountDAO).create(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).create(Mockito.any(), Mockito.any());
        boolean actDiscount = discountService.create(discount);
        assertTrue(actDiscount);
    }

    @AfterClass
    public void unSetSettings() {
        discountService.setDiscountDAO(DiscountDAOImpl.getInstance());
        AbstractDAO.setMock(false, null);
    }

}