package service.impl;

import dao.command.impl.OrderCommandDAO;
import dao.entity.Entity;
import dao.entity.exception.OrderNotCreateException;
import dao.entity.impl.*;
import dao.impl.AbstractDAO;
import dao.impl.OrderDAOImpl;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.*;
import service.exception.ServiceException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


import static org.testng.Assert.*;

public class OrderServiceImplTest {
    @Mock
    private OrderDAOImpl orderDAO = Mockito.mock(OrderDAOImpl.class);
    //    ♦♫
    @Mock
    private OrderCommandDAO commandDAO = Mockito.mock(OrderCommandDAO.class);

    private OrderServiceImpl orderService = OrderServiceImpl.getInstance();

    @BeforeClass
    public void setUp() {
        orderService.setOrderDAO(orderDAO);
        AbstractDAO.setMock(true, commandDAO);
    }

    @AfterClass
    public void tearDown() {
        orderService.setOrderDAO(OrderDAOImpl.getInstance());
        AbstractDAO.setMock(false, null);
    }

    @Test
    public void testCreateOrder() throws ServiceException, OrderNotCreateException {
        Order order = buildOrder();
        Optional<Order> expOrder = Optional.of(order);
        Mockito.when(orderDAO.createOrder(Mockito.any())).thenReturn(expOrder);
        Optional<Order> actOrder = orderService.createOrder(order);
        assertEquals(expOrder, actOrder);
    }

    @Test
    public void testFindAll() throws SQLException {
        Order order = buildOrder();
        List<Entity> exp = Collections.singletonList(order);
        Mockito.doReturn(exp).when(orderDAO).findAll();
        Mockito.doReturn(exp).when(commandDAO).findAll(Mockito.any());
        List<Entity> act = orderService.findAll();
        assertEquals(act, exp);
    }

    @Test
    public void testFindById() throws SQLException, ServiceException {
        Order expOrder = buildOrder();
        Optional<Entity> daoOrder = Optional.of(expOrder);
        Mockito.doReturn(daoOrder).when(orderDAO).findById(Mockito.eq(2));
        Mockito.doReturn(daoOrder).when(commandDAO).findById(Mockito.any());
        Order actOrder = (Order) orderService.findById(2);
        assertEquals(actOrder, expOrder);
    }

    @Test
    public void testDelete() throws SQLException {
        Order order = buildOrder();
        Mockito.doReturn(true).when(orderDAO).delete(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).delete(Mockito.any(), Mockito.any());
        boolean actOrder = orderService.delete(order);
        assertTrue(actOrder);
    }

    @Test
    public void testCreate() throws SQLException {
        Order order = buildOrder();
        Mockito.doReturn(true).when(orderDAO).create(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).create(Mockito.any(), Mockito.any());
        boolean actDiscount = orderService.create(order);
        assertTrue(actDiscount);
    }

    private static Order buildOrder() {
        Order order = new Order();
        order.setId(135);
        Hotelroom hotelroom = new Hotelroom();
        hotelroom.setId(24);
        hotelroom.setNumber(1);
        hotelroom.setPrice(40);
        Hotel hotel = new Hotel();
        hotel.setCountry("Belarus");
        hotel.setCity("Minsk");
        hotel.setName("Minsk City Hostel");
        hotelroom.setHotel(hotel);
        order.setHotelroom(hotelroom);
        order.setArrivalData("20/09/2019");
        order.setDepartureData("20/09/2019");
        User user = new User();
        user.setId(223);
        user.setEmail("ilewrqgalica@mail.ru");
        user.setName("asdfdsf");
        user.setSurname("dasfasdf");
        user.setPatronymic("fdsasdf");
        user.setTelephone("+375(32)534-53-45");
        user.setJobPlace("EPAM");
        order.setUser(user);
        order.setDiscount(new Discount());
        return order;
    }
}