package service.impl;

import dao.command.impl.HotelroomCommandDAO;
import dao.entity.Entity;
import dao.entity.HotelSearchDataContainer;
import dao.entity.Pagination;
import dao.entity.impl.Hotel;
import dao.entity.impl.Hotelroom;
import dao.impl.AbstractDAO;
import dao.impl.HotelroomDAOImpl;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.*;
import service.exception.ServiceException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.testng.Assert.*;

public class HotelroomServiceImplTest {
    @Mock
    private HotelroomDAOImpl hotelroomDAO = Mockito.mock(HotelroomDAOImpl.class);
    @Mock
    private HotelroomCommandDAO commandDAO = Mockito.mock(HotelroomCommandDAO.class);

    private HotelroomServiceImpl hotelroomService = HotelroomServiceImpl.getInstance();

    @BeforeClass
    public void setSettings() {
        hotelroomService.setHotelroomDAO(hotelroomDAO);
        AbstractDAO.setMock(true, commandDAO);
    }

    @Test
    public void testShowAll() {
        Hotelroom hotelroom = buildHotelroom();
        List<Hotelroom> exp = Collections.singletonList(hotelroom);
        Mockito.doReturn(exp).when(hotelroomDAO).showAll(Mockito.any(), Mockito.any());
        HotelSearchDataContainer container = new HotelSearchDataContainer();
        container.setArrivalData("2019-10-13");
        container.setDepartureData("2019-10-13");
        container.setPeopleCapacity(1);
        container.setCity("Минск");
        Pagination pagination = new Pagination();
        List<Hotelroom> act = hotelroomService.showAll(container, pagination);
        assertEquals(act, exp);
    }

    @Test
    public void testGetAmountOfValidHotelrooms() {
        String arrival = "20/10/2019";
        String departure = "20/10/2019";
        int exp = 6;
        Mockito.doReturn(exp).when(hotelroomDAO).getAmountOfValidHotelrooms(Mockito.any());
        HotelSearchDataContainer container = new HotelSearchDataContainer();
        container.setArrivalData(arrival);
        container.setDepartureData(departure);
        int act = hotelroomService.getAmountOfValidHotelrooms(container);
        assertEquals(act, exp);
    }

    @Test
    public void testGetDayAmount() {
        String arrival = "20/10/2019";
        String departure = "20/10/2019";
        int exp = 0;
        Mockito.doReturn(exp).when(hotelroomDAO).getDayAmount(Mockito.any(), Mockito.any());
        int act = hotelroomService.getDayAmount(arrival, departure);
        assertNotEquals(act, exp);
    }

    @Test
    public void testFindAll() throws SQLException {
        Hotelroom hotelroom = buildHotelroom();
        List<Entity> exp = Collections.singletonList(hotelroom);
        Mockito.doReturn(exp).when(hotelroomDAO).findAll();
        Mockito.doReturn(exp).when(commandDAO).findAll(Mockito.any());
        List<Entity> act = hotelroomService.findAll();
        assertEquals(act, exp);
    }

    @Test
    public void testFindById() throws SQLException, ServiceException {
        Hotelroom expHotelroom = buildHotelroom();
        Optional<Entity> daoDiscount = Optional.of(expHotelroom);
        Mockito.doReturn(daoDiscount).when(hotelroomDAO).findById(Mockito.eq(2));
        Mockito.doReturn(daoDiscount).when(commandDAO).findById(Mockito.any());
        Hotelroom actHotelroom = (Hotelroom) hotelroomService.findById(2);
        assertEquals(actHotelroom, expHotelroom);
    }

    @Test
    public void testDelete() {
        Hotelroom hotelroom = buildHotelroom();
        Mockito.doReturn(true).when(hotelroomDAO).delete(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).delete(Mockito.any(), Mockito.any());
        boolean actHotelroom = hotelroomService.delete(hotelroom);
        assertTrue(actHotelroom);
    }

    @Test
    public void testCreate() {
        Hotelroom hotelroom = buildHotelroom();
        Mockito.doReturn(true).when(hotelroomDAO).create(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).create(Mockito.any(), Mockito.any());
        boolean actDiscount = hotelroomService.create(hotelroom);
        assertTrue(actDiscount);
    }

    @AfterClass
    public void unSetSettings() {
        hotelroomService.setHotelroomDAO(HotelroomDAOImpl.getInstance());
        AbstractDAO.setMock(false, null);
    }

    private static Hotelroom buildHotelroom() {
        Hotelroom hotelroom = new Hotelroom();
        hotelroom.setId(24);
        hotelroom.setNumber(1);
        hotelroom.setPrice(40);
        Hotel hotel = new Hotel();
        hotel.setCountry("Belarus");
        hotel.setCity("Minsk");
        hotel.setName("Minsk City Hostel");
        hotelroom.setHotel(hotel);
        return hotelroom;
    }
}