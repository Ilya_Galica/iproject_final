package service.impl;

import dao.command.impl.UserCommandDAO;
import dao.entity.Entity;
import dao.entity.exception.SuchUserNotFoundException;
import dao.entity.impl.User;
import dao.impl.AbstractDAO;
import dao.impl.UserDAOImpl;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.exception.ServiceException;
import service.util.CryptoUtil;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class UserServiceImplTest {
    @Mock
    private UserDAOImpl userDAO = Mockito.mock(UserDAOImpl.class);

    @Mock
    private UserCommandDAO commandDAO = Mockito.mock(UserCommandDAO.class);

    @Mock
    private CryptoUtil cryptoUtil = Mockito.mock(CryptoUtil.class);

    private UserServiceImpl userService = UserServiceImpl.getInstance();

    @BeforeClass
    public void setSettings() {
        userService.setUserDAO(userDAO, cryptoUtil);
        AbstractDAO.setMock(true, commandDAO);
    }

    @AfterClass
    public void tearDown() {
        userService.setUserDAO(UserDAOImpl.getInstance(), CryptoUtil.getInstance());
        AbstractDAO.setMock(false, null);
    }

    @Test
    public void testSignIn() throws SuchUserNotFoundException, ServiceException, NoSuchAlgorithmException {
        User user = buildUser();
        String encryptedPass = "QjeSRr2s9HvL4No1Y5NDfg==";
        String decryptedPass = "Le1234567";
        String email = "ilyagalica@mail.ru";
        Optional<User> ipUser = Optional.of(user);
        Mockito.when(cryptoUtil
                .encrypt( Mockito.any())).thenReturn(encryptedPass);
        Mockito.doReturn(ipUser).when(userDAO)
                .signIn(Mockito.eq(email), Mockito.eq(encryptedPass));
        User act = userService.signIn(email, decryptedPass);
        assertEquals(act, act);
    }

    @Test
    public void testChangePersonalInfo() {
    }

    @Test
    public void testFindAll() throws SQLException {
        User user = buildUser();
        List<Entity> exp = Collections.singletonList(user);
        Mockito.doReturn(exp).when(userDAO).findAll();
        Mockito.doReturn(exp).when(commandDAO).findAll(Mockito.any());
        List<Entity> act = userService.findAll();
        assertEquals(act, exp);
    }

    @Test
    public void testFindById() throws SQLException, ServiceException {
        User expUser = buildUser();
        Optional<Entity> daoUser = Optional.of(expUser);
        Mockito.doReturn(daoUser).when(userDAO).findById(Mockito.eq(2));
        Mockito.doReturn(daoUser).when(commandDAO).findById(Mockito.any());
        User actUser = (User) userService.findById(2);
        assertEquals(actUser, expUser);
    }

    @Test
    public void testDelete() {
        User user = buildUser();
        Mockito.doReturn(true).when(userDAO).delete(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).delete(Mockito.any(), Mockito.any());
        boolean actUser = userService.delete(user);
        assertTrue(actUser);
    }

    @Test
    public void testCreate() throws SQLException {
        User user = buildUser();
        Mockito.doReturn(true).when(userDAO).create(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).create(Mockito.any(), Mockito.any());
        boolean actDiscount = userService.create(user);
        assertTrue(actDiscount);
    }

    private User buildUser() {
        User user = new User();
        user.setId(230);
        user.setEmail("ilyagalica@mail.ru");
        user.setName("Ilya");
        user.setSurname("Илья");
        user.setPatronymic("Васильевна");
        user.setTelephone("+375(43)576-58-68");
        user.setPassword("dasfasdsDDD2121");
        return user;
    }
}