package service.impl;

import dao.command.impl.HotelCommandDAO;
import dao.entity.Entity;
import dao.entity.impl.Hotel;
import dao.impl.AbstractDAO;
import dao.impl.HotelDAOImpl;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.*;
import service.exception.ServiceException;

import java.util.Optional;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.*;

public class HotelServiceImplTest {
    @Mock
    private HotelDAOImpl hotelDAO = Mockito.mock(HotelDAOImpl.class);
    @Mock
    private HotelCommandDAO commandDAO = Mockito.mock(HotelCommandDAO.class);

    private HotelServiceImpl hotelService = HotelServiceImpl.getInstance();

    @BeforeClass
    public void setUp() {
        hotelService.setHotelDAO(hotelDAO);
        AbstractDAO.setMock(true, commandDAO);
    }

    @AfterClass
    public void tearDown() {
        hotelService.setHotelDAO(HotelDAOImpl.getInstance());
        AbstractDAO.setMock(false, null);
    }

    @Test
    public void testFindAll() throws SQLException {
        Hotel hotel = buildHotel();
        List<Entity> exp = Collections.singletonList(hotel);
        Mockito.doReturn(exp).when(hotelDAO).findAll();
        Mockito.doReturn(exp).when(commandDAO).findAll(Mockito.any());
        List<Entity> act = hotelService.findAll();
        assertEquals(act, exp);
    }

    @Test
    public void testFindById() throws ServiceException, SQLException {
        Hotel expHotel = buildHotel();
        Optional<Entity> daoDiscount = Optional.of(expHotel);
        Mockito.doReturn(daoDiscount).when(hotelDAO).findById(Mockito.eq(2));
        Mockito.doReturn(daoDiscount).when(commandDAO).findById(Mockito.any());
        Hotel actHotel = (Hotel) hotelService.findById(2);
        assertEquals(actHotel, expHotel);
    }

    @Test
    public void testDelete() {
        Hotel hotel = buildHotel();
        Mockito.doReturn(true).when(hotelDAO).delete(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).delete(Mockito.any(), Mockito.any());
        boolean actHotel = hotelService.delete(hotel);
        assertTrue(actHotel);
    }

    @Test
    public void testCreate() throws SQLException {
        Hotel hotel = buildHotel();
        Mockito.doReturn(true).when(hotelDAO).create(Mockito.any());
        Mockito.doReturn(true).when(commandDAO).create(Mockito.any(), Mockito.any());
        boolean actDiscount = hotelService.create(hotel);
        assertTrue(actDiscount);
    }

    private static Hotel buildHotel() {
        Hotel hotel = new Hotel();
        hotel.setCountry("Belarus");
        hotel.setCity("Minsk");
        hotel.setName("Minsk City Hostel");
        return hotel;
    }
}